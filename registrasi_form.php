

<?php 
require_once 'getdata.php';

$kode_tuk = trim($_POST['kode_tuk']);
$skema = $_POST['kode_skema'];
$kode_unik = trim($_POST['kode_unik']);

include "crud.php";
include "db_config.php";

$crud = new crud();

$cekid = $crud->fetch(" ms_tuk " , " KODE_TUK = '$kode_tuk' and KODE_UNIK = $kode_unik ");
if($cekid[0]['KODE_TUK'] == "")
{
    echo "<script>window.alert('Kode unik salah !');
	window.location=('daftar.php')</script>";

}

?>



<!DOCTYPE html>
<html>

<head>
    <!--Import Google Icon Font-->
    <!--   <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" />

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>LSP DEKOPIN REGISTRASI PESERTA</title>
    <link type="text/css" rel="stylesheet" href="css/style.css" media="screen,projection" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>

<body>
    <br>
    <br>
    <div class="container">

        <div class="row">
            <form class="col s12" id="reg-form" method="POST" action="registrasi.php?page=registrasi">

            <input type="hidden" id="kode_tuk" name="kode_tuk" value="<?php echo $kode_tuk; ?>">
            <input type="hidden" id="skema" name="skema" value="<?php echo $skema; ?>">
                <div class="row">
                    <div class="input-field col s12">
                        <h5>Nama Lengkap ( Sesuai KTP )</h5>
                        <input id="nama" name="nama" type="text" class="validate"  required>
                        <span class="helper-text" data-error="Pertanyaan ini wajib di isi" data-success="right"></span>
                    </div>

                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <h5>Tempat / Tanggal Lahir</h5>
                    </div>
                    <div class="input-field col s6">
                        <p>Tempat</p>
                        <input id="tempat_lahir" name="tempat_lahir" type="text" class="validate" required>

                    </div>

                    <div class="input-field col s2">
                        <p>Tanggal</p>
                        <input id="tanggal" name="tanggal" type="number" min="0" max="31" class="validate" required>

                    </div>
                    <div class="input-field col s2">
                        <p>Bulan</p>
                        <input id="bulan" name="bulan" type="number" min="0" max="12" class="validate" required>

                    </div>
                    <div class="input-field col s2">
                        <p>Tahun</p>
                        <input id="tahun" name="tahun" type="number" min="1940" max="2015" class="validate" required>

                    </div>

                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <h5>Jenis Kelamin</h5>
                        <select class="browser-default" id="jns_kelamin" name="jns_kelamin" required>
                            <option value="" disabled selected>Choose your option</option>
                            <option value="L">Laki - Laki</option>
                            <option value="P">Perempuan</option>

                        </select>

                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <h5>No KTP /  Identitas</h5>
                        <input id="ktp" name="ktp" type="text" class="validate"  required>

                    </div>

                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <h5>Alamat Rumah</h5>
                        <input id="alamat" name="alamat" type="text" class="validate"  required>

                    </div>

                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <h5>No.Telp</h5>
                        <input id="hp" name="hp" type="number" class="validate"  required>
                        <span class="helper-text" data-error="Pertanyaan ini wajib di isi" data-success="right"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <h5>Email</h5>
                        <input id="email" type="email" name="email" class="validate"  required>
                        <span class="helper-text" data-error="Pertanyaan ini wajib di isi" data-success="right"></span>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <h5>Pendidikan Terakhir</h5>
                        <select class="browser-default" id="pendidikan" name="pendidikan">
                            <option value="" disabled selected>Choose your option</option>
                          <?php while($d = mysqli_fetch_array($pendidikan)){ ?>
                            <option value="<?php echo $d['KODE_PENDIDIKAN']; ?>"><?php echo $d['DESKRIPSI']; ?></option>
                            
                         <?php   }; ?>

                        </select>
                        <span class="helper-text" data-error="Pertanyaan ini wajib di isi" data-success="right"></span>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <h5>Pekerjaan</h5>
                        <select class="browser-default" id="Pekerjaan" name="Pekerjaan">
                            <option value="" disabled selected>Choose your option</option>
                            <?php while($d = mysqli_fetch_array($pekerjaan)){ ?>
                            <option value="<?php echo $d['KODE_PEKERJAAN']; ?>"><?php echo $d['NAMA_PEKERJAAN']; ?></option>
                            
                         <?php   }; ?>

                        </select>
                        <span class="helper-text" data-error="Pertanyaan ini wajib di isi" data-success="right"></span>
                    </div>
                </div>

                <div class="row">

                    <div class="input-field col s12">
                        <h5>Nama Koperasi</h5>
                        <input id="nama_koperasi" type="text" name="nama_koperasi" class="validate"  required>
                        <span class="helper-text" data-error="Pertanyaan ini wajib di isi" data-success="right"></span>
                    </div>
                </div>

                <div class="row">

                    <div class="input-field col s12">
                        <h5>Jabatan</h5>
                        <input id="jabatan_koperasi" type="text" name="jabatan_koperasi" class="validate"  required>
                        <span class="helper-text" data-error="Pertanyaan ini wajib di isi" data-success="right"></span>
                    </div>
                </div>

                <div class="row">

                    <div class="input-field col s12">
                        <h5>Alamat Koperasi</h5>
                        <input id="alamat_koperasi" type="text" name="alamat_koperasi" class="validate" required>
                        <span class="helper-text" data-error="Pertanyaan ini wajib di isi" data-success="right"></span>
                    </div>
                </div>

                <div class="row">

                    <div class="input-field col s12">
                        <h5>No. Telp / Fax / Email Koperasi </h5>
                        <input id="kontak_koperasi" type="text" name="kontak_koperasi" class="validate" placeholder="Jawaban Anda" required>
                        <span class="helper-text" data-error="Pertanyaan ini wajib di isi" data-success="right"></span>
                    </div>
                </div>

                <div class="input-field col s12 x">
                    <button class="btn btn-large btn-register waves-effect waves-light" type="submit" name="action">Registrasi

                    </button>

                </div>

        </div>
        </form>
    </div>

</body>

</html>
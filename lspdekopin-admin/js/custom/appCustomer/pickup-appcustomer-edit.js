$(document).ready(function(){

    //If followup status == "OK"
    if($('#pickupstatus').val()=="3"){
        $('input[type=text],input[type=number],textarea').attr('readonly','readonly');
        $('select,input[type=checkbox]').attr('disabled','disabled');
        $('#btnSave,#savebackfollowup').hide();
    }

    //this function extension is declared on custom/pages.js
    $('form').SubmitHelper($('#btnCancel').attr('href'));
    
    $('#saveentrydone').click(function(e){
        var btnJunk = $(this);
        e.preventDefault();
        if(confirm('Apakah anda yakin bahwa semua dokumen dan persyaratan sudah lengkap?')){
            $.ajax({
                url : btnJunk.attr('href'),
                type: 'post',
                data: {
                    transid: $('#transid').val()
                },
                dataType: 'html',
                success : function (response) {
                    alert(response.trim());
                    window.location.href = $('#btnCancel').attr('href');
                },
                error : function(data){
                    // alert('Terjadi kesalahan');
                    alert(JSON.stringify(data));
                }
            });
        }else{
            return false;
        }
    });

    
    $('#savebackfollowup').click(function(e){
        var btnJunk = $(this);
        e.preventDefault();
        if(confirm('Apakah anda yakin untuk mengembalikan data ke followup?')){
            $.ajax({
                url : btnJunk.attr('href'),
                type: 'post',
                data: {
                    transid: $('#transid').val()
                },
                dataType: 'html',
                success : function (response) {
                    alert(response.trim());
                    window.location.href = $('#btnCancel').attr('href');
                },
                error : function(data){
                    // alert('Terjadi kesalahan');
                    alert(JSON.stringify(data));
                }
            });
        }else{
            return false;
        }
    });

});
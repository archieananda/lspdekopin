$(document).ready(function(){

    //If followup status == "OK"
    if($('#followupstatus').val()=="1"){
        $('input[type=text],input[type=number],textarea').attr('readonly','readonly');
        $('select,input[type=checkbox]').attr('disabled','disabled');
        $('#btnSave').hide();
    }
   
    //this function extension is declared on custom/pages.js
    $('form').SubmitHelper(window.location.href);

    $('#savetojunk').click(function(e){
        var btnJunk = $(this);
        e.preventDefault();
        if(confirm('Apakah anda yakin menyimpan data ke dalam Junk?')){
            $.ajax({
                url : btnJunk.attr('href'),
                type: 'post',
                data: {
                    transid: $('#transid').val()
                },
                dataType: 'html',
                success : function (response) {
                    alert(response.trim());
                    window.location.href = $('#btnCancel').attr('href');
                },
                error : function(data){
                    // alert('Terjadi kesalahan');
                    alert(JSON.stringify(data));
                }
            });
        }else{
            return false;
        }
    });

    $('#savedataentry').click(function(e){
        var btnJunk = $(this);
        e.preventDefault();
        if(confirm('Apakah anda yakin meneruskan ke Data Entry?')){
            $.ajax({
                url : btnJunk.attr('href'),
                type: 'post',
                data: {
                    transid: $('#transid').val()
                },
                dataType: 'html',
                success : function (response) {
                    alert(response.trim());
                    window.location.href = $('#btnCancel').attr('href');
                },
                error : function(data){
                    // alert('Terjadi kesalahan');
                    alert(JSON.stringify(data));
                }
            });
        }else{
            return false;
        }
    });
});
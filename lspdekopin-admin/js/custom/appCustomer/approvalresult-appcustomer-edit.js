$(document).ready(function(){

    //If followup status == "OK"
    $('#approvalresult').on('change',function(e){
        $('#rejectreason').val("");
        $('#rejectreason').removeAttr('required');
        if($(this).val()=="REJ"){
            $('.reject-reason').show();
            $('#rejectreason').attr('required','true');
        }else{
            $('.reject-reason').hide();
        }
    });

    //this function extension is declared on custom/pages.js
    $('form').SubmitHelper($('#btnCancel').attr('href'));
    
});
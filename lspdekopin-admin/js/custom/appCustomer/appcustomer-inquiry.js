
$(document).ready(function () {

    var visibleAssign = $('#leaderid').val() != '' && $('#leaderid').val() != "0" && $('#salesid').val() == '';
    
    var appCustColumnsMetadata = 
    [
        {title: "", data:"TransID", visible: visibleAssign, render: function(data, type, full, meta){
            return  (full["StatusPengajuan"] == "Request" || 
                full["StatusPengajuan"] == "FollowUp") && full["SalesName"] == null ?
                '<input type="checkbox" name="chk'+data+'" id="chk'+data+'" value="'+data+'" />' : '';
        }},
        { 
            title: "Action", data: "TransID",
                "render": function (data, type, full, meta) {
                    var fieldParam = "StatusPengajuan"
                    var link = "";
                    if(full["StatusPengajuan"] =="Request" || full["StatusPengajuan"] == "Followup"){
                        link = '<a href="followup/' + data + '" data-toggle="tooltip" title="Follow Up" class="btn btn-success mini"><i class="glyphicon glyphicon-earphone"></i></a>' 
                    }else if(full["StatusPengajuan"] =="Data Entry" || full["StatusPengajuan"] =="PickUp"){
                        link = '<a href="pickup/' + data + '" data-toggle="tooltip" title="Pick Up" class="btn btn-primary mini"><i class="glyphicon glyphicon-briefcase"></i></a>' 
                    }else if(full["StatusPengajuan"] =="Entry Done"){
                        link = '<a href="banksubmit/' + data + '" data-toggle="tooltip" title="Submit to Bank" class="btn btn-warning mini"><i class="glyphicon glyphicon-share-alt"></i></a>' 
                    }else if(full["StatusPengajuan"] =="Submit Bank"){
                        link = '<a href="bankapproval/' + data + '" data-toggle="tooltip" title="Approval Result" class="btn btn-info mini"><i class="glyphicon glyphicon-thumbs-up"></i></a>' 
                    }else{
                        link = '<a href="detailview/' + data + '" data-toggle="tooltip" title="View Detail" class="btn btn-default mini"><i class="glyphicon glyphicon-search"></i></a>' 
                    }
                    return link;
                }
        },  
        //SalesName, KTPNumber, ProductName, d.Description as ProductType, StatusPengajuan      
        { title: "Sales", data: "SalesName" },
        { title: "Customer Name", data: "CustomerName" },
        { title: "Product", data: "ProductName" },
        { title: "Product Type", data: "ProductType" },
        { title: "Status", data: "StatusPengajuan" },
        { 
            title: "Tgl.Trans",
            data: "TglPengajuan", 
            render: function(d){
                return moment(d).format("DD/MM/YYYY");
            }
        }
    ];

    function LoadData(){
        LoadDataTable($('#appCustomerList'), $('#appCustomerListRaw').val(),
        appCustColumnsMetadata,
        null,
        {
            //data table options
            searching: false
        },
        true,
        // function(settings, json) {        
        //     $('#appCustomerList input[type=checkbox]').each(function(){
        //         $(this).change(function(){

        //         });
        //     });     
        // }
        );
    }

    LoadData();
 
    $('.do-search').find("#search").click(function(){
        $.ajax({
            url : $(this).attr('urlaction'),
            type: 'post',
            data: {
                fullname : $('#fullname').val(),
                ktpno : $('#ktp').val(),
                appstatus : $('#appstatus').val(),
                producttypeid : $('#producttypeid').val(),
                productname : $('#productname').val(),
                trxfrom : $('#trxfrom').val(),
                trxto : $('#trxto').val(),
                salesid : $('#salesid').val(),
                leaderid: $('#leaderid').val()
            },
            dataType: 'html',
            success : function (response) {
                try{
                    var result = JSON.parse(response);                
                    $('#appCustomerListRaw').val(JSON.stringify(result.data));
                    LoadData();
                }catch(e){
                    alert(JSON.stringify(response));
                }
            },
            error : function(data){
                // alert(JSON.stringify(data));
                $('.container-box').html(JSON.stringify(data));
            }
        });
    });

    $('.do-search').find("#search").click();

    $('#assigntosales').click(function(){

        if($('#assignedsales').val() == ''){
            alert('Mohon pilih Sales yang akan di-assign.');
            return;
        }

        var arrSelectedID = [];
        
        $('#appCustomerList input[type=checkbox]').each(function(){
            if($(this).is(':checked')){
                arrSelectedID.push($(this).val());
            }
        });  

        if(arrSelectedID.length == 0){
            alert('Mohon pilih aplikasi untuk di-assign.');            
            return;            
        }
        
        $.ajax({
            url : $(this).attr('urlaction'),
            type: 'post',
            data: {
                transids : JSON.stringify(arrSelectedID),
                salesid : $('#assignedsales').val()
            },
            dataType: 'html',
            success : function (response) {
                try{
                    alert(response);    
                    $('.do-search').find("#search").click();
                }catch(e){
                    alert('error= '+ JSON.stringify(response));
                }
            },
            error : function(data){
                // alert(JSON.stringify(data));
                $('.container-box').html(JSON.stringify(data));
            }
        });
    })
});
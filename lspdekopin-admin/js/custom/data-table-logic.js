function LoadDataTable(objToLoad, rawData, colSetting, colDefSetting, tableSetting, specifyFieldName, onComplete){

    var parsed = JSON.parse(rawData);
    var arr = [];

    parsed.forEach(element => {    
        var array = $.map(element, function(value, index) {
            return [value];
        });  
        
        arr.push(array);
    });

    // alert(JSON.stringify(arr));

    var dtTableOption = {
        columnDefs: colDefSetting,
        data: specifyFieldName ? parsed : arr,
        columns: colSetting,
        destroy:true,
        initComplete: onComplete
    };


    if(tableSetting != null && tableSetting != undefined){
        for (var property in tableSetting) {
            if (tableSetting.hasOwnProperty(property)) {
                dtTableOption[property] = tableSetting[property];                
            }
        }
    }

    objToLoad.DataTable(dtTableOption);
}
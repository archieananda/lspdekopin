
$(document).ready(function () {
    LoadDataTable($('#plafondList'), $('#plafondListRaw').val(),
    [
        { title: "Action",
        "render": function (data, type, full, meta) {
            var link = 
            '<a href="setting/edit/' + data + '" class="btn btn-success mini"><i class="glyphicon glyphicon-edit"></i></a>' +
            // '<button urlaction="'+document.URL.substr(0,document.URL.lastIndexOf('/'))+'/delete" uniqueid="'+data+'" class="btn-delete btn btn-danger mini"><i class="glyphicon glyphicon-trash"></i></button>';
            '<button urlaction="setting/delete" uniqueid="'+data+'" class="btn-delete btn btn-danger mini"><i class="glyphicon glyphicon-trash"></i></button>';
            return link;}
        },        
        { title: "Plafond Min", render: $.fn.dataTable.render.number( ',', '.', 0 ) },
        { title: "Plafond Max", render: $.fn.dataTable.render.number( ',', '.', 0 ) },
        { title: "Status Active",
            render: function ( data, type, row ) {
                return data == 1 ? "Yes" : "No";
            }
        }
    ], 
    [
        { "className": "dt-right", "targets": [1,2] },
        { "width": "70px", "targets": [0] },
        { "className": "dt-center", "targets": [0,3] }
    ]);

    //Extend for handling delete data
    $('#plafondList').AddDeleteAction(function(){
        window.location.href = window.location.href;
    });
});

$(document).ready(function () {
    LoadDataTable($('#salesList'), $('#salesListRaw').val(),
    [
        { title: "Action",
        "render": function (data, type, full, meta) {
            var link = 
            '<a href="setting/edit/' + data + '" class="btn btn-success mini"><i class="glyphicon glyphicon-edit"></i></a>' +
            // '<button urlaction="'+document.URL.substr(0,document.URL.lastIndexOf('/'))+'/delete" uniqueid="'+data+'" class="btn-delete btn btn-danger mini"><i class="glyphicon glyphicon-trash"></i></button>';
            '<button urlaction="setting/delete" uniqueid="'+data+'" class="btn-delete btn btn-danger mini"><i class="glyphicon glyphicon-trash"></i></button>';
            return link;}
        },        
        { title: "Sales Name" },
        { title: "Sales Contact" },
        { title: "Status Active",
            render: function ( data, type, row ) {
                return data == 1 ? "Yes" : "No";
            }
        }
    ], 
    [
        { "width": "70px", "targets": [0] },
        { "className": "dt-center", "targets": [0,3] }
    ]);

    //Extend for handling delete data
    $('#salesList').AddDeleteAction(function(){
        window.location.href = window.location.href;
    });
});
$(document).ready(function(){
    $(document).ready(function(){
        tinymce.init({ selector:'#pagesContent',
        plugins: ["code", "table" ],
        height : "500" });
        
        $('#pagesName').change(function(){
            var newsVal = $(this).val().toLowerCase();
            $('#seoTitle').val(newsVal.replace(/[\W_\s]/g, "-"));
        });

        $('#pagesName').change();  
    
        //this function extension is declared on custom/pages.js
        $('form').SubmitHelper($('.btn-cancel').attr('href'));
    });
})
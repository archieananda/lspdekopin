
$(document).ready(function () {
    LoadDataTable(
        $('#pagesList'), 
        $('#pagesListRaw').val(),
        [
            {title: "Action", data:"id_pages",
                render: function (data, type, full, meta) {
                    var link = 
                    '<a href="setting/edit/' + data + '" class="btn btn-success mini"><i class="glyphicon glyphicon-edit"></i>&nbsp;Edit</a>&nbsp;' +
                    '<button urlaction="setting/delete" uniqueid="'+data+'" class="btn-delete btn btn-danger mini"><i class="glyphicon glyphicon-trash"></i>&nbsp;Delete</button>';
                    return link;
                }
            },
            {title: "Pages Name", data: "name_pages"},
            {title: "SEO Title", data: "judul_seo", 
                render: function(data){
                    var renderedData = data;
                    return renderedData.length > 30 ?
                    renderedData.substr( 0, 30 ) +'…' :
                    renderedData;
                }
            },
            {title: "Tags", data: "tags", 
                render: function(data){
                    var renderedData = data == null ? "" : data;
                    return renderedData.length > 30 ?
                    renderedData.substr( 0, 30 ) +'…' :
                    renderedData;
                }
            },
            {title: "Parent", data: "namaparent"},
            // {
            //     title: "HTML Content", data: "pages",
            //     render: function(data){
            //         var renderedData = "<![CDATA["+data+"";
            //         return renderedData.length > 100 ?
            //         renderedData.substr( 0, 100 ) +'…' :
            //         renderedData;
            //     }
            // }
        ], 
        [
            { "width": "140px", "className" : "dt-center", "targets": [0] },
        ],
        null,
        true
    );

    //Extend for handling delete data
    $('#pagesList').AddDeleteAction(function(){
        window.location.href = window.location.href;
    });
});
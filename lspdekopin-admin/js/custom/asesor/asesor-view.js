
$(document).ready(function () {
    LoadDataTable($('#userList'), $('#userListRaw').val(),
    [
        { title: "Action",
        "render": function (data, type, full, meta) {
            return '<a href="setting/edit/' + data + '" class="btn btn-success mini"><i class="glyphicon glyphicon-edit"></i>&nbsp;Edit</a>';}
        },        
        { title: "Kode Asesor"},
        { title: "Nama Asesor"},
        { title: "Active" }
    ],
    [
        { "className": "dt-center", "targets": [0] }
    ]);
});
$(document).ready(function() {

    $('.to-top').click(function(){
        var body = $("html, body");
        body.stop().animate({scrollTop:0}, 500, 'swing', function() { 
           
        });
    });

    $('.to-bottom').click(function(){
        var body = $("html, body");
        body.stop().animate({scrollTop:$(document).height()}, 500, 'swing', function() { 
           
        });
    });

    $(document).find('.form-horizontal label').each(function(){
        $(this).addClass('control-label');
    })

    $('.date-picker.single.date').each(function(){
        var objDatePickerDate = $(this);        
        objDatePickerDate.dateRangePicker(
            {
                format: 'dddd, DD/MM/YYYY HH:mm:ss',
                singleDate: true,
                singleMonth: true,
                time:{
                  enabled: true  
                },
                getValue: function()
                {
                    if (objDatePickerDate.val())
                        return objDatePickerDate.val();
                    else
                        return '';
                },
                customTopBar: function(){
                    return objDatePickerDate.val()
                },
                setValue: function(s)
                {
                    objDatePickerDate.val(s);
                }
            }	
        );
    });

    $('.date-picker.from').each(function(){
        var objDatePicker = $(this);
        var objDatePickerTo = objDatePicker.closest('.form-group').find('.date-picker.to');
        objDatePicker.dateRangePicker(
            {
                format: 'DD/MM/YYYY',
                separator : ' to ',
                getValue: function()
                {
                    if (objDatePicker.val() && objDatePickerTo.val() )
                        return objDatePicker.val() + ' to ' + objDatePickerTo.val();
                    else
                        return '';
                },
                setValue: function(s,s1,s2)
                {
                    objDatePicker.val(s1);
                    objDatePickerTo.val(s2);
                }
            }	
        );
        objDatePickerTo.dateRangePicker(
            {
                format: 'DD/MM/YYYY',
                separator : ' to ',
                getValue: function()
                {
                    if (objDatePicker.val() && objDatePickerTo.val() )
                        return objDatePicker.val() + ' to ' + objDatePickerTo.val();
                    else
                        return '';
                },
                setValue: function(s,s1,s2)
                {
                    objDatePicker.val(s1);
                    objDatePickerTo.val(s2);
                }
            }	
        );
    });

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });

    //This extend table for being enable to delete data automatically, based on its UniqueId attribute
    //Attribute needed : 
    //1.urlaction = url action for deletion (placed in controller)
    //2.uniqueid = id uniqueid for deletion parameter db
    $.fn.AddDeleteAction = function(onSuccessAfterDeletion){
        $(this).each(function(){
            //each table has class btn-delete (for delete automation action)
            $(this).find('td .btn-delete').each(function(){
                var btnDelete = $(this);

                btnDelete.click(function(){
                    if(!confirm('Yakin untuk menghapus data?')){
                        return false;
                    }else{
                        $.ajax({
                            url : btnDelete.attr('urlaction'),
                            type: 'post',
                            data: {
                                uniqueid: btnDelete.attr('uniqueid')
                            },
                            dataType: 'html',
                            success : function (response) {
                                alert(response.trim());
                                if(response){
                                    onSuccessAfterDeletion();                          
                                }
                            },
                            error : function(data){
                                // alert('Terjadi kesalahan');
                                alert(JSON.stringify(data));
                            }
                        });
                    }
                })
            })
        });
    }

    //This extend table for being enable to perform certain action data automatically, based on its UniqueId attribute
    //Attribute needed : 
    //1.urlaction = url for action call (placed in controller)
    //2.uniqueid = id uniqueid for action parameter db
    $.fn.AddPerformAction = function(strUniqueSelector, confirmMessage, onSuccessAferResult){
        $(this).each(function(){
            //each table has class btn-delete (for delete automation action)
            $(this).find('td '+strUniqueSelector).each(function(){
                var btnDelete = $(this);

                btnDelete.click(function(){
                    if(!confirm(confirmMessage)){
                        return false;
                    }else{
                        $.ajax({
                            url : btnDelete.attr('urlaction'),
                            type: 'post',
                            data: {
                                uniqueid: btnDelete.attr('uniqueid')
                            },
                            dataType: 'html',
                            success : function (response) {
                                alert(response.trim());
                                if(response){
                                    onSuccessAferResult();                          
                                }
                            },
                            error : function(data){
                                // alert('Terjadi kesalahan');
                                alert(JSON.stringify(data));
                            }
                        });
                    }
                })
            })
        });
    }

    $('select option:first-child').each(function(){
        $(this).prepend('-');
    });

    $('form').find('.btn-cancel').each(function(){
        $(this).click(function(e){
            return confirm('Apakah anda yakin keluar? Data anda mungkin saja belum tersimpan');
        });
    });

    $('fieldset').each(function(){
        $(this).find('legend').append('<i class="glyphicon glyphicon-chevron-down toggle-min"></i>');
    });

    $('.toggle-min').each(function(){
        var objToggle = $(this);
        $(this).click(function(){
            // objToggle.closest('fieldset').find('.form-horizontal').height(0);
            var objParent = objToggle.closest('fieldset').find('.form-horizontal');
            objParent.animate({
                // opacity: 0.25,
                height: "toggle"
            },500, function(){
                if(objToggle.hasClass('glyphicon-chevron-up')){
                    objToggle.removeClass('glyphicon-chevron-up');                    
                    objToggle.addClass('glyphicon-chevron-down');
                }else{
                    objToggle.removeClass('glyphicon-chevron-down');                    
                    objToggle.addClass('glyphicon-chevron-up');
                }
            });
        });
    });

    $.fn.SubmitHelper = function(urlRedirectSuccess, fnCondition){
        $(this).submit(function(e){
            e.preventDefault();            
            var cond = true;

            if(fnCondition != undefined)
                cond = fnCondition();
        
            if(cond){
                
                if(confirm('Yakin untuk menyimpan data?')){
                    
                    $( "#loading" ).show();

                    if (typeof FormData !== 'undefined') {

                        var formData = new FormData( $(this)[0] );

                        // alert(JSON.stringify($(this)[0]));

                        $.ajax({
                            url : $(this).attr('action'), // or whatever
                            type : 'POST',
                            data : formData,
                            cache : false,
                            contentType : false,
                            processData : false,
                            success : function (response) {
                                alert(response.trim());
                                if(response){
                                    window.location.href=urlRedirectSuccess;     
                                    $( "#loading" ).hide();
                                }
                            },
                            error : function(data){
                                alert(JSON.stringify(data));
                                $( "#loading" ).hide();                                
                            }
                        });

                        
                        // $.ajax({
                        //     url : $(this).attr('action'), // or whatever
                        //     type: 'post',
                        //     data: $(this).serialize(),
                        //     dataType: 'html',
                        //     success : function (response) {
                        //         alert(response.trim());
                        //         // if(response){
                        //         //     window.location.href=urlRedirectSuccess;                            
                        //         // }
                        //     },
                        //     error : function(data){
                        //         alert('Terjadi kesalahan');
                        //     }
                        // });
                    
                    } else {
                        message("Your Browser Don't support FormData API! Use IE 10 or Above!");
                    }  
                }
            }

        });            
    }

    $('.do-search').each(function(){
        var formSearch = $(this);
        $(this).find("#reset").click(function(){
            formSearch.find('input[type="text"],textarea,select').each(function(){
                $(this).val('');
            });
        });
    })

    // $('#sidebar').each(function(){
    //     $(this).find('a').each(function(){
    //         if(window.location.href==$(this).attr('href')){
    //             $(this).addClass('active');
    //         }
    //     });
    // })

    
    $(document).ajaxStart(function() {
        $( "#loading" ).show();
    });

    $(document).ajaxComplete(function() {
        $( "#loading" ).hide();
    });

    $('select').select2();

    $('.select2-container').width('100%');

    var $dragging = null;

    $(document.body).on("mousemove", function(e) {
        // if($(e.target).parent().hasClass('nav-scroll')){
        //     var objParent = $(e.target).parent();
        //     objParent.offset({
        //         top: e.pageY,
        //         left: e.pageX
        //     });
        // }else{
            if ($dragging) {
                $dragging.offset({
                    top: e.pageY,
                    left: e.pageX
                });
            }
        // }
    });

    $(document.body).on("mousedown", "div", function (e) {
        if($(e.target).hasClass('nav-scroll'))
            $dragging = $(e.target);
    });

    $(document.body).on("mouseup", function (e) {
        $dragging = null;
    });

    function auto_grow(element) {
        $(element).css('height', element.scrollHeight + 'px');
    }

    $('textarea').each(function(){
        auto_grow(this);
    });

    $(".btn-print").on('click', function() {
        //Print ele4 with custom options
        $(".container-box").print({
            //Use Global styles
            globalStyles : true,
            //Add link with attrbute media=print
            mediaPrint : true,
            //Custom stylesheet
            stylesheet : "http://localhost/danakuadmin/css/style-print.css",
            //Print in a hidden iframe
            iframe : false,
            //Don't print this
            noPrintSelector : ".nav-scroll,.act-float",
            //Add this at top
            prepend : "Print Tools",
            //Add this on bottom
            append : "Document Print For Data Entry",
            //Log to console when printing is done via a deffered callback
            deferred: $.Deferred().done(function() { console.log('Printing done', arguments); })
        });
    });
});
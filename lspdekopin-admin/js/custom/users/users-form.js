$(document).ready(function(){
    
    if(window.location.href.indexOf('add') >= 0){
        $('#password').attr("required","true");
    }

    //If user_level == 4 (Sales)
    $('#userlevel').on('change',function(e){
        $('#leader').val("");
        $('#leader').removeAttr('required');
        if($(this).val()=="4"){
            $('.leader-select').show();
            $('#leader').attr('required','true');
        }else{
            $('.leader-select').hide();
        }
    });

    $('#userlevel').change(function(){
        
    });

    //this function extension is declared on custom/pages.js
    $('form').SubmitHelper($('.btn-cancel').attr('href'),
    function(){
        if($('#password').val()!=$('#retype').val()){
            return false;
        }else{
            return true;
        }
    });
});
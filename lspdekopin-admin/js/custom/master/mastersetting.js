
$(document).ready(function () {
    function LoadData(){
        LoadDataTable($('#masterSettingList'), $('#masterSettingListRaw').val(),
        [
            { title: "Action",
            "render": function (data, type, full, meta) {
                var link = '<a href="setting/edit/' + data + '" data-toggle="tooltip" title="Edit Setting" class="btn btn-success mini"><i class="glyphicon glyphicon-edit"></i>&nbsp;Edit</a>'
                + '<button urlaction="delete" uniqueid="'+data+'" class="btn-delete btn btn-danger mini" title="Delete Setting"><i class="glyphicon glyphicon-trash"></i>&nbsp;Delete</button>';
                return link;}
            },  
            { title: "Parameter" },
            { title: "Value" },
            { title: "Description" },
            { title: "Status" }
            // { 
            //     title: "Tgl.Trans", 
            //     render: function(d){
            //         return moment(d).format("DD/MM/YYYY");
            //     }
            // }
        ],
        null,
        {
            //data table options
            searching: false
        });
    }

    LoadData();
 
    $('.do-search').find("#search").click(function(){

        if($('#settingparam').val()==''){
            $('#addsetting').prop('disabled', true);
        }else{
            $('#addsetting').prop('disabled', false);
            $('#addsetting').click(function(){
                window.location.href = $(this).attr('urlaction') + $('#settingparam').val();
            });
        }

        $.ajax({
            url : $(this).attr('urlaction'),
            type: 'post',
            data: {
                settingparam : $('#settingparam').val(),
            },
            dataType: 'html',
            success : function (response) {
                try{
                    var result = JSON.parse(response);                
                    $('#masterSettingListRaw').val(JSON.stringify(result.data));
                    LoadData();
                }catch(e){
                    alert(JSON.stringify(response));
                }
            },
            error : function(data){
                // alert(JSON.stringify(data));
                $('.container-box').html(JSON.stringify(data));
            }
        });
    });

    // $('.do-search').find("#search").click();
});
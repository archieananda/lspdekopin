
$(document).ready(function () {
    LoadDataTable($('#prodRateList'), $('#prodRateListRaw').val(),
    [
        { title: "Action",
        "render": function (data, type, full, meta) {
            var link = 
            '<a href="edit/' + data + '" class="btn btn-success mini"><i class="glyphicon glyphicon-edit"></i></a>' +
            // '<button urlaction="'+document.URL.substr(0,document.URL.lastIndexOf('/'))+'/delete" uniqueid="'+data+'" class="btn-delete btn btn-danger mini"><i class="glyphicon glyphicon-trash"></i></button>';
            '<button urlaction="delete" uniqueid="'+data+'" class="btn-delete btn btn-danger mini"><i class="glyphicon glyphicon-trash"></i></button>';
            return link;}
        },        
        { title: "Product",
            render: function ( data, type, row ) {
                return data.length > 50 ?
                    data.substr( 0, 50 ) +'…' :
                    data;
            }
        },
        { title: "Type" },
        { title: "Tenor" },
        { title: "Plafond Min", render: $.fn.dataTable.render.number( ',', '.', 0 ) },
        { title: "Plafond Max", render: $.fn.dataTable.render.number( ',', '.', 0 ) },
        { title: "Eff. Rate", render: $.fn.dataTable.render.number( ',', '.', 2 ) },
        { title: "Flat Rate", render: $.fn.dataTable.render.number( ',', '.', 2 ) },
        { title: "Status",
            render: function ( data, type, row ) {
                return data == 1 ? "Yes" : "No";
            }
        }
    ], 
    [
        { "className": "dt-right", "targets": [4,5] },
        { "width": "70px", "targets": [0] },
        { "className": "dt-center", "targets": [0] }
    ]);

    //Extend for handling delete data
    $('#prodRateList').AddDeleteAction(function(){
        window.location.href = window.location.href;
    });
});
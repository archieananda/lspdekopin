
$(document).ready(function () {
    LoadDataTable($('#prodList'), $('#prodListRaw').val(),
    [
        { title: "Edit",
        render: function (data, type, full, meta) {
            var link = 
            '<a href="setting/edit/' + data + '" class="btn btn-primary mini"><i class="glyphicon glyphicon-edit"></i>&nbsp;Product</a>&nbsp;' +
            (full[1] == "KTA" || full[1] == "MULTIGUNA" ?
            '<a href="setting/rate/' + data + '" class="btn btn-success mini"><i class="glyphicon glyphicon-edit"></i>&nbsp;Rate</a>'
            : "");
            return link;}
        },        
        { title: "Type" },
        { title: "Product Name",
        render: function ( data, type, row ) {
            return data.length > 40 ?
                data.substr( 0, 40 ) +'…' :
                data;
        }},
        { title: "Active" }
        // ,
        // { title: "HTMLContent",
        // render: function ( data, type, row ) {
        //     return data.length > 40 ?
        //         data.substr( 0, 40 ) +'…' :
        //         data;
        // }}
    ], 
    [
        { "className": "dt-center", "targets": [0] },
    ]);
});
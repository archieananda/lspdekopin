
$(document).ready(function () {
    LoadDataTable(
        $('#postsList'), 
        $('#postsListRaw').val(),
        [
            {title: "Action", data:"id_berita",
                render: function (data, type, full, meta) {
                    var link = 
                    '<a href="setting/edit/' + data + '" class="btn btn-success mini"><i class="glyphicon glyphicon-edit"></i>&nbsp;Edit</a>&nbsp;' +
                    '<button urlaction="setting/delete" uniqueid="'+data+'" class="btn-delete btn btn-danger mini"><i class="glyphicon glyphicon-trash"></i>&nbsp;Delete</button>';
                    return link;
                }
            },
            {title: "Post Title", data: "judul_berita"},
            {title: "SEO Title", data: "judul_seo"},
            {title: "Tags", data: "tags"},
            // {
            //     title: "HTML Content", data: "pages",
            //     render: function(data){
            //         var renderedData = "<![CDATA["+data+"";
            //         return renderedData.length > 100 ?
            //         renderedData.substr( 0, 100 ) +'…' :
            //         renderedData;
            //     }
            // }
        ], 
        [
            { "width": "140px", "className" : "dt-center", "targets": [0] },
        ],
        null,
        true
    );

    //Extend for handling delete data
    $('#postsList').AddDeleteAction(function(){
        window.location.href = window.location.href;
    });
});
$(document).ready(function(){

    tinymce.init({ selector:'#postsContent',
    height : "500" });

    tinymce.init({ selector:'#leadinberita',
    height : "500" });

    $('#newsTitle').change(function(){
        var newsVal = $(this).val().toLowerCase();
        $('#seoTitle').val(newsVal.replace(/[\W_\s]/g, "-"));
    });

    $('#newsTitle').change();    

    //this function extension is declared on custom/pages.js
    $('form').SubmitHelper($('.btn-cancel').attr('href'));
})
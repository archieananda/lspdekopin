<?php
/**
 * Home Class
 *
 * Archie Ananda
 */
class Home extends CI_Controller {
	/**
	 * Constructor
	 */
	public function  __construct()
	{
		parent::__construct();
		if ($this->session->userdata('login') == FALSE)
		{
			redirect('Login');	
		}
		$this->load->model('Home_model', '', TRUE);
		$this->load->model('Pages_model', '', TRUE);
	}

	var $limit = 10;
	var $title = 'Home';
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman absen,
	 * jika tidak akan meload halaman login
	 */
	function index()
	{
		$this->tampil_home();
	}

		function image_editor() {
		
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= 'Image Editor';
		$data['main_view'] 		= 'image_editor.php';
		$data['form_action']	= site_url('post/add_process');
		$this->load->view('template', $data);
	
	}

	function tampil_home(){

		$data['title'] = $this->title;
		$data['h2_title'] = 'Home - Dashboard';
		$data['main_view'] = 'home/home_view';
		$data['newPages'] = $this->Pages_model->terbaruPages();

		$this->load->view('template', $data);

	}
	
	}
	
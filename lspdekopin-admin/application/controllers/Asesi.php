<?php
/**
 * Asesi Class
 *
 * Archie Ananda
 */
class Asesi extends CI_Controller {

	public function  __construct()
	{
		parent::__construct();
		if ($this->session->userdata('login') == FALSE)
		{
			redirect('login');	
		}
		$this->load->model('Users_model', '', TRUE);
		$this->load->model('general_model', '', TRUE);
		$this->load->model('Asesi_model', '', TRUE);
		//declare helper web_helper
        $this->load->helper('Web_helper');
		$this->load->helper(array('form', 'url'));
		
        $this->load->model('Login_model', '', TRUE);        
        if($this->Login_model->get_userlevel_by_username($this->session->userdata('username')) != USER_LEVEL_ADMIN )
        {
            show_error("Forbidden Access");
        }
	}

	var $limit = 10;
	var $title = 'Asesi';
	

	function index()
	{
		$this->tampilview();
	}

	function tampilview(){

		$data['title'] = $this->title;
		$data['h2_title'] = '<i class="glyphicon glyphicon-file"></i> Asesi Management';
		$data['main_view'] = 'asesi/asesi_view';
		
		$data['extraHelperScript'] = getDataTableScriptSource();
        $data['extraHelperCss'] = getDataTableCssSource();
		$data['extraScript'] = array('asesi/asesi-view.js');
		
		$data['isi'] = json_encode($this->Asesi_model->daftarAsesi());
		$this->load->view('template', $data);
	}


	function add() {
		
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= '<i class="glyphicon glyphicon-user"></i> Asesi > Add asesi';
		$data['main_view'] 		= 'asesi/asesi_form';
		$data['form_action']	= site_url('asesi/setting/add/insert');
		$data['extraScript'] = array('asesi/asesi-form.js');
		$this->load->view('template', $data);
	
	}


	function addinsert() {

		$userid = $this->input->post('userid');
		$kodetuk = $this->input->post('kodetuk');
		
		$lokasi = $this->input->post('lokasi');
		$tanggal = $this->input->post('tanggal');
		$newdate = DateTime::createFromFormat('d-m-Y', $tanggal)->format('Y-m-d');
		
		$kode = $this->input->post('kode');

		if($this->input->post('asesi_id') !== NULL)
		$asesi_id = $this->input->post('asesi_id');	
		
		if($this->input->post('old_tuk') !== NULL)
		$kodetuk_lama = $this->input->post('old_tuk');
		
	
	

		$newData = array(
			'KODE_TUK'=>$kodetuk,	
			'TGL_ASESI'=>$newdate,
			'STATUS'=>'NEW',
			'CRTBY'=>$this->session->userdata('username'),
			'CRTDATE'=>date('Y-m-d h:i:s')
		);

		$newData2 = array(
			'KODE_TUK'=>$kodetuk,	
			'DESKRIPSI'=>$lokasi,
			'ISACTIVE'=>1,
			'KODE_UNIK'=>$kode,
			'UPDBY'=>$this->session->userdata('username'),
			'LASTUPD'=>date('Y-m-d h:i:s')
		);


		if(strlen($asesi_id) > 0){		
			$newData = array_merge($newData, array(
				'H_KODE_ASESI'=>$asesi_id
			));
			
			if($this->Asesi_model->saveUpdateAsesiSetting($newData) && $this->Asesi_model->saveUpdatemsTuk($newData2,$kodetuk_lama) ){
				echo SUCCESS_SAVE;
			}else{
				echo FAILED_SAVE;
			}
		}
		else{
			if($this->Asesi_model->saveInsertAsesiSetting($newData) && $this->Asesi_model->saveInsertmstuk($newData2)){
				echo SUCCESS_SAVE;
			}else{
				echo FAILED_SAVE;
			}
		}
		
	
	}

	function asesi_edit($id = 0) {
		
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= '<i class="glyphicon glyphicon-user"></i> Users > Edit user';
		$data['main_view'] 		= 'users/users_form';
		$data['form_action']	= site_url('users/setting/edit/update');
		$data['asesiData'] = $this->Users_model->getUserData($id);
		$data['extraScript'] = array('asesi/asesi-form.js');
			

		$this->load->view('template', $data);

	}




	function getlistAsesimember($id = 0) {
		
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= '<i class="glyphicon glyphicon-user"></i> Data > Penailaian Asesi';
		$data['main_view'] 		= 'asesi/asesi_detail';
		$data['form_action']	= site_url('asesi/setting/submit/insert');
		$data['extraHelperScript'] = getDataTableScriptSource();
        $data['extraHelperCss'] = getDataTableCssSource();
		$data['extraScript'] = array('asesi/asesi-detail.js');
		$data['listasesor'] = $this->Asesi_model->getlistasesor();
		$data['idasesi'] = $id;
		$data['daftarasesi'] = $this->Asesi_model->DaftarAsesibyid($id);
		$this->load->view('template', $data);
	
	}

	function ExportAsesi($id = 0) {
		
		$data['title'] 			= "ExportAsesi".$id.date('Y-m-d h:i:s');
		$data['listasesor'] = $this->Asesi_model->getlistasesor();
		$data['daftarasesi'] = $this->Asesi_model->DaftarAsesibyid($id);
		$this->load->view('asesi/asesi_export', $data);
	
	}


	function delete_asesi($id = 0) {


		if($this->input->post('uniqueid') == NULL)
        {
            show_error('uniqueid = 0');
        }
        
		$id = $this->input->post('uniqueid');
		
		$newData = array(
			'H_KODE_ASESI'=>$id,	
			'IS_DELETED'=>1,
			'STATUS'=>'DELETED',
			'UPDBY'=>$this->session->userdata('username'),
			'LASTUPD'=>date('Y-m-d h:i:s')
		);

	

        if($this->Asesi_model->deleteasesi($newData)){
            echo SUCCESS_DELETE;
        }else{
            echo NO_CHANGE_SAVE;
		}
		
	

	}

	function deletemember($id = 0) {

	
		
		$newData = array(
			'D_KODE_ASESI'=>$id,	
			'ISDELETED'=>1,
			
			'UPDBY'=>$this->session->userdata('username'),
			'LASTUPD'=>date('Y-m-d h:i:s')
		);

	

        if($this->Asesi_model->deletemember($newData)){
            echo SUCCESS_DELETE;
        }else{
            echo NO_CHANGE_SAVE;
		}
		
	

	}

	function completed($id = 0) {

		
		$newData = array(
			'H_KODE_ASESI'=>$id,	
			'IS_DELETED'=>0,
			'STATUS'=>'COMPLETED',
			'UPDBY'=>$this->session->userdata('username'),
			'LASTUPD'=>date('Y-m-d h:i:s')
		);

	

        if($this->Asesi_model->deleteasesi($newData)){
			
			
			echo SUCCESS_COMPLETED;
        }else{
            echo FAIL_COMPLETED;
		}
		
	

	}

	public function submit_asesi() {
	//	print_r($_POST);
		$dataasesi = $this->input->post('dataasesi');
		foreach ($dataasesi as $value)
		{
			$id = $value['id'];
		$asesidata = array(
			'KODE_ASSESOR'		=> $value['asesor'],
			'HASIL'		=> $value['hasil'],
		
		);	
		$res1 = $this->Asesi_model->saveUpdateAsesi($asesidata,$id);
		if ($res1) {
			$response = array(
				'status' => 1,
				'message' => 'Data berhasil dirubah!!'
			);
		}
		else 
		{
			$response = array(
				'status' => 0,
				'message' => 'Data berhasil di update'
			);
		}
	}
	echo json_encode($response);
	}

		//print_r($_POST);
	//die;


		
	
	}



	
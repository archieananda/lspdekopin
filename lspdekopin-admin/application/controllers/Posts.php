<?php
/**
 * Posts Class
 *
 * Archie Ananda
 */
class Posts extends CI_Controller {
	/**
	 * Constructor
	 */
	public function  __construct()
	{
		parent::__construct();
		if ($this->session->userdata('login') == FALSE)
		{
			redirect('login');	
		}
		$this->load->model('Posts_model', '', TRUE);
		$this->load->model('General_model', '', TRUE);

		//declare helper web_helper
        $this->load->helper('Web_helper');
		$this->load->helper(array('form', 'url'));
		
		$this->load->model('Login_model', '', TRUE);        
        if($this->Login_model->get_userlevel_by_username($this->session->userdata('username')) != USER_LEVEL_ADMIN )
        {
            show_error("Forbidden Access");
        }
	}

	var $limit = 10;
	var $title = 'News / Article Setting';
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman absen,
	 * jika tidak akan meload halaman login
	 */
	function index()
	{
		$this->tampil_posts();
	}

	function add() {
		$data['title'] = $this->title;
        $data['h2_title'] = '<i class="glyphicon glyphicon-file"></i> '.$this->title.' > Add';
        $data['main_view'] = 'posts/posts_form';
        $data['form_action']	= site_url('posts/setting/add/insert');
		$data['extraHelperScript'] = array('tinymce/tinymce.min.js','bootstrap-tagsinput.js');
		$data['extraHelperCss'] = array('bootstrap-tagsinput.css');
        $data['extraScript'] = array('postsSetting/posts-form.js');

		$this->load->view('template', $data);
	}

	function edit($id) {
		$data['title'] = $this->title;
        $data['h2_title'] = '<i class="glyphicon glyphicon-file"></i> '.$this->title.' > Add';
        $data['main_view'] = 'posts/posts_form';
        $data['form_action']	= site_url('posts/setting/add/insert');
		$data['extraHelperScript'] = array('tinymce/tinymce.min.js','bootstrap-tagsinput.js');
		$data['extraHelperCss'] = array('bootstrap-tagsinput.css');
        $data['extraScript'] = array('postsSetting/posts-form.js');
		// $data['pagesTypeList'] = $this->General_model->getSettingByParameter('PageType');

		$data['viewData'] = $this->Posts_model->postsbyid($id);

		$this->load->view('template', $data);
	}

	function tampil_posts(){

		$data['title'] = $this->title;
        $data['h2_title'] = '<i class="glyphicon glyphicon-file"></i> '.$this->title;
        $data['main_view'] = 'posts/posts_view';
        //notes: getDataTableScriptSource declared in helpers directory, inside web_helper
        $data['extraHelperScript'] = getDataTableScriptSource();
        $data['extraHelperCss'] = getDataTableCssSource();
        $data['extraScript'] = array('postsSetting/posts-view.js');
        $data['postsListData'] = json_encode($this->Posts_model->daftarPosts());

		$this->load->view('template', $data);

	}

	function delete_postssetting() {

        if($this->input->post('uniqueid') == NULL)
        {
            show_error('uniqueid = 0');
        }
        
        $id = $this->input->post('uniqueid');

        if($this->General_model->deleteTableBasedOnTableAndID($id, 'berita', 'id_berita')){
            echo SUCCESS_DELETE;
        }else{
            echo NO_CHANGE_SAVE;
        }
    }

	function update_postssetting() {

        try{

            $config = array(
                'upload_path'   =>  'uploadResource/img/',
                'allowed_types' => 'gif|jpg|png',
                'max_size'      => '5000',
                'max_width'     => '1024',
                'max_height'    => '768',
                'encrypt_name'  => true,
            );

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('postsImage')) {

                $upload_data = $this->upload->data();
            
                $data_ary = array(
                    'title'     => $upload_data['client_name'],
                    'file'      => $upload_data['file_name'],
                    'width'     => $upload_data['image_width'],
                    'height'    => $upload_data['image_height'],
                    'type'      => $upload_data['image_type'],
                    'size'      => $upload_data['file_size'],
                    'path'      => $upload_data['full_path'],
                    'date'      => time()
                );

                $postsImageFilePath = $config["upload_path"].$data_ary['file'];    
			}          
			
			
            if ($this->upload->do_upload('postsThumbnail')) {

                $upload_data = $this->upload->data();
            
                $data_ary = array(
                    'title'     => $upload_data['client_name'],
                    'file'      => $upload_data['file_name'],
                    'width'     => $upload_data['image_width'],
                    'height'    => $upload_data['image_height'],
                    'type'      => $upload_data['image_type'],
                    'size'      => $upload_data['file_size'],
                    'path'      => $upload_data['full_path'],
                    'date'      => time()
                );

                $postsThumbnailFilePath = $config["upload_path"].$data_ary['file'];    
            }         

			$newsTitle = $this->input->post('newsTitle');
			$seoTitle = $this->input->post('seoTitle');
			$author = $this->input->post('author');
			$editor = $this->input->post('editor');
			$tags = $this->input->post('tags');
			$postsContent = $this->input->post('postsContent');
			$leadinberita = $this->input->post('leadinberita');
			$publisheddate = $this->input->post('publisheddate');
			$imagecaption = $this->input->post('imagecaption');
			
            // echo $productName;

            if($this->input->post('postsid') !== NULL)
                $postsid = $this->input->post('postsid');		
			
			setlocale(LC_TIME, "id_ID");
            $newData = array(
				'judul_seo'=>$seoTitle,	
				'editor'=>$editor,	
				'detail_berita'=>$postsContent,
				'author'=>$author,
				'status_headline'=>1,
				'status_berita'=>'published',
				'leadin_berita'=>$leadinberita,
				'hari'=>date_format(new DateTime($publisheddate),'l'),
				'tgl_publish'=>date_format(new DateTime($publisheddate),'Y-m-d'),
				'judul_berita'=>$newsTitle,
				'tags'=>$tags,
				'img_caption'=>$imagecaption,
				'url'=>'',
				'views'=>0,
                'last_user'=>$this->session->userdata('username'),
                'last_update'=>date('Y-m-d h:i:s')
            );

            if(isset($postsImageFilePath)){
                $newData = array_merge($newData, array(
                    'img_path'=>$postsImageFilePath
                ));
			}

            if(isset($postsThumbnailFilePath)){
                $newData = array_merge($newData, array(
                    'thumbnail_path'=>$postsThumbnailFilePath
                ));
            }

            // echo $data_ary['size'];
            
            if(strlen($postsid) > 0){		
                $newData = array_merge($newData, array(
                    'id_berita'=>$postsid
                ));
                if($this->Posts_model->saveUpdatePostsSetting($newData)){
                    echo SUCCESS_SAVE;
                }else{
                    echo FAILED_SAVE;
                }
            }
            else{
                if($this->Posts_model->saveInsertPostsSetting($newData)){
                    echo SUCCESS_SAVE;
                }else{
                    echo FAILED_SAVE;
                }
            }
            

        }catch(Exception $exp){
            echo $exp;
        }
	}


	
}
	
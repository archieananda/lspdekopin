<?php
/**
 * Product Class
 *
 * Archie Ananda
 */
class Product extends CI_Controller {
    public function  __construct()
	{
        parent::__construct();
        if ($this->session->userdata('login') == FALSE)
		{
			redirect('login');	
        }        

        $this->load->model('Product_model', '', TRUE);
        $this->load->model('General_model', '', TRUE);        
        //declare helper web_helper
        $this->load->helper('Web_helper');
        $this->load->helper(array('form', 'url'));

        $this->load->model('Login_model', '', TRUE);        
        if($this->Login_model->get_userlevel_by_username($this->session->userdata('username')) != USER_LEVEL_ADMIN )
        {
            show_error("Forbidden Access");
        }
    }
    
	var $limit = 10;
	var $title = 'Product Setting';

    function index()
	{
		$this->product_setting();
    }

    function product_setting(){

		$data['title'] = $this->title;
        $data['h2_title'] = '<i class="glyphicon glyphicon-cog"></i> Product Setting';
        $data['main_view'] = 'products/productsetting_view';
        //notes: getDataTableScriptSource declared in helpers directory, inside web_helper
        $data['extraHelperScript'] = getDataTableScriptSource();
        $data['extraHelperCss'] = getDataTableCssSource();
        $data['extraScript'] = array('product/product-setting.js');
        $data['prodListData'] = json_encode($this->Product_model->productSettingList());

		$this->load->view('template', $data);
    }

    function product_setting_add(){
        $data['title'] = $this->title;
        $data['h2_title'] = '<i class="glyphicon glyphicon-cog"></i> Product Setting > Add';
        $data['main_view'] = 'products/productsetting_edit_view';
        $data['form_action']	= site_url('product/setting/add/insert');
        $data['extraHelperScript'] = array('tinymce/tinymce.min.js');
        $data['extraScript'] = array('product/product-setting-edit.js');
        $data['productTypeList'] = $this->Product_model->getProductType();

		$this->load->view('template', $data);
    }

    function product_setting_edit($id=0){
       
        $data['title'] = $this->title;
        $data['h2_title'] = '<i class="glyphicon glyphicon-cog"></i> Product Setting > Edit';
        $data['main_view'] = 'products/productsetting_edit_view';
        $data['form_action']	= site_url('product/setting/edit/update');
        $data['extraHelperScript'] = array('tinymce/tinymce.min.js');
        $data['extraScript'] = array('product/product-setting-edit.js');
        $data['productTypeList'] = $this->Product_model->getProductType();
        $data['productData'] = $this->Product_model->getProductSetting($id);

		$this->load->view('template', $data);
    }
    
	function update_productsetting() {

        try{

            $config = array(
                'upload_path'   =>  'uploadResource/img/',
                'allowed_types' => 'gif|jpg|png',
                'max_size'      => '5000',
                'max_width'     => '1024',
                'max_height'    => '768',
                'encrypt_name'  => true,
            );

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('productImage')) {

                $upload_data = $this->upload->data();
            
                $data_ary = array(
                    'title'     => $upload_data['client_name'],
                    'file'      => $upload_data['file_name'],
                    'width'     => $upload_data['image_width'],
                    'height'    => $upload_data['image_height'],
                    'type'      => $upload_data['image_type'],
                    'size'      => $upload_data['file_size'],
                    'path'      => $upload_data['full_path'],
                    'date'      => time()
                );

                $productImageFilePath = $config["upload_path"].$data_ary['file'];    
            }            

            $productName = $this->input->post('productName');
            $productType = $this->input->post('productType');
            $productStatus = $this->input->post('productStatus');
            $productContent = $this->input->post('productContent');

            // echo $productName;

            if($this->input->post('productid') !== NULL)
                $productid = $this->input->post('productid');		
            
            $newData = array(
                'ProductName'=>$productName,	
                'ProductType'=>$productType,
                'HTMLContent'=>$productContent,  
                'IsActive'=>($productStatus) ? 1 : 0,
                'UserUpdate'=>$this->session->userdata('username'),
                'LastupdateDate'=>date('Y-m-d h:i:s')
            );

            if(isset($productImageFilePath)){
                $newData = array_merge($newData, array(
                    'ImagePath'=>$productImageFilePath
                ));
            }

            // echo $data_ary['size'];
            
            if(strlen($productid) > 0){		
                $newData = array_merge($newData, array(
                    'ProductID'=>$productid
                ));
                if($this->Product_model->saveUpdateProductSetting($newData)){
                    echo SUCCESS_SAVE;
                }else{
                    echo FAILED_SAVE;
                }
            }
            else{
                if($this->Product_model->saveInsertProductSetting($newData)){
                    echo SUCCESS_SAVE;
                }else{
                    echo FAILED_SAVE;
                }
            }
            

        }catch(Exception $exp){
            echo $exp;
        }
	}

    function product_rate_setting($prodID = 0){

        if($prodID == 0)
            show_404();

		$data['title'] = $this->title;
        $data['h2_title'] = '<i class="glyphicon glyphicon-wrench"></i> Product Rate Setting';
        $data['main_view'] = 'products/productratesetting_view';
        //notes: getDataTableScriptSource declared in helpers directory, inside web_helper
        $data['extraHelperScript'] = getDataTableScriptSource();
        $data['extraHelperCss'] = getDataTableCssSource();
        $data['extraScript'] = array('product/productrate-setting.js');
        $data['productData'] = $this->Product_model->getProductSetting($prodID);
        $data['prodRateListData'] = json_encode($this->Product_model->productRateSettingList($prodID));

		$this->load->view('template', $data);
    }

    function product_rate_setting_add($prodID = 0){
        
        if($prodID == 0)
            show_404();

        $data['title'] = $this->title;
        $data['h2_title'] = '<i class="glyphicon glyphicon-cog"></i> Product Rate Setting > Add';
        $data['main_view'] = 'products/productratesetting_edit_view';
        $data['form_action']	= site_url('product/setting/rate/add/insert');
        $data['extraScript'] = array('product/productrate-setting-edit.js');
        // $data['productId'] = $prodID;        
        $data['productData'] = $this->Product_model->getProductSetting($prodID);
        $data['tenorList'] = $this->Product_model->getProductTenor();
        $data['plafondList'] = $this->Product_model->getPlafondScheme();

		$this->load->view('template', $data);
    }

    function product_rate_setting_edit($rateid = 0){
        
        if($rateid == 0)
            show_404();

        $data['title'] = $this->title;
        $data['h2_title'] = '<i class="glyphicon glyphicon-cog"></i> Product Rate Setting > Add';
        $data['main_view'] = 'products/productratesetting_edit_view';
        $data['form_action']	= site_url('product/setting/rate/edit/update');
        $data['extraScript'] = array('product/productrate-setting-edit.js');
        $dataProdDetail = $this->Product_model->getProductRateSetting($rateid);
        $data['productRateData'] = $dataProdDetail;
        // $data['productRateData'] = $this->Product_model->getProductSetting($rateid);

        $data['productData'] = $this->Product_model->getProductSetting($dataProdDetail->ProductID);
        
        $data['tenorList'] = $this->Product_model->getProductTenor();
        $data['plafondList'] = $this->Product_model->getPlafondScheme();

		$this->load->view('template', $data);
    }

    
	function update_productratesetting() {

        if($this->input->post('productid') == NULL)
        {
            show_error('productid = 0');
        }

        $productid = $this->input->post('productid');
        $tenure = $this->input->post('tenure');
        $plafond = $this->input->post('plafond');
        $effectiverate = $this->input->post('effectiverate');
        $flatrate = $this->input->post('flatrate');
        $productRateStatus = $this->input->post('productRateStatus');

		if($this->input->post('productrateid') !== NULL)
            $productrateid = $this->input->post('productrateid');		

		$newData = array(
            'ProductID'=>$productid,
            'TennorID'=>$tenure,
            'PlafondID'=>$plafond,
            'EffectiveRate'=>$effectiverate,
            'FlatRate'=>$flatrate,
            'IsActive'=>($productRateStatus) ? 1 : 0,
            'UserUpdate'=>$this->session->userdata('username'),
            'LastUpdateDate'=>date('Y-m-d h:i:s')
		);
		
		if(strlen($productrateid) > 0){		
			$newData = array_merge($newData, array(
				'ProductRateID'=>$productrateid
            ));
            
			if($this->Product_model->saveUpdateProductRateSetting($newData)){
                echo SUCCESS_SAVE;
            }else{
                echo NO_CHANGE_SAVE;
            }
		}
		else{
			if($this->Product_model->saveInsertProductRateSetting($newData)){
                echo SUCCESS_SAVE;
            }else{
                echo NO_CHANGE_SAVE;
            }
		}
    }
    
    function delete_productratesetting() {

        if($this->input->post('uniqueid') == NULL)
        {
            show_error('uniqueid = 0');
        }
        
        $id = $this->input->post('uniqueid');

        if($this->General_model->deleteTableBasedOnTableAndID($id, 'm_productrate', 'ProductRateID')){
            echo SUCCESS_DELETE;
        }else{
            echo NO_CHANGE_SAVE;
        }
    }

}
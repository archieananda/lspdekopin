<?php
/**
 * Sales Class
 *
 * Archie Ananda
 */
class Sales extends CI_Controller {
    public function  __construct()
	{
        parent::__construct();
        if ($this->session->userdata('login') == FALSE)
		{
			redirect('login');	
		}
        $this->load->model('Sales_model', '', TRUE);
        $this->load->model('General_model', '', TRUE);        
        //declare helper Web_helper
        $this->load->helper('Web_helper');

        $this->load->model('Login_model', '', TRUE);        
        if($this->Login_model->get_userlevel_by_username($this->session->userdata('username')) != USER_LEVEL_ADMIN )
        {
            show_error("Forbidden Access");
        }
    }

    var $limit = 10;
    var $title = 'Sales Setting';

    function index()
	{
		$this->sales_setting();
    }

    function sales_setting(){

		$data['title'] = $this->title;
        $data['h2_title'] = '<i class="glyphicon glyphicon-cog"></i> '.$this->title;
        $data['main_view'] = 'sales/salessetting_view';
        //notes: getDataTableScriptSource declared in helpers directory, inside Web_helper
        $data['extraHelperScript'] = getDataTableScriptSource();
        $data['extraHelperCss'] = getDataTableCssSource();
        $data['extraScript'] = array('sales/sales-setting.js');
        $data['salesListData'] = json_encode($this->Sales_model->salesSettingList());

		$this->load->view('template', $data);
    }

    function sales_setting_add(){
        $data['title'] = $this->title;
        $data['h2_title'] = '<i class="glyphicon glyphicon-cog"></i> '.$this->title.' > Add';
        $data['main_view'] = 'sales/salessetting_edit_view';
        $data['form_action']	= site_url('sales/setting/add/insert');
        $data['extraScript'] = array('sales/sales-setting-edit.js');

		$this->load->view('template', $data);
    }

    function sales_setting_edit($id=0){
        $data['title'] = $this->title;
        $data['h2_title'] = '<i class="glyphicon glyphicon-cog"></i> '.$this->title.' > Edit';
        $data['main_view'] = 'sales/salessetting_edit_view';
        $data['form_action']	= site_url('sales/setting/edit/update');
        $data['extraScript'] = array('sales/sales-setting-edit.js');
        $data['salesData'] = $this->Sales_model->getSalesSetting($id);

		$this->load->view('template', $data);
    }

    function update_salessetting() {
        $salesname = $this->input->post('salesname');
        $salescontact = $this->input->post('salescontact');
        $salesstatus = $this->input->post('salesstatus');

		if($this->input->post('salesid') !== NULL)
            $salesid = $this->input->post('salesid');		
        
		$newData = array(
			'salesname'=>$salesname,	
			'salescontact'=>$salescontact,
			'IsActive'=>($salesstatus) ? 1 : 0,
            'UserUpdate'=>$this->session->userdata('username'),
            'LastupdateDate'=>date('Y-m-d h:i:s')
		);
		
		if(strlen($salesid) > 0){		
			$newData = array_merge($newData, array(
				'SalesID'=>$salesid
			));
			if($this->Sales_model->saveUpdateSalesSetting($newData)){
                echo SUCCESS_SAVE;
            }else{
                echo NO_CHANGE_SAVE;
            }
		}
		else{
			if($this->Sales_model->saveInsertSalesSetting($newData)){
                echo SUCCESS_SAVE;
            }else{
                echo NO_CHANGE_SAVE;
            }
		}
    }
    
    function delete_salessetting() {

        if($this->input->post('uniqueid') == NULL)
        {
            show_error('uniqueid = 0');
        }
        
        $id = $this->input->post('uniqueid');

        if($this->General_model->deleteTableBasedOnTableAndID($id, 'm_sales', 'SalesID')){
            echo SUCCESS_DELETE;
        }else{
            echo NO_CHANGE_SAVE;
        }
    }
}
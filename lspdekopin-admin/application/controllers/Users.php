<?php
/**
 * Users Class
 *
 * Archie Ananda
 */
class Users extends CI_Controller {

	public function  __construct()
	{
		parent::__construct();
		if ($this->session->userdata('login') == FALSE)
		{
			redirect('login');	
		}
		$this->load->model('Users_model', '', TRUE);
		$this->load->model('general_model', '', TRUE);
		//declare helper web_helper
        $this->load->helper('Web_helper');
		$this->load->helper(array('form', 'url'));
		
        $this->load->model('Login_model', '', TRUE);        
        if($this->Login_model->get_userlevel_by_username($this->session->userdata('username')) != USER_LEVEL_ADMIN )
        {
            show_error("Forbidden Access");
        }
	}

	var $limit = 10;
	var $title = 'Users';
	

	function index()
	{
		$this->tampil_users();
	}

	function tampil_users(){

		$data['title'] = $this->title;
		$data['h2_title'] = '<i class="glyphicon glyphicon-user"></i> Users Management';
		$data['main_view'] = 'users/users_view';
		
		$data['extraHelperScript'] = getDataTableScriptSource();
        $data['extraHelperCss'] = getDataTableCssSource();
		$data['extraScript'] = array('users/users-view.js');
		
		$data['isi'] = json_encode($this->Users_model->daftarUsers());
		$this->load->view('template', $data);
	}

	function add() {
		
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= '<i class="glyphicon glyphicon-user"></i> Users > Add user';
		$data['main_view'] 		= 'users/users_form';
		$data['form_action']	= site_url('users/setting/add/insert');
		$data['extraScript'] = array('users/users-form.js');
		$data['roleTypeList'] = $this->Users_model->getRoleType();
		$data['productTypeList'] = $this->general_model->getSettingByParameter('ProductType');
		$data['userLeaderList'] = $this->Users_model->getUserDropDownByLevelID('3');

		$this->load->view('template', $data);
	
	}

	function user_edit($id = 0) {
		
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= '<i class="glyphicon glyphicon-user"></i> Users > Edit user';
		$data['main_view'] 		= 'users/users_form';
		$data['form_action']	= site_url('users/setting/edit/update');
		$data['userData'] = $this->Users_model->getUserData($id);
		$data['roleTypeList'] = $this->Users_model->getRoleType();
		$data['productTypeList'] = $this->general_model->getSettingByParameter('ProductType');
		$data['extraScript'] = array('users/users-form.js');
		$data['userLeaderList'] = $this->Users_model->getUserDropDownByLevelID('3');		

		$this->load->view('template', $data);

	}

	function update_user() {
		$username = $this->input->post('username');
		$name = $this->input->post('name');
		$password = sha1( $this->input->post('password'));
		$email = $this->input->post('email');
		$userstatus = $this->input->post('userstatus');
		$userlevel = $this->input->post('userlevel');
		$phone = $this->input->post('phone');
		$producttype = $this->input->post('producttype');
		$leader = $this->input->post('leader');

		if($this->input->post('userid') !== NULL)
			$userid = $this->input->post('userid');		

		$newData = array(
			'user_nama'=>$name,	
			'user_username'=>$username,
			'user_email'=>$email,  
			'user_level'=>$userlevel,
			'user_status'=>($userstatus) ? 1 : 0,
			'user_telp'=>$phone,
			'user_parent'=>$leader,
			'ProductTypeID'=>$producttype
		);

		if(strlen($password) > 0){
			$newData = array_merge($newData, array(
				'user_password'=>$password
			));
		}

		if(isset($userid) && $userid > 0){		
			$newData = array_merge($newData, array(
				'user_id'=>$userid
			));
			// echo $this->Users_model->saveUpdateUser($newData);
			if($this->Users_model->saveUpdateUser($newData)){
				echo SUCCESS_SAVE;
			}else{
				echo NO_CHANGE_SAVE;
			}
		}
		else{
			// echo print_r($this->Users_model->saveInsertUser($newData));
			if($this->Users_model->saveInsertUser($newData)){
				echo SUCCESS_SAVE;
			}else{
				echo NO_CHANGE_SAVE;
			}
		}
	}
}
	
<?php

/**
 * AppCustomer Class
 *
 * Archie Ananda
 */
class AppCustomer extends CI_Controller {
    public function  __construct()
	{
        parent::__construct();        
        if ($this->session->userdata('login') == FALSE)
		{
			redirect('login');	
		}
        
        $this->load->helper('url');
        $this->load->model('appCustomer_model', '', TRUE);
        $this->load->model('general_model', '', TRUE);    
        $this->load->model('users_model', '', TRUE);  
        $this->load->model('product_model', '', TRUE);    
        //declare helper web_helper
        $this->load->helper('web_helper');
    }

    var $limit = 10;
    var $title = 'Customer Application';

    function index()
	{
        $data['title'] = $this->title;
        $data['h2_title'] = '<i class="glyphicon glyphicon-duplicate"></i> '.$this->title;
        $data['main_view'] = 'appCustomer/inquiry_appCustomer_view';
        //notes: getDataTableScriptSource declared in helpers directory, inside web_helper
        $data['extraHelperScript'] = getDataTableScriptSource();
        $data['extraHelperCss'] = getDataTableCssSource();
        $data['extraScript'] = array('appCustomer/appcustomer-inquiry.js');
        $data['appCustListData'] = json_encode($this->appCustomer_model->appCustomerList(null));
        
        $userData = $this->general_model->getUserByUserName($this->session->userdata('username'));
        if($userData->ProductTypeID != null){
            $data['productTypeList'] = $this->general_model->getSettingByParameterWithCond('ProductType', $userData->ProductTypeID);
        }else{
            $data['productTypeList'] = $this->general_model->getSettingByParameter('ProductType');
        }

        //if user level = 3 (leader)
        if($userData->user_level == 3){
            $data['salesList'] = $this->users_model->getUserDropDownByParentID($userData->user_id);
            $data['leaderID'] = $userData->user_id;
        }

        //if user level = 4 (sales)
        if($userData->user_level == 4){
            $data['SalesID'] = $userData->user_id;
            $data['SalesName'] = $userData->user_nama;
        }

        $data['applicationStatusList'] = $this->general_model->getSettingByParameter('ApplicationStatus');

		$this->load->view('template', $data);
    }

    function getAppCustomerList()
    {
        $fullname = $this->input->post("fullname");
        $ktpno = $this->input->post("ktpno");
        $appstatus = $this->input->post("appstatus");
        $producttypeid = $this->input->post("producttypeid");
        $productname = $this->input->post("productname");
        $productname = $this->input->post("productname");
        $trxfrom = $this->input->post("trxfrom");
        $trxto = $this->input->post("trxto");
        $salesid = $this->input->post("salesid");
        $leaderid = $this->input->post("leaderid");

        $newData = array(
            'fullname'=>$fullname,
            'ktpno'=>$ktpno,
            'appstatus'=>$appstatus,
            'productname'=>$productname,
            'producttypeid'=>$producttypeid,
            'trxfrom'=>$trxfrom,
            'trxto'=>$trxto,
            'salesid'=>$salesid,
            'leaderid'=>$leaderid
        );

        echo json_encode(
            array('code' => 'OK', 'data' => $this->appCustomer_model->appCustomerList($newData))
        );
    }

    function folowUpCustEdit($transID)
    {
        if($transID == 0)
            show_404();

        $data['title'] = $this->title;
        $data['h2_title'] = '<i class="glyphicon glyphicon-earphone"></i> '.$this->title.' > Follow Up';
        $data['main_view'] = 'appCustomer/followup_appCustomer_edit_view';
        $data['extraScript'] = array('appCustomer/followup-appcustomer-edit.js');
        $data['form_action']	= site_url('appcustomer/followup/save');
        $data['channelList'] = $this->general_model->getSettingByParameter('Channel');
        $data['productTypeList'] = $this->general_model->getSettingByParameter('ProductType');
        $data['residenceStatusList'] = $this->general_model->getSettingByParameter('ResidenceStatus');
        $data['educationList'] = $this->general_model->getSettingByParameter('Education');
        $data['occupationList'] = $this->general_model->getSettingByParameter('Occupation');
        $data['relationshipList'] = $this->general_model->getSettingByParameter('Relationship');
        $data['productSettingList'] = $this->product_model->productSettingList(array('IsActive' => '1'));
        $data['cardTypeList'] = $this->general_model->getSettingByParameter('CardType');
        $data['CCBankList'] = $this->general_model->getSettingByParameter('BankSetting');
        $data['followupStatusList'] = $this->general_model->getSettingByParameter('FollowupStatus');
        $data['mailingAddressTypeList'] = $this->general_model->getSettingByParameter('MailingAddressType');
        $data['bankPayrolList'] = $this->general_model->getSettingByParameter('BankSetting');
        $data['monthList'] = $this->general_model->getMonthDropDown();
        $data['customerTransData'] = $this->appCustomer_model->getCustomerTransactionByID($transID);
        $data['userLoginData'] = $this->general_model->getUserByUserName($this->session->userdata('username'));

		$this->load->view('template', $data);
    }


    function saveAssign(){
        $transids = json_decode($this->input->post("transids"));
        $salesid = $this->input->post("salesid");

        $newData = array(
            'TransIDs'=>$transids,
            'SalesID'=>$salesid
        );

        $this->appCustomer_model->saveAssign($newData);

        echo SUCCESS_SAVE_ASSIGN;
    }


    function folowUpSaveJunk(){
        if($this->input->post('transid') == NULL)
        {
            show_error('transid = 0');
            return;
        }

        $transid = $this->input->post('transid');
        $newData = array(
            'TransID'=>$transid,
            'StatusPengajuan'=>3
        );

        $this->appCustomer_model->saveFollowupTransactionData($newData, 'trans_h_pengajuan');

        echo SUCCESS_SAVE_JUNK;
    }

    function pickUpCustEdit($transID){

        if($transID == 0)
            show_404();

        $data['title'] = $this->title;
        $data['h2_title'] = '<i class="glyphicon glyphicon-briefcase"></i> '.$this->title.' > Pick Up';
        $data['main_view'] = 'appCustomer/pickup_appCustomer_edit_view';
        $data['process_info_view'] = 'appCustomer/appCustomer_processInfo_view';
        $data['extraScript'] = array('appCustomer/pickup-appcustomer-edit.js');
        $data['form_action']	= site_url('appcustomer/pickup/save');
        $data['pickupStatusList'] = $this->general_model->getSettingByParameter('PickupStatus');
        $data['customerTransData'] = $this->appCustomer_model->getCustomerTransactionByID($transID);

        $this->load->view('template', $data);
    }

    function bankSubmitCustEdit($transID){

        if($transID == 0)
            show_404();

        $data['title'] = $this->title;
        $data['h2_title'] = '<i class="glyphicon glyphicon-share-alt"></i> '.$this->title.' > Bank Submit';
        $data['main_view'] = 'appCustomer/banksubmit_appCustomer_edit_view';
        $data['process_info_view'] = 'appCustomer/appCustomer_processInfo_view';
        $data['extraScript'] = array('appCustomer/banksubmit-appcustomer-edit.js');
        $data['form_action']	= site_url('appcustomer/banksubmit/save');
        $data['customerTransData'] = $this->appCustomer_model->getCustomerTransactionByID($transID);

        $this->load->view('template', $data);
    }

    function approvalResultBankCustEdit($transID){

        if($transID == 0)
            show_404();

        $data['title'] = $this->title;
        $data['h2_title'] = '<i class="glyphicon glyphicon-briefcase"></i> '.$this->title.' > Approval Result Bank';
        $data['main_view'] = 'appCustomer/approvalresult_appCustomer_edit_view';
        $data['process_info_view'] = 'appCustomer/appCustomer_processInfo_view';
        $data['extraScript'] = array('appCustomer/approvalresult-appcustomer-edit.js');
        $data['form_action']	= site_url('appcustomer/bankapproval/save');
        $data['approvalResultList'] = $this->general_model->getSettingByParameter('ApprovalResult');        
        $data['rejectReasonList'] = $this->general_model->getSettingByParameter('RejectReason');   
        $data['customerTransData'] = $this->appCustomer_model->getCustomerTransactionByID($transID);

        $this->load->view('template', $data);
    }

    function viewDetailCust($transID){
        if($transID == 0)
            show_404();

        $data['title'] = $this->title;
        $data['h2_title'] = '<i class="glyphicon glyphicon-search"></i> '.$this->title.' > View Detail';
        $data['main_view'] = 'appCustomer/viewDetail_appCustomer_view';
        $data['process_info_view'] = 'appCustomer/appCustomer_processInfo_view';
        // $data['extraScript'] = array('appCustomer/approvalresult-appcustomer-edit.js');
        // $data['form_action']	= site_url('appcustomer/bankapproval/save');
        $data['customerTransData'] = $this->appCustomer_model->getCustomerTransactionByID($transID);

        $this->load->view('template', $data);
    }

    function folowUpSaveToDataEntry(){
        if($this->input->post('transid') == NULL)
        {
            show_error('transid = 0');
            return;
        }

        $transid = $this->input->post('transid');
        $newData = array(
            'TransID'=>$transid,
            'StatusPengajuan'=>4,
            'DataEntryDate'=> date('Y-m-d H:i:s'),
            'UserUpdate'=>$this->session->userdata('username'),
            'LastUpdateDate'=>date('Y-m-d h:i:s')
        );

        $this->appCustomer_model->saveFollowupTransactionData($newData, 'trans_h_pengajuan');

        $this->appCustomer_model->insertTransactionHistory(
            $transid,4,'',$newData['UserUpdate'],$newData['LastUpdateDate']);

        echo SUCCESS_SAVE_DATAENTRY;
    }

    function bankSubmitSave(){
        if($this->input->post('transid') == NULL)
        {
            show_error('transid = 0');
            return;
        }

        $transid = $this->input->post('transid');
        $banksubmitnotes = $this->input->post('banksubmitnotes');

        $newData = array(
            'TransID'=>$transid,
            'StatusPengajuan'=>7,
            'BankSubmitNotes'=>$banksubmitnotes,
            'BankSubmitDate'=> date('Y-m-d H:i:s'),
            'UserUpdate'=>$this->session->userdata('username'),
            'LastUpdateDate'=>date('Y-m-d h:i:s')
        );

        $this->appCustomer_model->saveFollowupTransactionData($newData, 'trans_h_pengajuan');
        
        $this->appCustomer_model->insertTransactionHistory(
            $transid,7,$newData['BankSubmitNotes'],$newData['UserUpdate'],$newData['LastUpdateDate']);

        echo SUCCESS_BANK_SUBMIT;
    }

    function pickUpSave(){
        if($this->input->post('transid') == NULL)
        {
            show_error('transid = 0');
            return;
        }

        $transid = $this->input->post('transid');
        $pickupby = $this->input->post('pickupby');
        $pickupnotes = $this->input->post('pickupnotes');
        $pickupstatus = $this->input->post('pickupstatus');        

        $newData = array(
            'TransID'=>$transid,
            'StatusPengajuan'=>5,
            'PickupStatus'=>$pickupstatus,
            'PickupBy'=>$pickupby,
            'PickupNotes'=>$pickupnotes,
            'PickUpStatusDate'=> date('Y-m-d H:i:s'),
            'UserUpdate'=>$this->session->userdata('username'),
            'LastUpdateDate'=>date('Y-m-d h:i:s')
        );

        $this->appCustomer_model->saveFollowupTransactionData($newData, 'trans_h_pengajuan');
        
        $this->appCustomer_model->insertTransactionHistory(
            $transid,5,$newData['PickupNotes'],$newData['UserUpdate'],$newData['LastUpdateDate']);


        echo SUCCESS_SAVE_PICKUP;
    }

    function entryDoneSave(){
        if($this->input->post('transid') == NULL)
        {
            show_error('transid = 0');
            return;
        }

        $transid = $this->input->post('transid');        

        $newData = array(
            'TransID'=>$transid,
            'StatusPengajuan'=>6,
            'EntryDoneDate'=> date('Y-m-d H:i:s'),
            'UserUpdate'=>$this->session->userdata('username'),
            'LastUpdateDate'=>date('Y-m-d h:i:s')
        );

        $this->appCustomer_model->saveFollowupTransactionData($newData, 'trans_h_pengajuan');
        
        $this->appCustomer_model->insertTransactionHistory(
            $transid,6,'',$newData['UserUpdate'],$newData['LastUpdateDate']);

        echo SUCCESS_SAVE_ENTRYDONE;
    }

    function bankResultSave(){
        if($this->input->post('transid') == NULL)
        {
            show_error('transid = 0');
            return;
        }

        $transid = $this->input->post('transid');        
        $approvalresult = $this->input->post('approvalresult');        
        $rejectreason = $this->input->post('rejectreason');        
        $resultbanknotes = $this->input->post('resultbanknotes');

        $newData = array(
            'TransID'=>$transid,
            'ApprovalResult'=>$approvalresult,
            'UserUpdate'=>$this->session->userdata('username'),
            'LastUpdateDate'=>date('Y-m-d h:i:s'),
            'ResultBankNotes'=>$resultbanknotes
        );

        if($approvalresult == "APP"){
            $newData = array_merge($newData, array(
                'StatusPengajuan'=>9,
                'ApprovalDate'=> date('Y-m-d H:i:s')
            ));
        }else if($approvalresult == "REJ"){
            $newData = array_merge($newData, array(
                'StatusPengajuan'=>8,
                'RejectDate'=> date('Y-m-d H:i:s'),
                'RejectReason'=>$rejectreason
            ));
        }else{
            echo "Data yang diinput error.";
            return;
        }

        $this->appCustomer_model->saveFollowupTransactionData($newData, 'trans_h_pengajuan');
        
        $this->appCustomer_model->insertTransactionHistory(
            $transid,9,$newData['ResultBankNotes'],$newData['UserUpdate'],$newData['LastUpdateDate']);

        echo SUCCESS_SAVE_BANK_RESULT;
    }

    function backToFollowUpSave(){
        if($this->input->post('transid') == NULL)
        {
            show_error('transid = 0');
            return;
        }

        $transid = $this->input->post('transid');        

        $newData = array(
            'TransID'=>$transid,
            'StatusPengajuan'=>2,
            'UserUpdate'=>$this->session->userdata('username'),
            'LastUpdateDate'=>date('Y-m-d h:i:s')
        );

        $this->appCustomer_model->saveFollowupTransactionData($newData, 'trans_h_pengajuan');

        
        $this->appCustomer_model->insertTransactionHistory(
            $transid,2,'',$newData['UserUpdate'],$newData['LastUpdateDate']);

        echo SUCCESS_SAVE_BACK_TO_FOLLOWUP;
    }

    function folowUpSave(){
        if($this->input->post('transid') == NULL)
        {
            show_error('transid = 0');
            return;            
        }

        //General
        $transid = $this->input->post('transid');
        $salesid = $this->input->post('salesid');

        //Product Info
        $producttype = $this->input->post('producttype');
        $productid = $this->input->post('productid');
        $plafond = $this->input->post('plafond');
        $effrate = $this->input->post('effrate');
        $bankchannel = $this->input->post('bankchannel');
        $plafond = $this->input->post('plafond');
        $flatrate = $this->input->post('flatrate');
        $tenor = $this->input->post('tenor');

        //Existing Credit Card
        $cctype = $this->input->post('cctype');
        $cclimit = $this->input->post('cclimit');
        $ccexpiredmonth = $this->input->post('ccexpiredmonth');
        $ccexpiredyear = $this->input->post('ccexpiredyear');
        $ccbankid = $this->input->post('ccbankid');
        $ccactivemonth = $this->input->post('ccactivemonth');
        $ccactiveyear = $this->input->post('ccactiveyear');

        //Personal Information
        $custfullname = $this->input->post('custfullname');
        $custmobileno = $this->input->post('custmobileno');
        $custhomephone = $this->input->post('custhomephone');
        $custemailaddress = $this->input->post('custemailaddress');
        $custeducation = $this->input->post('custeducation');
        $custresidencestatus = $this->input->post('custresidencestatus');
        $custmothersurename = $this->input->post('custmothersurename');
        $custlivingperiod = $this->input->post('custlivingperiod');
        $custdependents = $this->input->post('custdependents');
        $address = $this->input->post('address');
        $ktpno = $this->input->post('ktpno');

        //Occupation / Business Information
        $custoccupation = $this->input->post('custoccupation');
        $custcompanyname = $this->input->post('custcompanyname');
        $custcorebusiness = $this->input->post('custcorebusiness');
        $companyaddress = $this->input->post('companyaddress');
        $custposition = $this->input->post('custposition');
        $custdivision = $this->input->post('custdivision');
        $custworkingperiod = $this->input->post('custworkingperiod');
        $businessphone = $this->input->post('businessphone');
        $extbphone = $this->input->post('extbphone');
        $monthlyincomeamount = $this->input->post('monthlyincomeamount');
        $bankpayrol = $this->input->post('bankpayrol');

        //Emergency Contact Information
        $relationtype = $this->input->post('relationtype');
        $ecfullname = $this->input->post('ecfullname');
        $ecphone = $this->input->post('ecphone');
        $ecaddress = $this->input->post('ecaddress');
        
        //Document Information
        $hasktp = $this->input->post('hasktp');
        $hasnpwp = $this->input->post('hasnpwp');
        $hasreklistrik = $this->input->post('hasreklistrik');
        $hasKK = $this->input->post('hasKK');
        $hascc = $this->input->post('hascc');
        $hasbankacc = $this->input->post('hasbankacc');
        $mailingaddresstype = $this->input->post('mailingaddresstype');

        //Survey information        
        $surveydate = $this->input->post('surveydate');
        $actualsurveydate = '';
        $actualsurveytime = '';

        if(isset($surveydate) && $surveydate != null){
            $actualsurveydate = DateTime::createFromFormat('l, d/m/Y H:i:s', $surveydate)->format('Y-m-d');
            $actualsurveytime = DateTime::createFromFormat('l, d/m/Y H:i:s', $surveydate)->format('H:i:s');
        }

        $surveynotes = $this->input->post('surveynotes');
        $surveyaddress = $this->input->post('surveyaddress');

        //Spouse Contact Information
        $spousename = $this->input->post('spousename');
        $spousektp = $this->input->post('spousektp');
        $spousephone = $this->input->post('spousephone');

        //Followup Information
        $followupstatus = $this->input->post('followupstatus');
        $notes = $this->input->post('notes');
        
        //Prepare array for Trans_H_Pengajuan
        $trans_h_pengajuanData = array(
            'TransID'=>$transid,
            'ProductID'=>$productid,
            'SalesID'=>$salesid,
            'Notes'=>$notes,
            'SpouseName'=>$spousename,
            'SpouseKTP'=>$spousektp,
            'SpousePhone'=>$spousephone,
            'HasKTP'=>($hasktp) ? 1 : 0,
            'HasNPWP'=>($hasnpwp) ? 1 : 0,
            'HasKK'=>($hasKK) ? 1 : 0,
            'KTPNumber'=>$ktpno,
            'HasCreditCard'=>($hascc) ? 1 : 0,
            'HasBankAccount'=>($hasbankacc) ? 1 : 0,
            'HasRekListrik'=>($hasreklistrik) ? 1 : 0,
            'MailingAddressType'=>$mailingaddresstype,
            'DateOfSUrvey'=>$actualsurveydate,
            'TimeOfSUrvey'=>$actualsurveytime,
            'SurveyAddress'=>$surveyaddress,
            'SurveyNotes'=>$surveynotes,
            'StatusPengajuan'=>2,
            'FolowupStatus'=>$followupstatus,
            'UserUpdate'=>$this->session->userdata('username'),
            'LastUpdateDate'=>date('Y-m-d h:i:s')
        );

        //Prepare array for Trans_D_Product
        $trans_d_productData = array(
            'TransID'=>$transid,
            'ChannelID'=>$bankchannel,
            'FlatRate'=>$flatrate,
            'EffectiveRate'=>$effrate,
            'TennureMonth'=>$tenor,
            'Plafond'=>$plafond,
            'UserUpdate'=>$this->session->userdata('username'),
            'LastUpdateDate'=>date('Y-m-d h:i:s')
        );

        //Prepare array for Trans_D_DataDiri
        $trans_d_datadiriData = array(
            'TransID'=>$transid,
            'FullName'=>$custfullname,
            'Address'=>$address,
            'HomePhone'=>$custhomephone,
            'MobileNo'=>$custmobileno,
            'MotherSureName'=>$custmothersurename,
            'EmailAddress'=>$custemailaddress,
            'ResidenceStatus'=>$custresidencestatus,
            'LivingPeriod'=>$custlivingperiod,
            'JmlTanggungan'=>$custdependents,
            'EducationID'=>$custeducation,
            'Occupation'=>$custoccupation,
            'CompanyName'=>$custcompanyname,
            'CoreBusiness'=>$custcorebusiness,
            'LamaKerja'=>$custworkingperiod,
            'Position'=>$custposition,
            'Division'=>$custdivision,
            'CompanyAddress'=>$companyaddress,
            'CompanyPhone'=>$businessphone,
            'ExtensionPhone'=>$extbphone,
            'MonthlyIncomeAmount'=>$monthlyincomeamount,
            'BankPayrol'=>$bankpayrol,
            'UserUpdate'=>$this->session->userdata('username'),
            'LastUpdateDate'=>date('Y-m-d h:i:s')
        );

        //Prepare array for trans_d_emergencycontact
        $trans_d_emergencycontactData = array(
            'TransID'=>$transid,
            'RelationshipID'=>$relationtype,
            'FullName'=>$ecfullname,
            'HomeAddress'=>$ecaddress,
            'HomePhone'=>$ecphone,
            'UserUpdate'=>$this->session->userdata('username'),
            'LastUpdateDate'=>date('Y-m-d h:i:s')
        );

        //Prepare array for trans_d_existing_cc
        $trans_d_existing_ccData = array(
            'TransID'=>$transid,
            'CCTypeID'=>$cctype,
            'Limit'=>$cclimit,
            'CCBankID'=>$ccbankid,
            'ActiveDate'=>($ccactivemonth && $ccactiveyear) ? "$ccactiveyear-$ccactivemonth-1" : '',
            'ExpiredDate'=>($ccexpiredmonth && $ccexpiredyear) ? "$ccexpiredyear-$ccexpiredmonth-1" : '',
            'UserUpdate'=>$this->session->userdata('username'),
            'LastUpdateDate'=>date('Y-m-d h:i:s')
         );

        // print_r($trans_d_existing_ccData);

        //Save trans_h_pengajuanData
        $this->appCustomer_model->saveFollowupTransactionData($trans_h_pengajuanData, 'trans_h_pengajuan');

        //Save trans_d_productData
        $this->appCustomer_model->saveFollowupTransactionData($trans_d_productData, 'trans_d_product');

        //Save trans_d_datadiriData
        $this->appCustomer_model->saveFollowupTransactionData($trans_d_datadiriData, 'trans_d_datadiri');

        //Save trans_d_emergencycontactData
        $testRow = $this->appCustomer_model->checkTransTableDataExistsByTransID($transid, 'trans_d_emergencycontact');
        if(count($testRow) == 0){
            //If no row exists, insert one
            $this->appCustomer_model->insertFollowupTransactionData($trans_d_emergencycontactData, 'trans_d_emergencycontact');
        }
        $this->appCustomer_model->saveFollowupTransactionData($trans_d_emergencycontactData, 'trans_d_emergencycontact');

        //Save trans_d_existing_ccData
        $testRow = $this->appCustomer_model->checkTransTableDataExistsByTransID($transid, 'trans_d_existing_cc');
        if(count($testRow) == 0){
            //If no row exists, insert one
            $this->appCustomer_model->insertFollowupTransactionData($trans_d_existing_ccData, 'trans_d_existing_cc');
        }
        $this->appCustomer_model->saveFollowupTransactionData($trans_d_existing_ccData, 'trans_d_existing_cc');
        
        $this->appCustomer_model->insertTransactionHistory(
            $transid,2,$followupstatus,$trans_h_pengajuanData['UserUpdate'],$trans_h_pengajuanData['LastUpdateDate']);

        echo SUCCESS_SAVE;
        return;
    }
}

?>
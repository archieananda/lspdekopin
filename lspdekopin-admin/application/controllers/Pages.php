<?php
/**
 * Pages Class
 *
 * Archie Ananda
 */
class Pages extends CI_Controller {
	/**
	 * Constructor
	 */
	public function  __construct()
	{
		parent::__construct();
		if ($this->session->userdata('login') == FALSE)
		{
			redirect('login');	
		}
		$this->load->model('Pages_model', '', TRUE);
		$this->load->model('General_model', '', TRUE);

		//declare helper web_helper
        $this->load->helper('Web_helper');
		$this->load->helper(array('form', 'url'));
		
		$this->load->model('Login_model', '', TRUE);        
        if($this->Login_model->get_userlevel_by_username($this->session->userdata('username')) != USER_LEVEL_ADMIN )
        {
            show_error("Forbidden Access");
        }
	}

	var $limit = 10;
	var $title = 'Pages Setting';
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman absen,
	 * jika tidak akan meload halaman login
	 */
	function index()
	{
		$this->tampil_pages();
	}

	function add() {
		$data['title'] = $this->title;
        $data['h2_title'] = '<i class="glyphicon glyphicon-file"></i> '.$this->title.' > Add';
        $data['main_view'] = 'pages/pages_form';
        $data['form_action']	= site_url('pages/setting/add/insert');
		$data['extraHelperScript'] = array('tinymce/tinymce.min.js','bootstrap-tagsinput.js');
		$data['extraHelperCss'] = array('bootstrap-tagsinput.css');
        $data['extraScript'] = array('pagesSetting/pages-form.js');
        $data['pagesTypeList'] = $this->General_model->getSettingByParameter('PageType');;

		$this->load->view('template', $data);
	}

	function edit($id) {
		$data['title'] = $this->title;
        $data['h2_title'] = '<i class="glyphicon glyphicon-file"></i> '.$this->title.' > Add';
        $data['main_view'] = 'pages/pages_form';
        $data['form_action']	= site_url('pages/setting/edit/update');
		$data['extraHelperScript'] = array('tinymce/tinymce.min.js','bootstrap-tagsinput.js');
		$data['extraHelperCss'] = array('bootstrap-tagsinput.css');
        $data['extraScript'] = array('pagesSetting/pages-form.js');
		$data['pagesTypeList'] = $this->General_model->getSettingByParameter('PageType');

		$data['viewData'] = $this->Pages_model->pagesbyid($id);

		$this->load->view('template', $data);
	}

	function tampil_pages(){

		$data['title'] = $this->title;
        $data['h2_title'] = '<i class="glyphicon glyphicon-file"></i> Pages Setting';
        $data['main_view'] = 'pages/pages_view';
        //notes: getDataTableScriptSource declared in helpers directory, inside web_helper
        $data['extraHelperScript'] = getDataTableScriptSource();
        $data['extraHelperCss'] = getDataTableCssSource();
        $data['extraScript'] = array('pagesSetting/pages-view.js');
        $data['pagesListData'] = json_encode($this->Pages_model->daftarPages());

		$this->load->view('template', $data);

	}

	function delete_pagessetting() {

        if($this->input->post('uniqueid') == NULL)
        {
            show_error('uniqueid = 0');
        }
        
        $id = $this->input->post('uniqueid');

        if($this->General_model->deleteTableBasedOnTableAndID($id, 'pages', 'id_pages')){
            echo SUCCESS_DELETE;
        }else{
            echo NO_CHANGE_SAVE;
        }
    }

	function update_pagessetting() {

        try{

            $config = array(
                'upload_path'   =>  'uploadResource/img/',
                'allowed_types' => 'gif|jpg|png',
                'max_size'      => '5000',
                'max_width'     => '1024',
                'max_height'    => '768',
                'encrypt_name'  => true,
            );

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('pagesImage')) {

                $upload_data = $this->upload->data();
            
                $data_ary = array(
                    'title'     => $upload_data['client_name'],
                    'file'      => $upload_data['file_name'],
                    'width'     => $upload_data['image_width'],
                    'height'    => $upload_data['image_height'],
                    'type'      => $upload_data['image_type'],
                    'size'      => $upload_data['file_size'],
                    'path'      => $upload_data['full_path'],
                    'date'      => time()
                );

                $pagesImageFilePath = $config["upload_path"].$data_ary['file'];    
            }            

            $pagesName = $this->input->post('pagesName');
            $pagesType = $this->input->post('pagesType');
			$seoTitle = $this->input->post('seoTitle');
			$tags = $this->input->post('tags');
			$pagesContent = $this->input->post('pagesContent');
			$showImage = $this->input->post('showImage');
			$order = $this->input->post('order');
			$ispublish = $this->input->post('ispublish');
			$parentid = $this->input->post('parentid');
			$ismenu = $this->input->post('ismenu');
			$subpages = $this->input->post('subpages');
			$sidepages = $this->input->post('sidepages');
			
            // echo $productName;

            if($this->input->post('pagesid') !== NULL)
                $pagesid = $this->input->post('pagesid');		
            
            $newData = array(
                'judul_seo'=>$seoTitle,	
				'name_pages'=>$pagesName,
				'pages'=>$pagesContent,
				'tipe_pages'=>$pagesType,
				'tags'=>$tags,
				'urutan'=>$order,
				'isMenu'=>($ismenu) ? 1 : 0,  
				'subpages'=>($subpages) ? 1 : 0,  
				'sidepages'=>($sidepages) ? 1 : 0, 
				'is_publish'=>$ispublish,
                'showImage'=>($showImage) ? 1 : 0,
                'last_user'=>$this->session->userdata('username'),
                'last_update'=>date('Y-m-d h:i:s')
            );

            if(isset($pagesImageFilePath)){
                $newData = array_merge($newData, array(
                    'image_name'=>$pagesImageFilePath
                ));
            }

            // echo $data_ary['size'];
            
            if(strlen($pagesid) > 0){		
                $newData = array_merge($newData, array(
                    'id_pages'=>$pagesid
                ));
                if($this->Pages_model->saveUpdatePagesSetting($newData)){
                    echo SUCCESS_SAVE;
                }else{
                    echo FAILED_SAVE;
                }
            }
            else{
                if($this->Pages_model->saveInsertPagesSetting($newData)){
                    echo SUCCESS_SAVE;
                }else{
                    echo FAILED_SAVE;
                }
            }
            

        }catch(Exception $exp){
            echo $exp;
        }
	}
	
}
	
<?php
/**
 * Plafond Class
 *
 * Archie Ananda
 */
class Plafond extends CI_Controller {
    public function  __construct()
	{
        parent::__construct();
        if ($this->session->userdata('login') == FALSE)
		{
			redirect('login');	
		}
        $this->load->model('Plafond_model', '', TRUE);
        $this->load->model('General_model', '', TRUE);        
        //declare helper web_helper
        $this->load->helper('Web_helper');
        
        $this->load->model('Login_model', '', TRUE);        
        if($this->Login_model->get_userlevel_by_username($this->session->userdata('username')) != USER_LEVEL_ADMIN )
        {
            show_error("Forbidden Access");
        }
    }

    var $limit = 10;
    var $title = 'Plafond Setting';

    function index()
	{
		$this->plafond_setting();
    }

    function plafond_setting(){

		$data['title'] = $this->title;
        $data['h2_title'] = '<i class="glyphicon glyphicon-cog"></i> '.$this->title;
        $data['main_view'] = 'plafond/plafondsetting_view';
        //notes: getDataTableScriptSource declared in helpers directory, inside web_helper
        $data['extraHelperScript'] = getDataTableScriptSource();
        $data['extraHelperCss'] = getDataTableCssSource();
        $data['extraScript'] = array('plafond/plafond-setting.js');
        $data['plafondListData'] = json_encode($this->Plafond_model->plafondSettingList());

		$this->load->view('template', $data);
    }

    function plafond_setting_add(){
        $data['title'] = $this->title;
        $data['h2_title'] = '<i class="glyphicon glyphicon-cog"></i> '.$this->title.' > Add';
        $data['main_view'] = 'plafond/plafondsetting_edit_view';
        $data['form_action']	= site_url('plafond/setting/add/insert');
        $data['extraScript'] = array('plafond/plafond-setting-edit.js');

		$this->load->view('template', $data);
    }

    function plafond_setting_edit($id=0){
        $data['title'] = $this->title;
        $data['h2_title'] = '<i class="glyphicon glyphicon-cog"></i> '.$this->title.' > Edit';
        $data['main_view'] = 'plafond/plafondsetting_edit_view';
        $data['form_action']	= site_url('plafond/setting/edit/update');
        $data['extraScript'] = array('plafond/plafond-setting-edit.js');
        $data['plafondData'] = $this->Plafond_model->getPlafondSetting($id);

		$this->load->view('template', $data);
    }

    function update_plafondsetting() {
        $plafondmin = $this->input->post('plafondmin');
        $plafondmax = $this->input->post('plafondmax');
        $plafondstatus = $this->input->post('plafondstatus');

		if($this->input->post('plafondid') !== NULL)
            $plafondid = $this->input->post('plafondid');		
        
		$newData = array(
			'plafondmin'=>$plafondmin,	
			'plafondmax'=>$plafondmax,
			'IsActive'=>($plafondstatus) ? 1 : 0,
            'UserUpdate'=>$this->session->userdata('username'),
            'LastupdateDate'=>date('Y-m-d h:i:s')
		);
		
		if(strlen($plafondid) > 0){		
			$newData = array_merge($newData, array(
				'PlafondID'=>$plafondid
			));
			if($this->Plafond_model->saveUpdatePlafondSetting($newData)){
                echo SUCCESS_SAVE;
            }else{
                echo NO_CHANGE_SAVE;
            }
		}
		else{
			if($this->Plafond_model->saveInsertPlafondSetting($newData)){
                echo SUCCESS_SAVE;
            }else{
                echo NO_CHANGE_SAVE;
            }
		}
    }
    
    function delete_plafondsetting() {

        if($this->input->post('uniqueid') == NULL)
        {
            show_error('uniqueid = 0');
        }
        
        $id = $this->input->post('uniqueid');

        if($this->General_model->deleteTableBasedOnTableAndID($id, 'm_plafondscheme', 'PlafondID')){
            echo SUCCESS_DELETE;
        }else{
            echo NO_CHANGE_SAVE;
        }
    }
}
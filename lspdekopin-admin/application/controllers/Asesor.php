<?php
/**
 * Asesor Class
 *
 * Archie Ananda
 */
class Asesor extends CI_Controller {

	public function  __construct()
	{
		parent::__construct();
		if ($this->session->userdata('login') == FALSE)
		{
			redirect('login');	
		}
		$this->load->model('Users_model', '', TRUE);
		$this->load->model('general_model', '', TRUE);
		$this->load->model('Asesor_model', '', TRUE);
		//declare helper web_helper
        $this->load->helper('Web_helper');
		$this->load->helper(array('form', 'url'));
		
		
        $this->load->model('Login_model', '', TRUE);        
        if($this->Login_model->get_userlevel_by_username($this->session->userdata('username')) != USER_LEVEL_ADMIN )
        {
            show_error("Forbidden Access");
        }
	}

	var $limit = 10;
	var $title = 'Data Asesor';
	

	function index()
	{
		$this->tampil_data();
	}

	function tampil_data(){

		$data['title'] = $this->title;
		$data['h2_title'] = '<i class="glyphicon glyphicon-user"></i> Users Management';
		$data['main_view'] = 'asesor/asesor_view';
		
		$data['extraHelperScript'] = getDataTableScriptSource();
        $data['extraHelperCss'] = getDataTableCssSource();
		$data['extraScript'] = array('asesor/asesor-view.js');
		
		$data['isi'] = json_encode($this->Asesor_model->daftar());
		$this->load->view('template', $data);
	}

	function add() {
		
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= '<i class="glyphicon glyphicon-user"></i> Users > Add user';
		$data['main_view'] 		= 'asesor/asesor_form';
		$data['form_action']	= site_url('asesor/setting/add/insert');
		$data['extraScript'] = array('asesor/asesor-form.js');

		$this->load->view('template', $data);
	
	}

	function asesor_edit($id = 0) {
		
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= '<i class="glyphicon glyphicon-user"></i> Users > Edit user';
		$data['main_view'] 		= 'asesor/asesor_form';
		$data['form_action']	= site_url('asesor/setting/add/insert');
		$data['Dataasesor'] = $this->Asesor_model->getData($id);
		$data['extraScript'] = array('asesor/asesor-form.js');
	

		$this->load->view('template', $data);

	}

	function addinsert() {
		$kode_asesor = $this->input->post('kode_asesor');
		$DESKRIPSI = $this->input->post('DESKRIPSI');
		$ISACTIVE =  $this->input->post('ISACTIVE');
	
		if($this->input->post('hdnid') !== NULL)
			$hdnid = $this->input->post('hdnid');		

		$newData = array(
			'KODE_ASSESOR'=>$kode_asesor,	
			'DESKRIPSI'=>$DESKRIPSI,
			'ISACTIVE'=>($ISACTIVE) ? 1 : 0, 
			'UPDBY'=>$this->session->userdata('username'),
			'LASTUPD'=>date('Y-m-d h:i:s')
		);

		

		if(strlen($hdnid) > 0){		
			$newData = array_merge($newData, array(
				'ID'=>$hdnid
			));
			
			if($this->Asesor_model->saveUpdateSetting($newData) ){
				echo "<script>alert('Save success');</script>";
				$this->asesor_edit($hdnid);
			}else{
				echo "<script>alert('Save Failed');</script>";
				$this->asesor_edit($hdnid);
			}
		}
		else{
			if($this->Asesor_model->saveInsertSetting($newData)){
				echo "<script>alert('Add Success');</script>";
				redirect("asesor/setting");
			}else{
				echo "<script>alert('Add Failed');</script>";
				$this->tampil_data();
			}
		}
	}
}
	
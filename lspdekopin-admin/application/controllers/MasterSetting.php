<?php
/**
 * MasterSetting Class
 *
 * Archie Ananda
 */
class MasterSetting extends CI_Controller {
    public function  __construct()
	{
        parent::__construct();        
        if ($this->session->userdata('login') == FALSE)
		{
			redirect('login');	
		}
        
        $this->load->helper('url');
        $this->load->model('general_model', '', TRUE);    
        //declare helper web_helper
        $this->load->helper('web_helper');

        $this->load->model('Login_model', '', TRUE);        
        if($this->Login_model->get_userlevel_by_username($this->session->userdata('username')) != USER_LEVEL_ADMIN )
        {
            show_error("Forbidden Access");
        }
    }

    var $limit = 10;
    var $title = 'Master Setting';

    
    function index()
	{
        $data['title'] = $this->title;
        $data['h2_title'] = '<i class="glyphicon glyphicon-wrench"></i> '.$this->title;
        $data['main_view'] = 'master/mastersetting_view';
        //notes: getDataTableScriptSource declared in helpers directory, inside web_helper
        $data['extraHelperScript'] = getDataTableScriptSource();
        $data['extraHelperCss'] = getDataTableCssSource();
        $data['extraScript'] = array('master/mastersetting.js');
        $data['masterSettingParamList'] = $this->general_model->getAllParameter();
        $data['masterSettingListData'] = json_encode($this->general_model->getFullSettingByParameter(''));        
        
		$this->load->view('template', $data);
    }
    
    function getFullMasterSettingList()
    {
        $settingparam = $this->input->post("settingparam");

        echo json_encode(
            array('code' => 'OK', 'data' => $this->general_model->getFullSettingByParameter($settingparam))
        );
    }

    function master_setting_add($parameter=''){
        if($parameter == '')
            show_404();

        $data['title'] = $this->title;
        $data['h2_title'] = '<i class="glyphicon glyphicon-cog"></i> '.$this->title.' > Add';
        $data['main_view'] = 'master/mastersetting_edit_view';
        $data['form_action']	= site_url('master/setting/save/insert');
        $data['extraScript'] = array('master/mastersetting-edit.js');
        $data['SettingParameter'] = $parameter;

		$this->load->view('template', $data);
    }

    function master_setting_edit($settingID=0){
        if($settingID == 0)
            show_404();

        $data['title'] = $this->title;
        $data['h2_title'] = '<i class="glyphicon glyphicon-cog"></i> '.$this->title.' > Edit';
        $data['main_view'] = 'master/mastersetting_edit_view';
        $data['form_action']	= site_url('master/setting/save/update');
        $data['extraScript'] = array('master/mastersetting-edit.js');
        $data['viewData'] = $this->general_model->getFullSettingBySettingID($settingID);
        $data['SettingParameter'] = $data['viewData']->Parameter;

		$this->load->view('template', $data);
    }

    function update_mastersetting() {
        $settingparameter = $this->input->post('settingparameter');
        $settingvalue = $this->input->post('settingvalue');
        $settingdescription = $this->input->post('settingdescription');
        $settingstatus = $this->input->post('settingstatus');

		if($this->input->post('settingid') !== NULL)
            $settingid = $this->input->post('settingid');		
        
		$newData = array(
			'Parameter'=>$settingparameter,	
            'Value'=>$settingvalue,
			'Description'=>$settingdescription,            
			'IsActive'=>($settingstatus) ? 1 : 0,
            'UserUpdate'=>$this->session->userdata('username'),
            'LastupdateDate'=>date('Y-m-d h:i:s')
        );
        
		if(strlen($settingid) > 0){		
			$newData = array_merge($newData, array(
				'SettingID'=>$settingid
			));
			if($this->general_model->saveUpdateMasterSetting($newData)){
                echo SUCCESS_SAVE;
            }else{
                echo NO_CHANGE_SAVE;
            }
		}
		else{
			if($this->general_model->saveInsertMasterSetting($newData)){
                echo SUCCESS_SAVE;
            }else{
                echo NO_CHANGE_SAVE;
            }
		}
    }
}
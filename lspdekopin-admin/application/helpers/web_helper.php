
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('getDataTableScript'))
{
    function getDataTableScriptSource(){
        return array('jquery.dataTables.min.js','dataTables.bootstrap.min.js','custom/data-table-logic.js');
    }

    function getDataTableCssSource(){
        return array('dataTables.bootstrap.min.css');
    }
}

define('SUCCESS_SAVE','Data berhasil disimpan');
define('FAILED_SAVE','Data gagal disimpan');
define('NO_CHANGE_SAVE','Tidak ada perubahan data');
define('SUCCESS_DELETE','Data berhasil dihapus');
define('FAILED_DELETE','Data gagal dihapus');
define('SUCCESS_SUBMIT','Data berhasil disubmit');
define('FAILED_SUBMIT','Data gagal disubmit');
define('NOT_ALL_DATA_SAVED','Tidak semua data dapat tersimpan. Mohon periksa data anda.');
define('SUCCESS_SAVE_JUNK','Data berhasil dipindahkan dalam Junk');
define('SUCCESS_SAVE_DATAENTRY','Data berhasil diteruskan ke Data Entry');
define('SUCCESS_SAVE_PICKUP','Data pickup berhasil disimpan');
define('SUCCESS_SAVE_ENTRYDONE','Proses pengajuan aplikasi telah selesai. Mohon segera disubmit.');
define('SUCCESS_SAVE_BACK_TO_FOLLOWUP','Data telah dikembalikan ke proses followup untuk direvisi');
define('SUCCESS_BANK_SUBMIT','Data sukses disubmit ke Bank');
define('SUCCESS_SAVE_BANK_RESULT','Data pengajuan sukses diselesaikan');
define('SUCCESS_SAVE_ASSIGN','Data aplikasi sukses diassign ke sales');

define('SUCCESS_COMPLETED','Data asesi telah completed');
define('FAIL_COMPLETED','Data asesi gagal completed');

define('USER_LEVEL_ADMIN','1');
?>
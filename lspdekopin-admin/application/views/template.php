
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>LSP DEKOPIN</title>
         
        <link rel="stylesheet" href="<?php echo base_url().'css/daterangepicker.min.css?v=1.1';?>" />

        <link rel="stylesheet" href="<?php echo base_url().'css/select2.min.css';?>" />    

        <link rel="stylesheet" href="<?php echo base_url().'css/bootstrap.min.css';?>" />    

        <link rel="stylesheet" href="<?php echo base_url().'css/style.css?v=1.1';?>" />           

        <?php if(isset($extraHelperCss)){
            foreach($extraHelperCss as $css_helper_to_load):?>
            <link rel="stylesheet" href="<?php echo base_url().'css/'.$css_helper_to_load;?>" />
         <?php endforeach;}?>

         
        <!-- jQuery -->
        <script src="<?php echo base_url().'js/jquery-3.3.1.min.js';?>"></script>
         <!-- Bootstrap Js -->
         <script src="<?php echo base_url().'js/bootstrap.min.js';?>"></script>
         <!-- Date Picker Functionality -->
         <script src="<?php echo base_url().'js/moment.min.js';?>"></script>
         <!-- Drop Down Functionality -->
         <script src="<?php echo base_url().'js/select2.full.min.js';?>"></script>
         <script src="<?php echo base_url().'js/select2.min.js';?>"></script>
        <!-- Print Functionality -->
         <script src="<?php echo base_url().'js/jQuery.print.min.js';?>"></script>

         <script src="<?php echo base_url().'js/jquery.daterangepicker.min.js';?>"></script>         
         <!-- Custom Js -->
         <script src="<?php echo base_url().'js/custom/pages.js';?>"></script>
         
         <?php if(isset($extraHelperScript)){ foreach($extraHelperScript as $scripts_helper_to_load):?>
            <script type='text/javascript' src = '<?php echo base_url()."js/".$scripts_helper_to_load;?>'></script>
         <?php endforeach;}?>

    </head>
    <body>

        <div class="wrapper">
            <!-- Sidebar Holder -->
            <nav id="sidebar">
                <div class="sidebar-header">
                    <img src="<?php echo base_url() ?>images/provist-logo.jpg" style="width:50px;float:left;" />
                    <h3 style="margin-top:15px;font-size:20px;margin-left:60px;">LSP Admin</h3>
                    <strong>LSP</strong>
                    <div class="clearfix"></div>
                </div>

                <!-- Untuk loading menu -->
                <?php $this->load->view('menu'); ?>

            </nav>

            <!-- Page Content Holder -->
            <div id="content">

                <nav class="navbar navbar-default">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="btn btn-primary navbar-btn">
                                <i class="glyphicon glyphicon-align-justify"></i>
                            </button>
                        </div>
                        
                        <h3 class="title-page">
                            <?php echo $h2_title ?>
                        </h3>

                        <!-- <div class="navbar-right" style="margin-top:10px;margin-right:10px;border:1px solid #ccc;padding:5px 10px 10px 10px;border-radius:3px">
                            <i class="glyphicon glyphicon-user" style="margin-right:10px;padding-top:5px;"></i>
                            <?php echo $this->session->userdata('username'); ?>
                        </div> -->
                        
                    </div>
                </nav>
                <div class="container-box">
                    <?php $this->load->View($main_view); ?>
                </div>
            </div>
            <!-- <div class="nav-scroll">
                <div class="to-top">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                </div>
                <div class="to-bottom">
                    <i class="glyphicon glyphicon-chevron-down"></i>
                </div>
            </div> -->
        </div>

        <div id="loading">
          <!-- <img id="loading-image" src="images/ajax-loader.gif" alt="Loading..." /> -->
          <div class="loader"></div>
        </div>

        <div class="modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                </div>
            </div>
        </div>
        

         <?php if(isset($extraScript)){ foreach($extraScript as $scripts_to_load):?>
            <script type='text/javascript' src = '<?php echo base_url()."js/custom/".$scripts_to_load;?>'></script>
         <?php endforeach;}?>

    </body>
</html>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<title>Login Admin Punyaweb</title> 

<link rel="shortcut icon" href="<?php echo base_url().'images/img_login.png';?>" />
<link rel="stylesheet" href="<?php echo base_url().'css/bootstrap.css';?>" />
<link rel="stylesheet" href="<?php echo base_url().'css/style.css';?>" />
 
<style>
.container {width: 300px;}

</style>


<script language="javascript">
function validasi(form){
  if (form.username.value == ""){
    alert("Anda belum mengisikan Username.");
    form.username.focus();
    return (false);
  }
     
  if (form.password.value == ""){
    alert("Anda belum mengisikan Password.");
    form.password.focus();
    return (false);
  }
  return (true);
}
</script>
</head> 
<body> 

 <div class="container">
  <?php
    $attributes = array('name' => 'login_form', 'id' => 'login_form', 'class' => 'form-signin');
    echo form_open('login/process_login', $attributes);
  ?>



<img src="<?php echo base_url() . '/images/img_login.png';?>" class="img-responsive" alt="Punyaweb Login Image">

             <?php 
      $message = $this->session->flashdata('message');
      echo $message == '' ? '' : '<div class="alert alert-danger" role="alert">' . $message . '</div>';
    ?>


        <label for="inputEmail" class="sr-only">User Name</label>
        <input type="text" name="username" class="form-control" placeholder="User Name" value="<?php echo set_value('username');?>" required autofocus>
      <br>  <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password" class="form-control" placeholder="Password" value="<?php echo set_value('password');?>" required>
 <br>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>

    </div> <!-- /container -->
<!--
<div data-role="page">
<div data-role="header" data-theme="e" style="height:50">
</div><br>
<div data-role="content" style="width:300;position:relative;left:38%;text-align:center;border:dashed #CCC 1px"><br>
   <img src="images/img_login.png"><br><br>

  <form   method="post" onSubmit="return validasi(this)" data-ajax="false" name="login">
	<label for="basic" style="text-align:left">Email or Username</label>
    <input type="text" name="username" id="basic" value=""  />
    <label for="basic" style="text-align:left">Password</label>
    <input type="password" name="password" id="basic" value=""  />
    <input name="login" type="submit"  value="Log In" data-inline="true" data-theme="c" />
</form>
<br>
<p align="center"> Copyright &copy; 2013 - THIS CMS Provides by archieananda</p>
</div><br>
<div data-role="footer" data-theme="e" style="height:200px;">
</div>
	
</div>
-->

</body>
</html>
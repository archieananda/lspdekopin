<?php echo form_open_multipart($form_action);?>
  <fieldset>
    <legend>Posts Info</legend>
    
    <div class="form-group">
    <input type="hidden" name="postsid" 
        value='<?php if(isset($viewData->id_berita)) echo $viewData->id_berita; ?>' />
      <label for="newsTitle">News Title</label>
      <input type="text" class="form-control" required="true" id="newsTitle" name="newsTitle"
        value='<?php if(isset($viewData->judul_berita)) echo $viewData->judul_berita; ?>'
        placeholder="News Title">
    </div>

    <div class="form-group">
      <label for="seoTitle">SEO Title</label>
      <input type="text" class="form-control" required="true" id="seoTitle" name="seoTitle"
        value='<?php if(isset($viewData->judul_seo)) echo $viewData->judul_seo; ?>'
        placeholder="SEO Title">
    </div>

    <div class="form-group">
      <label for="tags">Tags</label>
      <input data-role="tagsinput" type="text" class="form-control" 
        required="true" id="tags" name="tags"
        value='<?php if(isset($viewData->tags)) echo $viewData->tags; ?>'
        placeholder="Tags">
    </div>

    <div class="form-group">
      <label for="author">Author</label>
      <input type="text" class="form-control" 
        required="true" id="author" name="author"
        value='<?php if(isset($viewData->author)) echo $viewData->author; else echo $this->session->userdata('username') ?>'
        placeholder="Author">
    </div>

    <div class="form-group">
      <label for="editor">Editor</label>
      <input type="text" class="form-control" 
        required="true" id="editor" name="editor"
        value='<?php if(isset($viewData->editor)) echo $viewData->editor; else echo $this->session->userdata('username') ?>'
        placeholder="Editor">
    </div>

    <div class="form-group">
      <label for="postsContent">Content</label>
      <text-area class="form-control" required="true" id="postsContent" name="postsContent"
        placeholder="Content">
        <?php if(isset($viewData->detail_berita)) echo $viewData->detail_berita; ?>
      </text-area>
    </div>

    <div class="form-group">
      <label for="leadinberita">News Lead In</label>
      <text-area class="form-control" 
        required="true" id="leadinberita" name="leadinberita"
        placeholder="News Lead In"><?php if(isset($viewData->leadin_berita)) echo $viewData->leadin_berita; ?></text-area>
    </div>

    <div class="form-group">
    <label for="publisheddate">Published Date</label>
      <input type="text" class="form-control date-picker single date" name="publisheddate" id="publisheddate" 
            placeholder="Published Date"
            value="<?php if(isset($viewData->tgl_publish) 
                && $viewData->tgl_publish != '0000-00-00 00:00:00') 
            echo date_format(new DateTime($viewData->tgl_publish),'l, d/m/Y'). 
            date_format(new DateTime($viewData->tgl_publish),' H:i:s'); ?>"/>
    </div>

    <div class="form-group">
      <label for="postsImage">Posts Image</label>
      <input type="file" class="form-control"  id="postsImage" name="postsImage"
        placeholder="Upload Posts Image" />
      <div class="img-preview-box">
        <span class='prev-cap'>Last Saved Preview :</span>
        <?php if(isset($viewData->img_path)){ ?>
        <img src="<?php echo base_url() . "/" . $viewData->img_path?>"
        width="200"/>        
        <?php }else{
          echo "<span class='no-img'>No image available</span>";
        } ?>
      </div>
    </div>

    <div class="form-group">
      <label for="postsThumbnail">Posts Thumbnail</label>
      <input type="file" class="form-control"  id="postsThumbnail" name="postsThumbnail"
        placeholder="Upload Posts Thumbnail" />
      <div class="img-preview-box">
        <span class='prev-cap'>Last Saved Preview :</span>
        <?php if(isset($viewData->thumbnail_path)){ ?>
        <img src="<?php echo base_url() . "/" . $viewData->thumbnail_path?>"
        width="200"/>        
        <?php }else{
          echo "<span class='no-img'>No image available</span>";
        } ?>
      </div>
    </div>

    <div class="form-group">
      <label for="imagecaption">Image Caption</label>
      <input type="text" class="form-control" 
        required="true" id="imagecaption" name="imagecaption"
        value='<?php if(isset($viewData->img_caption)) echo $viewData->img_caption; ?>'
        placeholder="Image Caption">
    </div>

    <?php echo anchor('posts/setting', 'Cancel', 'class="btn btn-default btn-cancel"');?>
    <input type="submit" value="Save" class="btn btn-primary" />
 </fieldset>
<?php echo form_close();?>
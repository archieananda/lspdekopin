<fieldset class="do-search">
    <legend>Search Criteria</legend>
    <div class="form-horizontal">
        <div class="col-lg-6">
            <div class="form-group">
                <label for="settingparam" class="col-lg-4">Parameter Setting</label>
                <div class=" col-lg-8">
                    <select name="settingparam" id="settingparam"  class="form-control">
                            <option value="">Select Parameter Setting</option>
                                <?php
                                foreach($masterSettingParamList as $type_list):
                                    echo "<option value=".$type_list["Parameter"].">"
                                        .$type_list["Parameter"]."</option>";
                                endforeach;
                                ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                    <button class="btn btn-primary" name="search" id="search"
                        urlaction="<?php echo base_url(); ?>/master/setting/load">
                        <i class="glyphicon glyphicon-cloud-download"></i>&nbsp;Load Setting
                    </button>
            </div>
        </div>
    </div>
</fieldset>
</hr>
<button class="btn btn-primary" name="addsetting" id="addsetting" disabled="disabled"
    urlaction="<?php echo base_url(); ?>/master/setting/add/">
    <i class="glyphicon glyphicon-plus-sign"></i>&nbsp;Add Setting
</button>
</br>
</br>
<input id="masterSettingListRaw" type="hidden" value='<?php echo $masterSettingListData; ?>' />
<table id="masterSettingList" class="table table-striped table-bordered" cellspacing="0" width="100%"></table>
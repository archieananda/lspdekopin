<form action='<?php echo $form_action; ?>' method='POST'>
  <fieldset>
    <legend>Master Setting Info</legend>
    <input type="hidden" name="settingid" value='<?php if(isset($viewData->SettingID)) echo $viewData->SettingID; ?>' />
    <div class="form-group">
      <label for="settingparameter">Setting Parameter</label>
      <input type="text" class="form-control" required="true" id="settingparameter" name="settingparameter"
        value='<?php if(isset($SettingParameter)) echo $SettingParameter; ?>'
        readonly="readonly">
    </div>
    <div class="form-group">
      <label for="settingvalue">Setting Value</label>
      <input type="text" class="form-control" required="true" id="settingvalue" name="settingvalue"
        value='<?php if(isset($viewData->Value)) echo $viewData->Value; ?>'
        placeholder="Setting Value">
    </div>
    <div class="form-group">
      <label for="settingdescription">Setting Description</label>
      <input type="text" class="form-control" required="true" id="settingdescription" name="settingdescription"
        value='<?php if(isset($viewData->Description)) echo $viewData->Description; ?>'
        placeholder="Setting Description">
    </div>
    <div class="form-group">
      <label class="form-check-label">
      <input class="form-check-input" type="checkbox" name="settingstatus"
            '<?php if(isset($viewData->IsActive)) echo ((bool)$viewData->IsActive) ? " checked='checked'" : ""; ?>' />
            Setting Status
      </label>
    </div>
    <?php echo anchor('master/setting', 'Cancel', 'class="btn btn-default btn-cancel"');?>
    <input type="submit" value="Save" class="btn btn-primary" />
 </fieldset>
</form>
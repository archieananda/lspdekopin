<?php echo form_open_multipart($form_action);?>
  <fieldset>
    <legend>Product Info</legend>
    <div class="form-group">
    <input type="hidden" name="productid" 
      value='<?php if(isset($productData->ProductID)) echo $productData->ProductID; ?>' />
      <label for="productType">Product Type</label>
      <select class="form-control" id="productType" required="true" name="productType">
        <option value="">Select Product Type</option>
         <?php
         foreach($productTypeList as $prod_type_list):
            if($productData->ProductType == $prod_type_list["value"])
                echo "<option selected='selected' value=".$prod_type_list["value"].">"
                .$prod_type_list["description"]."</option>";
            else
                echo "<option value=".$prod_type_list["value"].">"
                .$prod_type_list["description"]."</option>";
         endforeach;
         ?>
      </select>
    </div>
    <div class="form-group">
      <label for="productName">Product Name</label>
      <input type="text" class="form-control" required="true" id="productName" name="productName"
        value='<?php if(isset($productData->ProductName)) echo $productData->ProductName; ?>'
        placeholder="Product Name">
    </div>
    <div class="form-group">
      <label class="form-check-label">
      <input class="form-check-input" type="checkbox" name="productStatus"
            '<?php if(isset($productData->IsActive)) echo ((bool)$productData->IsActive) ? " checked='checked'" : ""; ?>' />
            Product Status
      </label>
    </div>
    <div class="form-group">
      <label for="productContent">Product Content</label>
      <text-area class="form-control" required="true" id="productContent" name="productContent"
        placeholder="Product Content">
        <?php if(isset($productData->HTMLContent)) echo $productData->HTMLContent; ?>
      </text-area>
    </div>
    <div class="form-group">
      <label for="productImage">Product Image</label>
      <?php if(isset($productData->ImagePath)){ ?>
        <input type="file" class="form-control"  id="productImage" name="productImage"
        placeholder="Upload Product Image" />
      <?php }else{ ?>
        <input type="file" class="form-control" required="true" id="productImage" name="productImage"
        placeholder="Upload Product Image" />
      <?php } ?>
      <div class="img-preview-box">
        <span class='prev-cap'>Last Saved Preview :</span>
        <?php if(isset($productData->ImagePath)){ ?>
        <img src="<?php echo base_url() . "/" . $productData->ImagePath?>"
        width="200"/>        
        <?php }else{
          echo "<span class='no-img'>No image available</span>";
        } ?>
      </div>
    </div>
    <?php echo anchor('product/setting', 'Cancel', 'class="btn btn-default btn-cancel"');?>
    <input type="submit" value="Save" class="btn btn-primary" />
 </fieldset>
<?php echo form_close();?>
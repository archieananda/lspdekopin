<div class="row">
    <fieldset>
        <legend>Product Info</legend>
        <div class="form-group">
            <label for="productName">Product Name</label>
            <input type="text" readonly="readonly" class="form-control" required="true" 
                id="productName" name="productName"
                value='<?php if(isset($productData->ProductName)) echo $productData->ProductName; ?>'>
        </div>
        <div class="form-group">
            <label for="productType">Product Type</label>
            <input type="text" readonly="readonly" class="form-control" required="true" 
                id="productType" name="productType"
                value='<?php if(isset($productData->ProductTypeName)) echo $productData->ProductTypeName; ?>'>
        </div>
    </fieldset>
    <hr/>
    <input id="prodId" type="hidden" value='<?php echo $productData->ProductID; ?>' />
    <input id="prodRateListRaw" type="hidden" value='<?php echo $prodRateListData; ?>' />
    <table id="prodRateList" class="table table-striped table-bordered" cellspacing="0" width="100%"></table>
    <hr/>
    <?php echo anchor('product/setting', 'Cancel','class="btn btn-default"');?>&nbsp;
    <?php echo anchor("product/setting/rate/add/".$productData->ProductID, 'Add Setting Rate','class="btn btn-primary"');?>
</div>
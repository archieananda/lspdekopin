<form action='<?php echo $form_action; ?>' method='POST'>
  <fieldset>
    <legend>Product Rate Info</legend>
    <input type="hidden" name="productid" id="productid"
      value='<?php if(isset($productData->ProductID)) echo $productData->ProductID; ?>' />
      <input type="hidden" name="productrateid" 
      value='<?php if(isset($productRateData->ProductRateID)) echo $productRateData->ProductRateID; ?>' />
    <div class="form-group">
      <label for="productName">Product Name</label>
      <input type="text" readonly="readonly" class="form-control" required="true" id="productName" name="productName"
        value='<?php if(isset($productData->ProductName)) echo $productData->ProductName; ?>'
        placeholder="Product Name">
    </div>
    <div class="form-group">
      <label for="tenure">Tenure</label>
      <select class="form-control" id="tenure" required="true" name="tenure">
        <option value="">Select Tenure</option>
         <?php
         foreach($tenorList as $tenor_List):
            if($productRateData->TennorID == $tenor_List["value"])
                echo "<option selected='selected' value=".$tenor_List["value"].">"
                .$tenor_List["description"]."</option>";
            else
                echo "<option value=".$tenor_List["value"].">"
                .$tenor_List["description"]."</option>";
         endforeach;
         ?>
      </select>
    </div>
    <div class="form-group">
      <label for="plafond">Plafond</label>
      <select class="form-control" id="plafond" required="true" name="plafond">
        <option value="">Select Plafond</option>
         <?php
         foreach($plafondList as $plafond_List):
            if($productRateData->PlafondID == $plafond_List["value"])
                echo "<option selected='selected' value=".$plafond_List["value"].">"
                .$plafond_List["description"]."</option>";
            else
                echo "<option value=".$plafond_List["value"].">"
                .$plafond_List["description"]."</option>";
         endforeach;
         ?>
      </select>
    </div>
    <div class="form-group">
      <label class="form-check-label">
      <input class="form-check-input" type="checkbox" name="productRateStatus"
            '<?php if(isset($productData->IsActive)) echo ((bool)$productData->IsActive) ? " checked='checked'" : ""; ?>' />
            Product Rate Status
      </label>
    </div>
    <div class="form-group">
      <label for="effectiverate">Effective Rate</label>
      <input type="number" class="form-control" step="0.01" required="true" id="effectiverate" name="effectiverate"
        value='<?php if(isset($productRateData->EffectiveRate)) echo $productRateData->EffectiveRate; ?>'
        placeholder="Effective Rate">
    </div>
    <div class="form-group">
      <label for="flatrate">Flat Rate</label>
      <input type="number" class="form-control" step="0.01" required="true" id="flatrate" name="flatrate"
        value='<?php if(isset($productRateData->FlatRate)) echo $productRateData->FlatRate; ?>'
        placeholder="Flat Rate">
    </div>
    <?php echo anchor("product/setting/rate/".$productData->ProductID, 'Cancel', 'class="btn btn-default btn-cancel"');?>
    <input type="submit" value="Save" class="btn btn-primary" />
 </fieldset>
</form>
<form action='<?php echo $form_action; ?>' method='POST'>
  <fieldset>
    <legend>Plafond Info</legend>
    <input type="hidden" name="plafondid" value='<?php if(isset($plafondData->PlafondID)) echo $plafondData->PlafondID; ?>' />
    <div class="form-group">
      <label for="plafondmin">Plafond Minimum</label>
      <input type="number" class="form-control" required="true" id="plafondmin" name="plafondmin"
        value='<?php if(isset($plafondData->PlafondMIN)) echo $plafondData->PlafondMIN; ?>'
        placeholder="Plafond Minimum">
    </div>
    <div class="form-group">
      <label for="plafondmax">Plafond Maximum</label>
      <input type="number" class="form-control" required="true" id="plafondmax" name="plafondmax"
        value='<?php if(isset($plafondData->PlafondMAX)) echo $plafondData->PlafondMAX; ?>'
        placeholder="Plafond Maximum">
    </div>
    <div class="form-group">
      <label class="form-check-label">
      <input class="form-check-input" type="checkbox" name="plafondstatus"
            '<?php if(isset($plafondData->IsActive)) echo ((bool)$plafondData->IsActive) ? " checked='checked'" : ""; ?>' />
            Plafond Status
      </label>
    </div>
    <?php echo anchor('plafond/setting', 'Cancel', 'class="btn btn-default btn-cancel"');?>
    <input type="submit" value="Save" class="btn btn-primary" />
 </fieldset>
</form>
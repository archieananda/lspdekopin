<form action='<?php echo $form_action; ?>' method='POST'>
  <fieldset>
    <legend>Sales Info</legend>
    <input type="hidden" name="salesid" value='<?php if(isset($salesData->SalesID)) echo $salesData->SalesID; ?>' />
    <div class="form-group">
      <label for="salesname">Sales Name</label>
      <input type="text" class="form-control" required="true" id="salesname" name="salesname"
        value='<?php if(isset($salesData->SalesName)) echo $salesData->SalesName; ?>'
        placeholder="Sales Name">
    </div>
    <div class="form-group">
      <label for="salescontact">Sales Contact</label>
      <input type="text" class="form-control" required="true" id="salescontact" name="salescontact"
        value='<?php if(isset($salesData->SalesContact)) echo $salesData->SalesContact; ?>'
        placeholder="Sales Contact">
    </div>
    <div class="form-group">
      <label class="form-check-label">
      <input class="form-check-input" type="checkbox" name="salesstatus"
            '<?php if(isset($salesData->IsActive)) echo ((bool)$salesData->IsActive) ? " checked='checked'" : ""; ?>' />
            Sales Status
      </label>
    </div>
    <?php echo anchor('sales/setting', 'Cancel', 'class="btn btn-default btn-cancel"');?>
    <input type="submit" value="Save" class="btn btn-primary" />
 </fieldset>
</form>
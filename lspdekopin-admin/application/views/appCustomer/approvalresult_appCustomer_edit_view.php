<form action='<?php echo $form_action; ?>' method='POST'>
    <?php $this->load->View($process_info_view); ?>
    <hr>
    <fieldset>
        <legend>Approval Result Information</legend>
        <div class="form-horizontal">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="approvalresult" class="col-lg-4">Approval Result</label>
                    <div class="col-lg-8">
                        <select class="form-control" id="approvalresult"  name="approvalresult" required="true">
                            <option value="">Select Approval Result</option>
                            <?php
                            foreach($approvalResultList as $row):
                                if(isset($customerTransData->ApprovalResult) && $customerTransData->ApprovalResult == $row["value"])
                                    echo "<option selected='selected' value=".$row["value"].">"
                                    .$row["description"]."</option>";
                                else
                                    echo "<option value=".$row["value"].">"
                                    .$row["description"]."</option>";
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group reject-reason" style="display:none">
                    <label for="rejectreason" class="col-lg-4">Reject Reason</label>
                    <div class="col-lg-8">
                        <select class="form-control" id="rejectreason"  name="rejectreason" required="true">
                            <option value="">Select Reject Reason</option>
                            <?php
                            foreach($rejectReasonList as $row):
                                if(isset($customerTransData->RejectReason) && $customerTransData->RejectReason == $row["value"])
                                    echo "<option selected='selected' value=".$row["value"].">"
                                    .$row["description"]."</option>";
                                else
                                    echo "<option value=".$row["value"].">"
                                    .$row["description"]."</option>";
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="resultbanknotes" class="col-lg-4">Result Bank Notes</label>
                    <div class=" col-lg-8">
                        <textarea class="form-control" name="resultbanknotes" id="resultbanknotes"
                            placeholder="Notes"><?php if(isset($customerTransData->ResultBankNotes)) echo $customerTransData->ResultBankNotes; ?></textarea>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <div style="margin-top:50px"></div>
    <div class="act-float">
        <?php echo anchor('appcustomer/inquiry', 'Cancel', 'id="btnCancel" class="btn btn-default btn-cancel"');?>
        <button type="submit" class="btn btn-info" id="btnSave">
            <i class="glyphicon glyphicon-thumbs-up"></i>&nbsp;&nbsp;Save Approval Result
        </button>
    </div>
</form>
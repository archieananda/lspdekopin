<fieldset>
        <legend>General Information</legend>
        <input type="hidden" name="transid" id="transid" 
            value="<?php if(isset($customerTransData->TransID)) echo $customerTransData->TransID; ?>"/>
        <div class="form-horizontal">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="referalid" class="col-lg-4">Referal ID Customer</label>
                    <div class=" col-lg-8">
                        <input type="text" class="form-control" name="referalid" id="referalid" 
                            readonly="readonly"
                            value="<?php if(isset($customerTransData->RefferalID)) echo $customerTransData->RefferalID; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="sales" class="col-lg-4">Sales</label>
                    <div class=" col-lg-8">
                        <input type="hidden" class="form-control" name="salesid" id="salesid" 
                            value="<?php if(isset($customerTransData->SalesID)) echo $customerTransData->SalesID; ?>"/>
                        <input type="text" class="form-control" name="sales" id="sales" 
                            readonly="readonly"
                            value="<?php if(isset($customerTransData->user_nama)) echo $customerTransData->user_nama; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="appstatus" class="col-lg-4">Referal Reference</label>
                    <div class=" col-lg-8">
                        <input type="text" class="form-control" name="appstatus" id="appstatus" 
                            readonly="readonly"
                            value="<?php if(isset($customerTransData->RefferalRef)) echo $customerTransData->RefferalRef; ?>"/>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="applicationdate" class="col-lg-4">Application Date</label>
                    <div class=" col-lg-8">
                        <input type="text" class="form-control" name="applicationdate" id="applicationdate" 
                            readonly="readonly"
                            value="<?php if(isset($customerTransData->TglPengajuan)) 
                            echo date_format(new DateTime($customerTransData->TglPengajuan),'d-M-Y'); ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="applicationstatus" class="col-lg-4">Application Status</label>
                    <div class=" col-lg-8">
                        <input type="text" class="form-control" name="applicationstatus" id="applicationstatus" 
                            readonly="readonly"
                            value="<?php if(isset($customerTransData->StatusPengajuanDesc)) echo $customerTransData->StatusPengajuanDesc; ?>"/>
                    </div>
                </div>
            </div>
        </div>
</fieldset>
<hr />
<fieldset>
    <legend>Product Information</legend>
    <div class="form-horizontal">
        <div class="col-lg-6">
            <div class="form-group">
                <label for="producttypedesc" class="col-lg-4">Product Type</label>
                <div class=" col-lg-8">
                    <input type="hidden" name="producttype" id="producttype" 
                    value="<?php if(isset($customerTransData->ProductType)) echo $customerTransData->ProductType; ?>"/>
                    <input type="text" class="form-control" name="producttypedesc" id="producttypedesc" 
                        readonly="readonly"
                        value="<?php if(isset($customerTransData->ProductTypeDesc)) echo $customerTransData->ProductTypeDesc; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="productname" class="col-lg-4">Product</label>
                <div class=" col-lg-8">
                    <input type="hidden" name="productid" id="productid" 
                        value="<?php if(isset($customerTransData->ProductID)) echo $customerTransData->ProductID; ?>"/>
                    <input type="text" class="form-control" name="productname" id="productname" 
                        readonly="readonly"
                        value="<?php if(isset($customerTransData->ProductName)) echo $customerTransData->ProductName; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="tenor" class="col-lg-4">Tenure Month</label>
                <div class=" col-lg-8">
                    <input type="text" class="form-control" name="tenor" id="tenor" 
                        placeholder="Tennure"
                        readonly="readonly"
                        value="<?php if(isset($customerTransData->TennureMonth)) echo $customerTransData->TennureMonth; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="effrate" class="col-lg-4">Effective Rate</label>
                <div class=" col-lg-8">
                    <input type="text" class="form-control" name="effrate" id="effrate" 
                    placeholder="Effective Rate"
                    readonly="readonly"
                    value="<?php if(isset($customerTransData->EffectiveRate)) echo $customerTransData->EffectiveRate; ?>"/>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label for="bankchannel" class="col-lg-4">Bank Channel</label>
                <div class=" col-lg-8">
                    <input type="text" class="form-control" name="bankchannel" id="bankchannel" 
                        placeholder="Bank Channel"
                        readonly="readonly"
                        value="<?php if(isset($customerTransData->ChannelDesc)) echo $customerTransData->ChannelDesc; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="plafond" class="col-lg-4">Plafond</label>
                <div class=" col-lg-8">
                    <input type="text" class="form-control" name="plafond" id="plafond" 
                        placeholder="Plafond"
                        readonly="readonly"
                        value="<?php if(isset($customerTransData->Plafond)) echo $customerTransData->Plafond; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="flatrate" class="col-lg-4">Flat Rate</label>
                <div class=" col-lg-8">
                    <input type="text" class="form-control" name="flatrate" id="flatrate" 
                        placeholder="Flat Rate"
                        readonly="readonly"
                        value="<?php if(isset($customerTransData->FlatRate)) echo $customerTransData->FlatRate; ?>"/>
                </div>
            </div>
        </div>
    </div>
</fieldset>
<hr />
<fieldset>
    <legend>Personal Information</legend>
    <div class="form-horizontal">
        <div class="col-lg-6">
            <div class="form-group">
                <label for="custfullname" class="col-lg-4">Full Name</label>
                <div class=" col-lg-8">
                    <input type="text" class="form-control" name="custfullname" id="custfullname" placeholder="Full Name"
                        readonly="readonly" 
                        value="<?php if(isset($customerTransData->FullName)) echo $customerTransData->FullName; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="custmobileno" class="col-lg-4">Mobile No</label>
                <div class=" col-lg-8">
                    <input type="text" class="form-control" name="custmobileno" id="custmobileno" placeholder="Mobile No"
                        readonly="readonly"
                        value="<?php if(isset($customerTransData->MobileNo)) echo $customerTransData->MobileNo; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="custhomephone" class="col-lg-4">Home Phone</label>
                <div class=" col-lg-8">
                    <input type="text" class="form-control" name="custhomephone" id="custhomephone" placeholder="Home Phone"
                    readonly="readonly"
                    value="<?php if(isset($customerTransData->HomePhone)) echo $customerTransData->HomePhone; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="custemailaddress" class="col-lg-4">Email Address</label>
                <div class=" col-lg-8">
                    <input type="text" class="form-control" name="custemailaddress" id="custemailaddress" placeholder="Email Address"
                    readonly="readonly"
                    value="<?php if(isset($customerTransData->EmailAddress)) echo $customerTransData->EmailAddress; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="custeducation" class="col-lg-4">Education</label>
                <div class=" col-lg-8">
                    <input type="text" class="form-control" name="custeducation" id="custeducation" placeholder="Education"
                    readonly="readonly"
                    value="<?php if(isset($customerTransData->EducationDesc)) echo $customerTransData->EducationDesc; ?>"/>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label for="custresidencestatus" class="col-lg-4">Residence Status</label>
                <div class=" col-lg-8">
                <input type="text" class="form-control" name="custresidencestatus" id="custresidencestatus" 
                    placeholder="Residence Status"
                    readonly="readonly"
                    value="<?php if(isset($customerTransData->ResidenceStatusDesc)) echo $customerTransData->ResidenceStatusDesc; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="custmothersurename" class="col-lg-4">Mother Maiden Name</label>
                <div class=" col-lg-8">
                    <input type="text" class="form-control" name="custmothersurename" id="custmothersurename" placeholder="Mother Sure Name"
                    readonly="readonly"
                    value="<?php if(isset($customerTransData->MotherSureName)) echo $customerTransData->MotherSureName; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="custlivingperiod" class="col-lg-4">Living Period</label>
                <div class=" col-lg-8">
                    <input type="text" class="form-control" name="custlivingperiod" id="custlivingperiod" placeholder="Living Period"
                    readonly="readonly"
                    value="<?php if(isset($customerTransData->LivingPeriod)) echo $customerTransData->LivingPeriod; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="custdependents" class="col-lg-4"># Dependents</label>
                <div class=" col-lg-8">
                    <input type="number" class="form-control" name="custdependents" id="custdependents" placeholder="# Dependents"
                    readonly="readonly"
                    value="<?php if(isset($customerTransData->JmlTanggungan)) echo $customerTransData->JmlTanggungan; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="address" class="col-lg-4">Residence Address</label>
                <div class=" col-lg-8">
                    <textarea class="form-control" name="address" id="address"
                    readonly="readonly"
                        placeholder="Residence Address"><?php if(isset($customerTransData->Address)) echo $customerTransData->Address; ?></textarea>
                </div>
            </div>
        </div>
    </div>
</fieldset>
<hr />
<fieldset>
    <legend>Occupation / Business Information</legend>
    <div class="form-horizontal">
        <div class="col-lg-6">
            <div class="form-group">
                <label for="custoccupation" class="col-lg-4">Occupation</label>
                <div class=" col-lg-8">
                    <select class="form-control" id="custoccupation" disabled="disabled"  name="custoccupation">
                        <option value="">Select Occupation</option>
                        <?php
                        foreach($occupationList as $row):
                            if($customerTransData->Occupation == $row["value"])
                                echo "<option selected='selected' value=".$row["value"].">"
                                .$row["description"]."</option>";
                            else
                                echo "<option value=".$row["value"].">"
                                .$row["description"]."</option>";
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="custcompanyname" class="col-lg-4">Company Name</label>
                <div class=" col-lg-8">
                    <input type="text" class="form-control" name="custcompanyname" id="custcompanyname" 
                        placeholder="Company Name" readonly="readonly"
                        value="<?php if(isset($customerTransData->CompanyName)) echo $customerTransData->CompanyName; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="custcorebusiness" class="col-lg-4">Core Business</label>
                <div class=" col-lg-8">
                    <input type="text" class="form-control" name="custcorebusiness" id="custcorebusiness" 
                        placeholder="Company Name" readonly="readonly"
                        value="<?php if(isset($customerTransData->CoreBusiness)) echo $customerTransData->CoreBusiness; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="companyaddress" class="col-lg-4">Company Address</label>
                <div class=" col-lg-8">
                    <textarea class="form-control" name="companyaddress" id="companyaddress"  readonly="readonly"
                        placeholder="Company Address"><?php if(isset($customerTransData->CompanyAddress)) echo $customerTransData->CompanyAddress; ?></textarea>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label for="custposition" class="col-lg-4">Position</label>
                <div class=" col-lg-8">
                    <input type="text" class="form-control" name="custposition" id="custposition" 
                        placeholder="Position" readonly="readonly"
                        value="<?php if(isset($customerTransData->Position)) echo $customerTransData->Position; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="custdivision" class="col-lg-4">Division</label>
                <div class=" col-lg-8">
                    <input type="text" class="form-control" name="custdivision" id="custdivision" 
                        placeholder="Division" readonly="readonly"
                        value="<?php if(isset($customerTransData->Division)) echo $customerTransData->Division; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="custworkingperiod" class="col-lg-4">Working Period</label>
                <div class=" col-lg-8">
                    <input type="text" class="form-control" name="custworkingperiod" id="custworkingperiod" 
                        placeholder="Working Period" readonly="readonly"
                        value="<?php if(isset($customerTransData->LamaKerja)) echo $customerTransData->LamaKerja; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="businessphone" class="col-lg-4">Business Phone</label>
                <div class=" col-lg-8">
                    <div class="col-xs-7" style="padding-left:0">
                        <input type="text" class="form-control" name="businessphone" id="businessphone" 
                            placeholder="Business Phone" readonly="readonly"
                            value="<?php if(isset($customerTransData->CompanyPhone)) echo $customerTransData->CompanyPhone; ?>"/>
                    </div>
                    <div class="col-xs-1" style="padding-left:0;padding-right:0">
                        <label for="extbphone" class="control-label">Ext</label>
                    </div>
                    <div class="col-xs-4" style="padding-left:0">
                        <input type="text" class="form-control" name="extbphone" id="extbphone" placeholder="Ext" readonly="readonly"
                        value="<?php if(isset($customerTransData->ExtensionPhone)) echo $customerTransData->ExtensionPhone; ?>" />
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="monthlyincomeamount" class="col-lg-4">Monthly Income Amount</label>
                <div class=" col-lg-8">
                    <input type="number" class="form-control" name="monthlyincomeamount" id="monthlyincomeamount" 
                        placeholder="Monthly Income Amount" readonly="readonly"
                        value="<?php if(isset($customerTransData->LamaKerja)) echo $customerTransData->LamaKerja; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="bankpayrol" class="col-lg-4">Bank Payrol</label>
                <div class=" col-lg-8">
                    <select class="form-control" id="bankpayrol" disabled="disabled"  name="bankpayrol">
                        <option value="">Select Bank Payrol</option>
                        <?php
                        foreach($bankPayrolList as $row):
                            if($customerTransData->BankPayrol == $row["value"])
                                echo "<option selected='selected' value=".$row["value"].">"
                                .$row["description"]."</option>";
                            else
                                echo "<option value=".$row["value"].">"
                                .$row["description"]."</option>";
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
</fieldset>
<hr>
<fieldset>
    <legend>Emergency Contact Information
    </legend>
    <div class="form-horizontal">
        <div class="col-lg-6">
            <div class="form-group">
                <label for="relationtype" class="col-lg-4">Relationship</label>
                <div class="col-lg-8">
                    <select class="form-control" id="relationtype"  disabled="disabled" name="relationtype">
                        <option value="">Select Relationship</option>
                        <?php
                        foreach($relationshipList as $row):
                            if($customerTransData->RelationshipID == $row["value"])
                                echo "<option selected='selected' value=".$row["value"].">"
                                .$row["description"]."</option>";
                            else
                                echo "<option value=".$row["value"].">"
                                .$row["description"]."</option>";
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="ecfullname" class="col-lg-4">Full Name</label>
                <div class="col-lg-8">
                    <input type="text" class="form-control" name="ecfullname" id="ecfullname" 
                        placeholder="Full Name" readonly="readonly"
                        value="<?php if(isset($customerTransData->FullNameEC)) echo $customerTransData->FullNameEC; ?>"/>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label for="ecphone" class="col-lg-4">Phone Number</label>
                <div class="col-lg-8">
                    <input type="text" class="form-control" name="ecphone" id="ecphone" 
                    placeholder="Phone Number" readonly="readonly"
                    value="<?php if(isset($customerTransData->HomePhoneEC)) echo $customerTransData->HomePhoneEC; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="ecaddress" class="col-lg-4">Contact Address</label>
                <div class=" col-lg-8">
                    <textarea class="form-control" name="ecaddress" id="ecaddress"  readonly="readonly"
                        placeholder="Contact Address"><?php if(isset($customerTransData->HomeAddressEC)) echo $customerTransData->HomeAddressEC; ?></textarea>
                </div>
            </div>
        </div>
    </div>
</fieldset>
<hr>
<fieldset>
    <legend>Document Information</legend>
    <div class="form-horizontal">
        <div class="col-lg-6">
            <div class="form-group">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="hasktp" name="hasktp" disabled="disabled"
                    '<?php if(isset($customerTransData->HasKTP)) echo ((bool)$customerTransData->HasKTP) ? " checked='checked'" : ""; ?>'>
                    <label class="custom-control-label" for="hasktp">Has KTP</label>
                </div>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="hasnpwp" name="hasnpwp" disabled="disabled"
                    '<?php if(isset($customerTransData->HasNPWP)) echo ((bool)$customerTransData->HasNPWP) ? " checked='checked'" : ""; ?>'>
                    <label class="custom-control-label" for="hasnpwp">Has NPWP</label>
                </div>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="hasreklistrik" name="hasreklistrik" disabled="disabled"
                    '<?php if(isset($customerTransData->HasRekListrik)) echo ((bool)$customerTransData->HasRekListrik) ? " checked='checked'" : ""; ?>'>
                    <label class="custom-control-label" for="hasreklistrik">Has Rek Listrik</label>
                </div>
            </div>
            <div class="form-group">
                <label for="mailingaddresstype" class="col-lg-4">Mailing Address</label>
                <div class="col-lg-8">
                    <select class="form-control" id="mailingaddresstype"  disabled="disabled" name="mailingaddresstype">
                        <option value="">Select Mailing Address</option>
                        <?php
                        foreach($mailingAddressTypeList as $row):
                            if($customerTransData->MailingAddressType == $row["value"])
                                echo "<option selected='selected' value=".$row["value"].">"
                                .$row["description"]."</option>";
                            else
                                echo "<option value=".$row["value"].">"
                                .$row["description"]."</option>";
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="hasKK" name="hasKK" disabled="disabled"
                    '<?php if(isset($customerTransData->HasKK)) echo ((bool)$customerTransData->HasKK) ? " checked='checked'" : ""; ?>'>
                    <label class="custom-control-label" for="hasKK">Has KK</label>
                </div>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="hascc" name="hascc" disabled="disabled"
                    '<?php if(isset($customerTransData->HasCreditCard)) echo ((bool)$customerTransData->HasCreditCard) ? " checked='checked'" : ""; ?>'>
                    <label class="custom-control-label" for="hascc">Has Credit Card</label>
                </div>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="hasbankacc" name="hasbankacc" disabled="disabled"
                    '<?php if(isset($customerTransData->HasBankAccount)) echo ((bool)$customerTransData->HasBankAccount) ? " checked='checked'" : ""; ?>'>
                    <label class="custom-control-label" for="hasbankacc">Has Cover Tabungan</label>
                </div>
            </div>
        </div>

    </div>
</fieldset>
<hr>
<fieldset>
    <legend>Spouse Contact Information</legend>
    <div class="form-horizontal">
        <div class="col-lg-6">
            <div class="form-group">
                <label for="spousename" class="col-lg-4">Full Name</label>
                <div class="col-lg-8">
                    <input type="text" class="form-control" name="spousename" id="spousename" 
                        placeholder="Full Name" readonly="readonly"
                        value="<?php if(isset($customerTransData->SpouseName)) echo $customerTransData->SpouseName; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="spousektp" class="col-lg-4">KTP Number</label>
                <div class="col-lg-8">
                    <input type="text" class="form-control" name="spousektp" id="spousektp" 
                        placeholder="KTP Number" readonly="readonly"
                        value="<?php if(isset($customerTransData->SpouseKTP)) echo $customerTransData->SpouseKTP; ?>"/>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label for="spousephone" class="col-lg-4">Phone Number</label>
                <div class="col-lg-8">
                    <input type="text" class="form-control" name="spousephone" id="spousephone" 
                        placeholder="Phone Number" readonly="readonly"
                        value="<?php if(isset($customerTransData->SpousePhone)) echo $customerTransData->SpousePhone; ?>"/>
                </div>
            </div>
        </div>
    </div>
</fieldset>
<hr>
<fieldset>
    <legend>Survey Information</legend>
    <div class="form-horizontal">
        <div class="col-lg-6">
            <div class="form-group">
                <label for="surveyaddress" class="col-lg-4">Survey Address</label>
                <div class="col-lg-8">
                    <textarea class="form-control" name="surveyaddress" id="surveyaddress" readonly="readonly"
                        placeholder="Survey Address"><?php if(isset($customerTransData->SurveyAddress)) echo $customerTransData->SurveyAddress; ?></textarea>                        
                </div>
            </div>
            <div class="form-group">
                <label for="surveynotes" class="col-lg-4">Survey Notes</label>
                <div class="col-lg-8">
                    <textarea class="form-control" name="surveynotes" id="surveynotes" readonly="readonly"
                        placeholder="Survey Notes"><?php if(isset($customerTransData->SurveyNotes)) echo $customerTransData->SurveyNotes; ?></textarea>                        
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label for="surveydate" class="col-lg-4">Survey Date</label>
                <div class="col-lg-8">
                <input type="text" class="form-control date-picker single date" name="surveydate" id="surveydate" 
                        placeholder="Survey Date" readonly="readonly"
                        value="<?php if(isset($customerTransData->DateOfSUrvey) 
                            && $customerTransData->DateOfSUrvey != '0000-00-00 00:00:00') 
                        echo date_format(new DateTime($customerTransData->DateOfSUrvey),'l, d/m/Y'). 
                        date_format(new DateTime($customerTransData->TimeOfSUrvey),' H:i:s'); ?>"/>
                </div>
            </div>
        </div>
    </div>
</fieldset>
<form action='<?php echo $form_action; ?>' method='POST'>
    <?php $this->load->View($process_info_view); ?>
    <hr>
    <fieldset>
        <legend>Pick Up Information</legend>
        <div class="form-horizontal">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="pickupstatus" class="col-lg-4">Pickup Status</label>
                    <div class="col-lg-8">
                        <select class="form-control" id="pickupstatus"  name="pickupstatus" required="true">
                            <option value="">Select Pickup Status</option>
                            <?php
                            foreach($pickupStatusList as $row):
                                if(isset($customerTransData->PickupStatus) && $customerTransData->PickupStatus == $row["value"])
                                    echo "<option selected='selected' value=".$row["value"].">"
                                    .$row["description"]."</option>";
                                else
                                    echo "<option value=".$row["value"].">"
                                    .$row["description"]."</option>";
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="pickupby" class="col-lg-4">Pickup By</label>
                    <div class=" col-lg-8">
                        <textarea class="form-control" name="pickupby" id="pickupby"
                            placeholder="Pickup By"><?php if(isset($customerTransData->PickupBy)) echo $customerTransData->PickupBy; ?></textarea>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="pickupnotes" class="col-lg-4">Pickup Notes</label>
                    <div class=" col-lg-8">
                        <textarea class="form-control" name="pickupnotes" id="pickupnotes"
                            placeholder="Notes"><?php if(isset($customerTransData->PickupNotes)) echo $customerTransData->PickupNotes; ?></textarea>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <div style="margin-top:50px"></div>
    <div class="act-float">
        <?php echo anchor('appcustomer/inquiry', 'Cancel', 'id="btnCancel" class="btn btn-default btn-cancel"');?>
        <button type="submit" class="btn btn-primary" id="btnSave">
            <i class="glyphicon glyphicon-briefcase"></i>&nbsp;&nbsp;Save Pick Up
        </button>

        <?php 
        
        if($customerTransData->PickupStatus=="3"){
        echo anchor('appcustomer/entrydone/save', 
            '<i class="glyphicon glyphicon-check"></i>&nbsp;Entry Done', 
            'id="saveentrydone" name="saveentrydone" class="btn btn-default btn-success"');
        } ?>

        
        <?php 
        
        echo anchor('appcustomer/followup/saveback', 
        '<i class="glyphicon glyphicon-earphone"></i>&nbsp;Back to Followup', 
        'id="savebackfollowup" name="savebackfollowup" class="btn btn-default btn-warning"');
        
        ?>

        <button id="print" type="button" name="print" class="btn btn-info btn-print">
            <i class="glyphicon glyphicon-print"></i>&nbsp;Print Document
        </button>

    </div>
</form>
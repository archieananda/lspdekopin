<form action='<?php echo $form_action; ?>' method='POST'>
    <fieldset>
        <legend>General Information</legend>
        <input type="hidden" name="transid" id="transid" 
            value="<?php if(isset($customerTransData->TransID)) echo $customerTransData->TransID; ?>"/>
        <div class="form-horizontal">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="referalid" class="col-lg-4">Referal ID</label>
                    <div class=" col-lg-8">
                        <input type="text" class="form-control" name="referalid" id="referalid" 
                            readonly="readonly"
                            value="<?php if(isset($customerTransData->RefferalID)) echo $customerTransData->RefferalID; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="sales" class="col-lg-4">Sales</label>
                    <div class=" col-lg-8">
                        <?php if($customerTransData->StatusPengajuan<=2){ ?>
                            <input type="hidden" class="form-control" name="salesid" id="salesid" 
                                value="<?php if(isset($userLoginData->user_id)) echo $userLoginData->user_id; ?>"/>
                            <input type="text" class="form-control" name="sales" id="sales" 
                                readonly="readonly"
                                value="<?php if(isset($userLoginData->user_nama)) echo $userLoginData->user_nama; ?>"/>
                        <?php }else{ ?>
                            <input type="hidden" class="form-control" name="salesid" id="salesid" 
                                value="<?php if(isset($customerTransData->SalesID)) echo $customerTransData->SalesID; ?>"/>
                            <input type="text" class="form-control" name="sales" id="sales" 
                                readonly="readonly"
                                value="<?php if(isset($customerTransData->user_nama)) echo $customerTransData->user_nama; ?>"/>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="appstatus" class="col-lg-4">Referal Reference</label>
                    <div class=" col-lg-8">
                        <input type="text" class="form-control" name="appstatus" id="appstatus" 
                            readonly="readonly"
                            value="<?php if(isset($customerTransData->RefferalRef)) echo $customerTransData->RefferalRef; ?>"/>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="applicationdate" class="col-lg-4">Application Date</label>
                    <div class=" col-lg-8">
                        <input type="text" class="form-control" name="applicationdate" id="applicationdate" 
                            readonly="readonly"
                            value="<?php if(isset($customerTransData->TglPengajuan)) 
                            echo date_format(new DateTime($customerTransData->TglPengajuan),'d-M-Y'); ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="applicationstatus" class="col-lg-4">Application Status</label>
                    <div class=" col-lg-8">
                        <input type="text" class="form-control" name="applicationstatus" id="applicationstatus" 
                            readonly="readonly"
                            value="<?php if(isset($customerTransData->StatusPengajuanDesc)) echo $customerTransData->StatusPengajuanDesc; ?>"/>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <hr>
    <fieldset>
        <legend>Product Information</legend>
        <div class="form-horizontal">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="producttypedesc" class="col-lg-4">Product Type</label>
                    <div class=" col-lg-8">
                        <input type="hidden" name="producttype" id="producttype" 
                        value="<?php if(isset($customerTransData->ProductType)) echo $customerTransData->ProductType; ?>"/>
                        <input type="text" class="form-control" name="producttypedesc" id="producttypedesc" 
                            readonly="readonly"
                            value="<?php if(isset($customerTransData->ProductTypeDesc)) echo $customerTransData->ProductTypeDesc; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="productname" class="col-lg-4">Product</label>
                    <div class=" col-lg-8">
                        <input type="hidden" name="productid" id="productid" 
                            value="<?php if(isset($customerTransData->ProductID)) echo $customerTransData->ProductID; ?>"/>
                        <input type="text" class="form-control" name="productname" id="productname" 
                            readonly="readonly"
                            value="<?php if(isset($customerTransData->ProductName)) echo $customerTransData->ProductName; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="tenor" class="col-lg-4">Tenure Month</label>
                    <div class=" col-lg-8">
                        <input type="text" class="form-control" name="tenor" id="tenor" 
                            placeholder="Tennure"
                            value="<?php if(isset($customerTransData->TennureMonth)) echo $customerTransData->TennureMonth; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="effrate" class="col-lg-4">Effective Rate</label>
                    <div class=" col-lg-8">
                        <input type="text" class="form-control" name="effrate" id="effrate" 
                        placeholder="Effective Rate"
                        value="<?php if(isset($customerTransData->EffectiveRate)) echo $customerTransData->EffectiveRate; ?>"/>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="bankchannel" class="col-lg-4">Bank Channel</label>
                    <div class=" col-lg-8">
                        <select class="form-control" id="bankchannel"  name="bankchannel">
                            <option value="">Select Bank Channel</option>
                            <?php
                            foreach($channelList as $row):
                                if($customerTransData->ChannelID == $row["value"])
                                    echo "<option selected='selected' value=".$row["value"].">"
                                    .$row["description"]."</option>";
                                else
                                    echo "<option value=".$row["value"].">"
                                    .$row["description"]."</option>";
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="plafond" class="col-lg-4">Plafond</label>
                    <div class=" col-lg-8">
                        <input type="text" class="form-control" name="plafond" id="plafond" 
                            placeholder="Plafond"
                            value="<?php if(isset($customerTransData->Plafond)) echo $customerTransData->Plafond; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="flatrate" class="col-lg-4">Flat Rate</label>
                    <div class=" col-lg-8">
                        <input type="text" class="form-control" name="flatrate" id="flatrate" 
                            placeholder="Flat Rate"
                            value="<?php if(isset($customerTransData->FlatRate)) echo $customerTransData->FlatRate; ?>"/>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <hr>
    <fieldset>
        <legend>Existing Credit Card</legend>
        <div class="form-horizontal">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="cctype" class="col-lg-4">Credit Card Type</label>
                    <div class=" col-lg-8">
                        <select class="form-control" id="cctype" name="cctype">
                            <option value="">Select Card Type</option>
                            <?php
                            foreach($cardTypeList as $row):
                                if($customerTransData->CCTypeID == $row["value"])
                                    echo "<option selected='selected' value=".$row["value"].">"
                                    .$row["description"]."</option>";
                                else
                                    echo "<option value=".$row["value"].">"
                                    .$row["description"]."</option>";
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="cclimit" class="col-lg-4">Limit</label>
                    <div class=" col-lg-8">
                        <input type="number" class="form-control" name="cclimit" id="cclimit" placeholder="Limit"
                        value="<?php if(isset($customerTransData->Limit)) echo $customerTransData->Limit; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="ccexpiredmonth" class="col-lg-4">Expired</label>
                    <div class=" col-lg-8">
                        <div class="col-xs-7" style="padding-left:0">
                            <select class="form-control" id="ccexpiredmonth"  name="ccexpiredmonth">
                                <option value="">Month</option>
                                <?php
                                foreach($monthList as $row):
                                    if($customerTransData->CCExpiredMonth == $row["value"])
                                        echo "<option selected='selected' value=".$row["value"].">"
                                        .$row["description"]."</option>";
                                    else
                                        echo "<option value=".$row["value"].">"
                                        .$row["description"]."</option>";
                                endforeach;
                                ?>
                            </select>
                        </div>
                        <div class="col-xs-4" style="padding-left:0">
                            <input type="number" class="form-control" name="ccexpiredyear" id="ccexpiredyear" placeholder="Year"
                            value="<?php if(isset($customerTransData->CCExpiredYear)) echo $customerTransData->CCExpiredYear; ?>"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="ccbankid" class="col-lg-4">Credit Card Bank</label>
                    <div class=" col-lg-8">
                        <select class="form-control" id="ccbankid"  name="ccbankid">
                            <option value="">Select CC Bank</option>
                            <?php
                            foreach($CCBankList as $row):
                                if($customerTransData->CCBankID == $row["value"])
                                    echo "<option selected='selected' value=".$row["value"].">"
                                    .$row["description"]."</option>";
                                else
                                    echo "<option value=".$row["value"].">"
                                    .$row["description"]."</option>";
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="ccactivemonth" class="col-lg-4">Active From</label>
                    <div class=" col-lg-8">
                        <div class="col-xs-7" style="padding-left:0">
                            <select class="form-control" id="ccactivemonth"  name="ccactivemonth">
                                <option value="">Month</option>
                                <?php
                                foreach($monthList as $row):
                                    if($customerTransData->CCActiveMonth == $row["value"])
                                        echo "<option selected='selected' value=".$row["value"].">"
                                        .$row["description"]."</option>";
                                    else
                                        echo "<option value=".$row["value"].">"
                                        .$row["description"]."</option>";
                                endforeach;
                                ?>
                            </select>
                        </div>
                        <div class="col-xs-4" style="padding-left:0">
                            <input type="number" class="form-control" name="ccactiveyear" id="ccactiveyear" placeholder="Year"
                            value="<?php if(isset($customerTransData->CCActiveYear)) echo $customerTransData->CCActiveYear; ?>"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <hr>
    <fieldset>
        <legend>Personal Information</legend>
        <div class="form-horizontal">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="custfullname" class="col-lg-4">Full Name</label>
                    <div class=" col-lg-8">
                        <input type="text" class="form-control" name="custfullname" id="custfullname" placeholder="Full Name"
                        value="<?php if(isset($customerTransData->FullName)) echo $customerTransData->FullName; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="custmobileno" class="col-lg-4">Mobile No</label>
                    <div class=" col-lg-8">
                        <input type="text" class="form-control" name="custmobileno" id="custmobileno" placeholder="Mobile No"
                        value="<?php if(isset($customerTransData->MobileNo)) echo $customerTransData->MobileNo; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="custhomephone" class="col-lg-4">Home Phone</label>
                    <div class=" col-lg-8">
                        <input type="text" class="form-control" name="custhomephone" id="custhomephone" placeholder="Home Phone"
                        value="<?php if(isset($customerTransData->HomePhone)) echo $customerTransData->HomePhone; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="custemailaddress" class="col-lg-4">Email Address</label>
                    <div class=" col-lg-8">
                        <input type="text" class="form-control" name="custemailaddress" id="custemailaddress" placeholder="Email Address"
                        value="<?php if(isset($customerTransData->EmailAddress)) echo $customerTransData->EmailAddress; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="custeducation" class="col-lg-4">Education</label>
                    <div class=" col-lg-8">
                        <select class="form-control" id="custeducation"  name="custeducation">
                            <option value="">Select Education</option>
                            <?php
                            foreach($educationList as $row):
                                if($customerTransData->EducationID == $row["value"])
                                    echo "<option selected='selected' value=".$row["value"].">"
                                    .$row["description"]."</option>";
                                else
                                    echo "<option value=".$row["value"].">"
                                    .$row["description"]."</option>";
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="ktpno" class="col-lg-4">KTP Number</label>
                    <div class=" col-lg-8">
                        <input type="text" class="form-control" name="ktpno" id="ktpno" placeholder="KTP Number"
                        value="<?php if(isset($customerTransData->KTPNumber)) echo $customerTransData->KTPNumber; ?>"/>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="custresidencestatus" class="col-lg-4">Residence Status</label>
                    <div class=" col-lg-8">
                        <select class="form-control" id="custresidencestatus"  name="custresidencestatus">
                            <option value="">Select Residence Status</option>
                            <?php
                            foreach($residenceStatusList as $row):
                                if($customerTransData->ResidenceStatus == $row["value"])
                                    echo "<option selected='selected' value=".$row["value"].">"
                                    .$row["description"]."</option>";
                                else
                                    echo "<option value=".$row["value"].">"
                                    .$row["description"]."</option>";
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="custmothersurename" class="col-lg-4">Mother Maiden Name</label>
                    <div class=" col-lg-8">
                        <input type="text" class="form-control" name="custmothersurename" id="custmothersurename" placeholder="Mother Sure Name"
                        value="<?php if(isset($customerTransData->MotherSureName)) echo $customerTransData->MotherSureName; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="custlivingperiod" class="col-lg-4">Living Period</label>
                    <div class=" col-lg-8">
                        <input type="text" class="form-control" name="custlivingperiod" id="custlivingperiod" placeholder="Living Period"
                        value="<?php if(isset($customerTransData->LivingPeriod)) echo $customerTransData->LivingPeriod; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="custdependents" class="col-lg-4"># Dependents</label>
                    <div class=" col-lg-8">
                        <input type="number" class="form-control" name="custdependents" id="custdependents" placeholder="# Dependents"
                        value="<?php if(isset($customerTransData->JmlTanggungan)) echo $customerTransData->JmlTanggungan; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="address" class="col-lg-4">Residence Address</label>
                    <div class=" col-lg-8">
                        <textarea class="form-control" name="address" id="address"
                            placeholder="Residence Address"><?php if(isset($customerTransData->Address)) echo $customerTransData->Address; ?></textarea>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <hr>
    <fieldset>
        <legend>Occupation / Business Information</legend>
        <div class="form-horizontal">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="custoccupation" class="col-lg-4">Occupation</label>
                    <div class=" col-lg-8">
                        <select class="form-control" id="custoccupation"  name="custoccupation">
                            <option value="">Select Occupation</option>
                            <?php
                            foreach($occupationList as $row):
                                if($customerTransData->Occupation == $row["value"])
                                    echo "<option selected='selected' value=".$row["value"].">"
                                    .$row["description"]."</option>";
                                else
                                    echo "<option value=".$row["value"].">"
                                    .$row["description"]."</option>";
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="custcompanyname" class="col-lg-4">Company Name</label>
                    <div class=" col-lg-8">
                        <input type="text" class="form-control" name="custcompanyname" id="custcompanyname" 
                            placeholder="Company Name"
                            value="<?php if(isset($customerTransData->CompanyName)) echo $customerTransData->CompanyName; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="custcorebusiness" class="col-lg-4">Core Business</label>
                    <div class=" col-lg-8">
                        <input type="text" class="form-control" name="custcorebusiness" id="custcorebusiness" 
                            placeholder="Company Name"
                            value="<?php if(isset($customerTransData->CoreBusiness)) echo $customerTransData->CoreBusiness; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="companyaddress" class="col-lg-4">Company Address</label>
                    <div class=" col-lg-8">
                        <textarea class="form-control" name="companyaddress" id="companyaddress" 
                            placeholder="Company Address"><?php if(isset($customerTransData->CompanyAddress)) echo $customerTransData->CompanyAddress; ?></textarea>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="custposition" class="col-lg-4">Position</label>
                    <div class=" col-lg-8">
                        <input type="text" class="form-control" name="custposition" id="custposition" 
                            placeholder="Position"
                            value="<?php if(isset($customerTransData->Position)) echo $customerTransData->Position; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="custdivision" class="col-lg-4">Division</label>
                    <div class=" col-lg-8">
                        <input type="text" class="form-control" name="custdivision" id="custdivision" 
                            placeholder="Division"
                            value="<?php if(isset($customerTransData->Division)) echo $customerTransData->Division; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="custworkingperiod" class="col-lg-4">Working Period</label>
                    <div class=" col-lg-8">
                        <input type="text" class="form-control" name="custworkingperiod" id="custworkingperiod" 
                            placeholder="Working Period"
                            value="<?php if(isset($customerTransData->LamaKerja)) echo $customerTransData->LamaKerja; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="businessphone" class="col-lg-4">Business Phone</label>
                    <div class=" col-lg-8">
                        <div class="col-xs-7" style="padding-left:0">
                            <input type="text" class="form-control" name="businessphone" id="businessphone" 
                                placeholder="Business Phone"
                                value="<?php if(isset($customerTransData->CompanyPhone)) echo $customerTransData->CompanyPhone; ?>"/>
                        </div>
                        <div class="col-xs-1" style="padding-left:0;padding-right:0">
                            <label for="extbphone" class="control-label">Ext</label>
                        </div>
                        <div class="col-xs-4" style="padding-left:0">
                            <input type="text" class="form-control" name="extbphone" id="extbphone" placeholder="Ext"
                            value="<?php if(isset($customerTransData->ExtensionPhone)) echo $customerTransData->ExtensionPhone; ?>" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="monthlyincomeamount" class="col-lg-4">Monthly Income Amount</label>
                    <div class=" col-lg-8">
                        <input type="number" class="form-control" name="monthlyincomeamount" id="monthlyincomeamount" 
                            placeholder="Monthly Income Amount"
                            value="<?php if(isset($customerTransData->LamaKerja)) echo $customerTransData->LamaKerja; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="bankpayrol" class="col-lg-4">Bank Payrol</label>
                    <div class=" col-lg-8">
                        <select class="form-control" id="bankpayrol"  name="bankpayrol">
                            <option value="">Select Bank Payrol</option>
                            <?php
                            foreach($bankPayrolList as $row):
                                if($customerTransData->BankPayrol == $row["value"])
                                    echo "<option selected='selected' value=".$row["value"].">"
                                    .$row["description"]."</option>";
                                else
                                    echo "<option value=".$row["value"].">"
                                    .$row["description"]."</option>";
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <hr>
    <fieldset>
        <legend>Emergency Contact Information
        </legend>
        <div class="form-horizontal">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="relationtype" class="col-lg-4">Relationship</label>
                    <div class="col-lg-8">
                        <select class="form-control" id="relationtype"  name="relationtype">
                            <option value="">Select Relationship</option>
                            <?php
                            foreach($relationshipList as $row):
                                if($customerTransData->RelationshipID == $row["value"])
                                    echo "<option selected='selected' value=".$row["value"].">"
                                    .$row["description"]."</option>";
                                else
                                    echo "<option value=".$row["value"].">"
                                    .$row["description"]."</option>";
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="ecfullname" class="col-lg-4">Full Name</label>
                    <div class="col-lg-8">
                        <input type="text" class="form-control" name="ecfullname" id="ecfullname" 
                            placeholder="Full Name"
                            value="<?php if(isset($customerTransData->FullNameEC)) echo $customerTransData->FullNameEC; ?>"/>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="ecphone" class="col-lg-4">Phone Number</label>
                    <div class="col-lg-8">
                        <input type="text" class="form-control" name="ecphone" id="ecphone" 
                        placeholder="Phone Number"
                        value="<?php if(isset($customerTransData->HomePhoneEC)) echo $customerTransData->HomePhoneEC; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="ecaddress" class="col-lg-4">Contact Address</label>
                    <div class=" col-lg-8">
                        <textarea class="form-control" name="ecaddress" id="ecaddress" 
                            placeholder="Contact Address"><?php if(isset($customerTransData->HomeAddressEC)) echo $customerTransData->HomeAddressEC; ?></textarea>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <hr>
    <fieldset>
        <legend>Document Information</legend>
        <div class="form-horizontal">
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="hasktp" name="hasktp"
                        '<?php if(isset($customerTransData->HasKTP)) echo ((bool)$customerTransData->HasKTP) ? " checked='checked'" : ""; ?>'>
                        <label class="custom-control-label" for="hasktp">Has KTP</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="hasnpwp" name="hasnpwp"
                        '<?php if(isset($customerTransData->HasNPWP)) echo ((bool)$customerTransData->HasNPWP) ? " checked='checked'" : ""; ?>'>
                        <label class="custom-control-label" for="hasnpwp">Has NPWP</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="hasreklistrik" name="hasreklistrik"
                        '<?php if(isset($customerTransData->HasRekListrik)) echo ((bool)$customerTransData->HasRekListrik) ? " checked='checked'" : ""; ?>'>
                        <label class="custom-control-label" for="hasreklistrik">Has Rek Listrik</label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="mailingaddresstype" class="col-lg-4">Mailing Address</label>
                    <div class="col-lg-8">
                        <select class="form-control" id="mailingaddresstype"  name="mailingaddresstype">
                            <option value="">Select Mailing Address</option>
                            <?php
                            foreach($mailingAddressTypeList as $row):
                                if($customerTransData->MailingAddressType == $row["value"])
                                    echo "<option selected='selected' value=".$row["value"].">"
                                    .$row["description"]."</option>";
                                else
                                    echo "<option value=".$row["value"].">"
                                    .$row["description"]."</option>";
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="hasKK" name="hasKK"
                        '<?php if(isset($customerTransData->HasKK)) echo ((bool)$customerTransData->HasKK) ? " checked='checked'" : ""; ?>'>
                        <label class="custom-control-label" for="hasKK">Has KK</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="hascc" name="hascc"
                        '<?php if(isset($customerTransData->HasCreditCard)) echo ((bool)$customerTransData->HasCreditCard) ? " checked='checked'" : ""; ?>'>
                        <label class="custom-control-label" for="hascc">Has Credit Card</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="hasbankacc" name="hasbankacc"
                        '<?php if(isset($customerTransData->HasBankAccount)) echo ((bool)$customerTransData->HasBankAccount) ? " checked='checked'" : ""; ?>'>
                        <label class="custom-control-label" for="hasbankacc">Has Cover Tabungan</label>
                    </div>
                </div>
            </div>

        </div>
    </fieldset>
    <hr>
    <fieldset>
        <legend>Spouse Contact Information</legend>
        <div class="form-horizontal">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="spousename" class="col-lg-4">Full Name</label>
                    <div class="col-lg-8">
                        <input type="text" class="form-control" name="spousename" id="spousename" 
                            placeholder="Full Name"
                            value="<?php if(isset($customerTransData->SpouseName)) echo $customerTransData->SpouseName; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="spousektp" class="col-lg-4">KTP Number</label>
                    <div class="col-lg-8">
                        <input type="text" class="form-control" name="spousektp" id="spousektp" 
                            placeholder="KTP Number"
                            value="<?php if(isset($customerTransData->SpouseKTP)) echo $customerTransData->SpouseKTP; ?>"/>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="spousephone" class="col-lg-4">Phone Number</label>
                    <div class="col-lg-8">
                        <input type="text" class="form-control" name="spousephone" id="spousephone" 
                            placeholder="Phone Number"
                            value="<?php if(isset($customerTransData->SpousePhone)) echo $customerTransData->SpousePhone; ?>"/>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <hr>
    <fieldset>
        <legend>Survey Information</legend>
        <div class="form-horizontal">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="surveyaddress" class="col-lg-4">Survey Address</label>
                    <div class="col-lg-8">
                        <textarea class="form-control" name="surveyaddress" id="surveyaddress"
                            placeholder="Survey Address"><?php if(isset($customerTransData->SurveyAddress)) echo $customerTransData->SurveyAddress; ?></textarea>                        
                    </div>
                </div>
                <div class="form-group">
                    <label for="surveynotes" class="col-lg-4">Survey Notes</label>
                    <div class="col-lg-8">
                        <textarea class="form-control" name="surveynotes" id="surveynotes"
                            placeholder="Survey Notes"><?php if(isset($customerTransData->SurveyNotes)) echo $customerTransData->SurveyNotes; ?></textarea>                        
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="surveydate" class="col-lg-4">Survey Date</label>
                    <div class="col-lg-8">
                    <input type="text" class="form-control date-picker single date" name="surveydate" id="surveydate" 
                            placeholder="Survey Date"
                            value="<?php if(isset($customerTransData->DateOfSUrvey) 
                                && $customerTransData->DateOfSUrvey != '0000-00-00 00:00:00') 
                            echo date_format(new DateTime($customerTransData->DateOfSUrvey),'l, d/m/Y'). 
                            date_format(new DateTime($customerTransData->TimeOfSUrvey),' H:i:s'); ?>"/>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <hr>
    <fieldset>
        <legend>Followup Information</legend>
        <div class="form-horizontal">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="followupstatus" class="col-lg-4">Followup Status</label>
                    <div class="col-lg-8">
                        <select class="form-control" id="followupstatus"  name="followupstatus">
                            <option value="">Select Followup Status</option>
                            <?php
                            foreach($followupStatusList as $row):
                                if($customerTransData->FolowupStatus == $row["value"])
                                    echo "<option selected='selected' value=".$row["value"].">"
                                    .$row["description"]."</option>";
                                else
                                    echo "<option value=".$row["value"].">"
                                    .$row["description"]."</option>";
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="notes" class="col-lg-4">Notes</label>
                    <div class=" col-lg-8">
                        <textarea class="form-control" name="notes" id="notes"
                            placeholder="Notes"><?php if(isset($customerTransData->Notes)) echo $customerTransData->Notes; ?></textarea>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <div style="margin-top:50px"></div>
    <div class="act-float">
        <?php echo anchor('appcustomer/inquiry', 'Cancel', 'id="btnCancel" class="btn btn-default btn-cancel"');?>
        <button type="submit" class="btn btn-primary" id="btnSave">
            <i class="glyphicon glyphicon-save"></i>&nbsp;&nbsp;Save
        </button>
        <?php if($customerTransData->FolowupStatus=="1"){
        echo anchor('appcustomer/followup/savedataentry', 
            '<i class="glyphicon glyphicon-save-file"></i>&nbsp;Data Entry', 
            'id="savedataentry" name="savedataentry" class="btn btn-default btn-success"');
        } ?>
        <?php if($customerTransData->StatusPengajuan!="Junk" && $customerTransData->FolowupStatus!="1"){
        echo anchor('appcustomer/followup/savejunk', 
            '<i class="glyphicon glyphicon-trash"></i>&nbsp;Save to Junk', 
            'id="savetojunk" name="savetojunk" class="btn btn-default btn-danger"');
        } ?>
        <button id="print" type="button" name="print" class="btn btn-info btn-print">
            <i class="glyphicon glyphicon-print"></i>&nbsp;Print Document
        </button>
        <!-- <button name="savetojunk" id="savetojunk" class="btn btn-danger">
            <i class="glyphicon glyphicon-trash"></i>&nbsp;&nbsp;Save to Junk</Button> -->
    </div>
</form>
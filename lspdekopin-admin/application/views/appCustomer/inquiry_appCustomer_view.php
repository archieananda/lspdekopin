    <fieldset class="do-search">
        <legend>Search Criteria</legend>
        <div class="form-horizontal">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="fullname" class="col-lg-4">Customer Name</label>
                    <div class=" col-lg-8">
                        <input type='hidden' name='leaderid' id='leaderid' 
                            value='<?php if(isset($leaderID)) echo $leaderID; ?>' />
                        <input type="text" class="form-control" name="fullname" id="fullname" 
                            placeholder="Customer Name" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="ktp" class="col-lg-4">KTP Number</label>
                    <div class=" col-lg-8">
                        <input type="text" class="form-control" name="ktp" id="ktp"
                        placeholder="KTP Number" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="appstatus" class="col-lg-4">App Status</label>
                    <div class=" col-lg-8">
                        <select name="appstatus" id="appstatus"  class="form-control">
                            <option value="">Select Application Status</option>
                                <?php
                                foreach($applicationStatusList as $type_list):
                                    echo "<option value=".$type_list["value"].">"
                                        .$type_list["description"]."</option>";
                                endforeach;
                                ?>
                            </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="salesid" class="col-lg-4">Sales</label>
                    <div class=" col-lg-8">
                    <?php if (isset($SalesID)) { ?>
                        <input type='hidden' value='<?php echo $SalesID; ?>' id='salesid' name='salesid' />
                        <input readonly='readonly' type='text' class='form-control' value='<?php echo $SalesName; ?>' id='salesname' name='salesname' />
                    <?php }else{?>
                        <select name="salesid" id="salesid"  class="form-control">
                            <option value="">Select Sales</option>
                                <?php
                                foreach($salesList as $row):
                                    echo "<option value=".$row["value"].">"
                                        .$row["description"]."</option>";
                                endforeach;
                                ?>
                        </select>
                    <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="producttypeid" class="col-lg-4">Product Type</label>
                    <div class=" col-lg-8">
                        <select name="producttypeid" id="producttypeid"  class="form-control">
                            <?php if(count($productTypeList) > 1) { ?>
                            <option value="">Select Product Type</option>
                            <?php } ?>
                                <?php
                                foreach($productTypeList as $prod_type_list):
                                    echo "<option value=".$prod_type_list["value"].">"
                                        .$prod_type_list["description"]."</option>";
                                endforeach;
                                ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="productname" class="col-lg-4">Product Name</label>
                    <div class=" col-lg-8">
                        <input type="text" class="form-control" name="productname" id="productname"
                         placeholder="Product Name"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="trxfrom" class="col-lg-4">Transaction Date</label>
                    <div class="col-lg-8">
                        <div class="col-xs-5" style="padding-left:0">
                            <input type="text" class="date-picker from form-control" name="trxfrom" id="trxfrom" 
                            placeholder="From"/>
                        </div>
                        <div class="col-xs-1" style="padding-left:0;padding-right:0">
                        <span>to</span>
                        </div>
                        <div class="col-xs-5" style="padding-left:0">
                            <input type="text" class="date-picker to form-control" name="trxto" id="trxto"
                            placeholder="To"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <div class="col-lg-offset-5">
                        <button class="btn btn-primary" name="search" id="search" 
                            urlaction="<?php echo base_url(); ?>/appcustomer/inquiry/search">
                            <i class="glyphicon glyphicon-search"></i>&nbsp;Search
                        </button>
                        <button class="btn btn-default" name="reset" id="reset">
                            <i class="glyphicon glyphicon-refresh"></i>&nbsp;Reset
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    
<input id="appCustomerListRaw" type="hidden" value='<?php echo $appCustListData; ?>' />
<table id="appCustomerList" class="table table-striped table-bordered" cellspacing="0" width="100%"></table>

<?php if(isset($leaderID)) { ?>

<div clas="form-horizontal">
    <dic class="col-lg-2">
        <div class="form-group">
            <button class="btn btn-success" name="assigntosales" id="assigntosales" 
                urlaction="<?php echo base_url(); ?>/appcustomer/assign/save">
                <i class="glyphicon glyphicon-transfer"></i>&nbsp;Assign To Sales
            </button>
        </div>
    </div>
    <dic class="col-lg-6">
        <div class="form-group">
            <select name="assignedsales" id="assignedsales"  class="form-control">
                <option value="">Sales To Be Assigned</option>
                    <?php
                    foreach($salesList as $row):
                        echo "<option value=".$row["value"].">"
                            .$row["description"]."</option>";
                    endforeach;
                    ?>
            </select>
        </div>
    </div>
</div>    

<?php } ?>



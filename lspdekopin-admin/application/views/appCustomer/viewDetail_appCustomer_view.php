<?php $this->load->View($process_info_view); ?>
<hr>
<fieldset>
    <legend>Follow Up Information</legend>
    <div class="form-horizontal">
        <div class="col-lg-6">
            <div class="form-group">
                <label for="followupsatus" class="col-lg-4">Followup Status</label>
                <div class=" col-lg-8">
                    <input type="text" class="form-control" name="followupsatus" id="followupsatus" placeholder="Followup Status"
                    readonly="readonly"
                    value="<?php if(isset($customerTransData->FollowupStatusDesc)) echo $customerTransData->FollowupStatusDesc; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="dataentrydate" class="col-lg-4">Date Entry Date</label>
                <div class=" col-lg-8">
                    <input type="text" class="form-control" name="dataentrydate" id="dataentrydate" 
                            readonly="readonly"
                            value="<?php if(isset($customerTransData->DataEntryDate)) 
                            echo date_format(new DateTime($customerTransData->DataEntryDate),'d-M-Y | h:m:s'); ?>"/>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label for="notes" class="col-lg-4">Notes</label>
                <div class=" col-lg-8">
                    <textarea class="form-control" name="notes" id="notes"
                        readonly="readonly"
                        placeholder="Notes"><?php if(isset($customerTransData->Notes)) echo $customerTransData->Notes; ?></textarea>
                </div>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Pick Up Information</legend>
    <div class="form-horizontal">
        <div class="col-lg-6">
            <div class="form-group">
                <label for="pickupstatus" class="col-lg-4">Pickup Status</label>
                <div class=" col-lg-8">
                    <input type="text" class="form-control" name="pickupstatus" id="pickupstatus" placeholder="Pickup Status"
                    readonly="readonly"
                    value="<?php if(isset($customerTransData->PickupStatusDesc)) echo $customerTransData->PickupStatusDesc; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="pickupby" class="col-lg-4">Pickup By</label>
                <div class=" col-lg-8">
                    <textarea class="form-control" name="pickupby" id="pickupby"
                        readonly="readonly"
                        placeholder="Pickup By"><?php if(isset($customerTransData->PickupBy)) echo $customerTransData->PickupBy; ?></textarea>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label for="pickupdate" class="col-lg-4">Last Pickup Date</label>
                <div class=" col-lg-8">
                <input type="text" class="form-control" name="pickupdate" id="pickupdate" 
                            readonly="readonly"
                            value="<?php if(isset($customerTransData->PickupStatusDate)) 
                            echo date_format(new DateTime($customerTransData->PickupStatusDate),'d-M-Y | h:m:s'); ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="pickupnotes" class="col-lg-4">Pickup Notes</label>
                <div class=" col-lg-8">
                    <textarea class="form-control" name="pickupnotes" id="pickupnotes"
                        readonly="readonly"
                        placeholder="Notes"><?php if(isset($customerTransData->PickupNotes)) echo $customerTransData->PickupNotes; ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="entrydonedate" class="col-lg-4">Entry Done Date</label>
                <div class=" col-lg-8">
                <input type="text" class="form-control" name="entrydonedate" id="entrydonedate" 
                            readonly="readonly"
                            value="<?php if(isset($customerTransData->EntryDoneDate)) 
                            echo date_format(new DateTime($customerTransData->EntryDoneDate),'d-M-Y | h:m:s'); ?>"/>
                </div>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Submission & Approval Information</legend>
    <div class="form-horizontal">
        <div class="col-lg-6">
            <div class="form-group">
                <label for="submitdate" class="col-lg-4">Submit Date</label>
                <div class=" col-lg-8">
                    <input type="text" class="form-control" name="submitdate" id="submitdate" 
                            readonly="readonly"
                            value="<?php if(isset($customerTransData->BankSubmitDate)) 
                            echo date_format(new DateTime($customerTransData->BankSubmitDate),'d-M-Y | h:m:s'); ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="approvalresult" class="col-lg-4">Approval Result</label>
                <div class=" col-lg-8">
                    <input type="text" class="form-control" name="approvalresult" id="approvalresult" placeholder="Approval Result"
                    readonly="readonly"
                    value="<?php if(isset($customerTransData->ApprovalResultDesc)) echo $customerTransData->ApprovalResultDesc; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="rejectreason" class="col-lg-4">Reject Reason</label>
                <div class=" col-lg-8">
                    <input type="text" class="form-control" name="rejectreason" id="rejectreason" placeholder="Reject Reason"
                    readonly="readonly"
                    value="<?php if(isset($customerTransData->RejectReasonDesc)) echo $customerTransData->RejectReasonDesc; ?>"/>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label for="approvaldate" class="col-lg-4">Approval Date</label>
                <div class=" col-lg-8">
                    <input type="text" class="form-control" name="approvaldate" id="approvaldate" 
                            readonly="readonly"
                            value="<?php if(isset($customerTransData->ApprovalDate)) 
                            echo date_format(new DateTime($customerTransData->ApprovalDate),'d-M-Y | h:m:s'); ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="rejectdate" class="col-lg-4">Reject Date</label>
                <div class=" col-lg-8">
                    <input type="text" class="form-control" name="rejectdate" id="rejectdate" 
                            readonly="readonly"
                            value="<?php if(isset($customerTransData->RejectDate)) 
                            echo date_format(new DateTime($customerTransData->RejectDate),'d-M-Y | h:m:s'); ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="banksubmitnotes" class="col-lg-4">Bank Submit Notes</label>
                <div class=" col-lg-8">
                    <textarea class="form-control" name="banksubmitnotes" id="banksubmitnotes"
                        readonly="readonly"
                        placeholder="Notes"><?php if(isset($customerTransData->BankSubmitNotes)) echo $customerTransData->BankSubmitNotes; ?></textarea>
                </div>
            </div>
        </div>
    </div>
</fieldset>
<div style="margin-top:50px"></div>
<div class="act-float">
    <?php echo anchor('appcustomer/inquiry', 'Cancel', 'id="btnCancel" class="btn btn-default btn-cancel"');?>
    <button type="button" id="print" name="print" class="btn btn-info btn-print">
        <i class="glyphicon glyphicon-print"></i>&nbsp;Print Document
    </button>
</div>
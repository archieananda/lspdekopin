<form action='<?php echo $form_action; ?>' method='POST'>
    <?php $this->load->View($process_info_view); ?>
    <hr>
    <fieldset>
        <legend>Submit Bank Information</legend>
        <div class="form-horizontal">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="banksubmitnotes" class="col-lg-4">Submit Notes</label>
                    <div class=" col-lg-8">
                        <textarea class="form-control" name="banksubmitnotes" id="banksubmitnotes"
                            placeholder="Notes"><?php if(isset($customerTransData->BankSubmitNotes)) echo $customerTransData->BankSubmitNotes; ?></textarea>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <div style="margin-top:50px"></div>
    <div class="act-float">
        <?php echo anchor('appcustomer/inquiry', 'Cancel', 'id="btnCancel" class="btn btn-default btn-cancel"');?>
        <button type="submit" class="btn btn-warning">
            <i class="glyphicon glyphicon-share-alt"></i>&nbsp;&nbsp;Submit to Bank
        </button>
    </div>
</form>
<?php echo form_open_multipart($form_action);?>
  <fieldset>
    <legend>Pages Info</legend>
    
    <div class="form-group">
      <input type="hidden" name="pagesid" 
        value='<?php if(isset($viewData->id_pages)) echo $viewData->id_pages; ?>' />
      <input type="hidden" name="order" 
        value='<?php if(isset($viewData->urutan)) echo $viewData->urutan; else echo "0";?>' />
      <input type="hidden" name="ispublish" 
        value='<?php if(isset($viewData->is_publish)) echo $viewData->is_publish; else echo "0";?>' />
      <input type="hidden" name="parentid" 
        value='<?php if(isset($viewData->parentid)) echo $viewData->parentid; else echo "-";?>' />

      <label for="pagesName">Pages Name</label>
      <input type="text" class="form-control" required="true" id="pagesName" name="pagesName"
        value='<?php if(isset($viewData->name_pages)) echo $viewData->name_pages; ?>'
        placeholder="Pages Name">
    </div>

    <div class="form-group">
      <label for="seoTitle">SEO Title</label>
      <input type="text" class="form-control" required="true" id="seoTitle" name="seoTitle"
        value='<?php if(isset($viewData->judul_seo)) echo $viewData->judul_seo; ?>'
        placeholder="SEO Title">
    </div>

    <div class="form-group">
      <label for="tags">Tags</label>
      <input data-role="tagsinput" type="text" class="form-control" 
        required="true" id="tags" name="tags"
        value='<?php if(isset($viewData->tags)) echo $viewData->tags; ?>'
        placeholder="Tags">
    </div>

    <div class="form-group">
      <label for="pagesContent">Pages Content</label>
      <text-area class="form-control" required="true" id="pagesContent" name="pagesContent"
        placeholder="Pages Content">
        <?php if(isset($viewData->pages)) echo $viewData->pages; ?>
      </text-area>
    </div>

    <div class="form-group">
      <label for="pagesImage">Pages Image</label>
      <input type="file" class="form-control"  id="pagesImage" name="pagesImage"
        placeholder="Upload Pages Image" />
      <div class="img-preview-box">
        <span class='prev-cap'>Last Saved Preview :</span>
        <?php if(isset($viewData->image_name)){ ?>
        <img src="<?php echo base_url() . "/" . $viewData->image_name?>"
        width="200"/>        
        <?php }else{
          echo "<span class='no-img'>No image available</span>";
        } ?>
      </div>
    </div>

    <div class="form-group">
      <label class="form-check-label">
      <input class="form-check-input" type="checkbox" name="showImage"
            '<?php if(isset($viewData->showImage)) echo ((bool)$viewData->showImage) ? " checked='checked'" : ""; ?>' />
            Show Image
      </label>
    </div>

    <div class="form-group">
      <label class="form-check-label">
      <input class="form-check-input" type="checkbox" name="ismenu"
            '<?php if(isset($viewData->isMenu)) echo ((bool)$viewData->isMenu) ? " checked='checked'" : ""; ?>' />
            Is Menu
      </label>
    </div>

    <div class="form-group">
      <label class="form-check-label">
      <input class="form-check-input" type="checkbox" name="subpages"
            '<?php if(isset($viewData->subpages)) echo ((bool)$viewData->subpages) ? " checked='checked'" : ""; ?>' />
            Sub Pages
      </label>
    </div>


    <div class="form-group">
      <label class="form-check-label">
      <input class="form-check-input" type="checkbox" name="sidepages"
            '<?php if(isset($viewData->sidepages)) echo ((bool)$viewData->sidepages) ? " checked='checked'" : ""; ?>' />
            Side Pages
      </label>
    </div>

    <div class="form-group">
      <label for="pagesType">Pages Type</label>
      <select class="form-control" id="pagesType" required="true" name="pagesType">
        <option value="">Select Pages Type</option>
         <?php
         foreach($pagesTypeList as $rowData):
            if($viewData->tipe_pages == $rowData["value"])
                echo "<option selected='selected' value='".$rowData["value"]."'>"
                .$rowData["description"]."</option>";
            else
                echo "<option value='".$rowData["value"]."'>"
                .$rowData["description"]."</option>";
         endforeach;
         ?>
      </select>
    </div>

    <?php echo anchor('pages/setting', 'Cancel', 'class="btn btn-default btn-cancel"');?>
    <input type="submit" value="Save" class="btn btn-primary" />
 </fieldset>
<?php echo form_close();?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method

$route['default_controller'] = 'login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
*/
$route['default_controller'] = "login";
$route['scaffolding_trigger'] = "";

$route['pages/setting'] = 'Pages';
$route['pages/setting/add'] = 'Pages/add';
$route['pages/setting/edit/(:num)'] = 'Pages/edit/$1';
$route['pages/setting/add/insert'] = 'Pages/update_pagessetting';
$route['pages/setting/edit/update'] = 'Pages/update_pagessetting';
$route['pages/setting/delete'] = 'Pages/delete_pagessetting';

$route['posts/setting'] = 'Posts';
$route['posts/setting/add'] = 'Posts/add';
$route['posts/setting/edit/(:num)'] = 'Posts/edit/$1';
$route['posts/setting/add/insert'] = 'Posts/update_postssetting';
$route['posts/setting/edit/update'] = 'Posts/update_postssetting';
$route['posts/setting/delete'] = 'Posts/delete_postssetting';

$route['product/setting'] = 'product/product_setting';
$route['product/setting/add'] = 'product/product_setting_add';
$route['product/setting/edit/(:num)'] = 'product/product_setting_edit/$1';
$route['product/setting/edit/update'] = 'product/update_productsetting';
$route['product/setting/add/insert'] = 'product/update_productsetting';
$route['users/setting'] = 'users';
$route['users/setting/add'] = 'users/add';
$route['users/setting/edit/(:num)'] = 'users/user_edit/$1';
$route['users/setting/edit/update'] = 'users/update_user';
$route['users/setting/add/insert'] = 'users/update_user';

$route['product/setting/rate/(:num)'] = 'product/product_rate_setting/$1';
$route['product/setting/rate/add/(:num)'] = 'product/product_rate_setting_add/$1';
$route['product/setting/rate/edit/(:num)'] = 'product/product_rate_setting_edit/$1';
$route['product/setting/rate/edit/update'] = 'product/update_productratesetting';
$route['product/setting/rate/add/insert'] = 'product/update_productratesetting';
$route['product/setting/rate/delete'] = 'product/delete_productratesetting';

$route['plafond/setting'] = 'plafond/plafond_setting';
$route['plafond/setting/add'] = 'plafond/plafond_setting_add';
$route['plafond/setting/edit/(:num)'] = 'plafond/plafond_setting_edit/$1';
$route['plafond/setting/edit/update'] = 'plafond/update_plafondsetting';
$route['plafond/setting/add/insert'] = 'plafond/update_plafondsetting';
$route['plafond/setting/delete'] = 'plafond/delete_plafondsetting';


$route['sales/setting'] = 'sales/sales_setting';
$route['sales/setting/add'] = 'sales/sales_setting_add';
$route['sales/setting/edit/(:num)'] = 'sales/sales_setting_edit/$1';
$route['sales/setting/edit/update'] = 'sales/update_salessetting';
$route['sales/setting/add/insert'] = 'sales/update_salessetting';
$route['sales/setting/delete'] = 'sales/delete_salessetting';

$route['master/setting'] = 'MasterSetting';
$route['master/setting/load'] = 'MasterSetting/getFullMasterSettingList';
$route['master/setting/add/(:any)'] = 'MasterSetting/master_setting_add/$1';
$route['master/setting/edit/(:num)'] = 'MasterSetting/master_setting_edit/$1';
$route['master/setting/save/insert'] = 'MasterSetting/update_mastersetting';
$route['master/setting/save/update'] = 'MasterSetting/update_mastersetting';

$route['appcustomer/inquiry'] = 'appCustomer/index';
$route['appcustomer/inquiry/search'] = 'appCustomer/getAppCustomerList';
$route['appcustomer/followup/(:num)'] = 'appCustomer/folowUpCustEdit/$1';
$route['appcustomer/followup/save'] = 'appCustomer/folowUpSave';
$route['appcustomer/followup/savejunk'] = 'appCustomer/folowUpSaveJunk';
$route['appcustomer/followup/savedataentry'] = 'appCustomer/folowUpSaveToDataEntry';
$route['appcustomer/pickup/(:num)'] = 'appCustomer/pickUpCustEdit/$1';
$route['appcustomer/pickup/save'] = 'appCustomer/pickUpSave';
$route['appcustomer/entrydone/save'] = 'appCustomer/entryDoneSave';
$route['appcustomer/followup/saveback'] = 'appCustomer/backToFollowUpSave';
$route['appcustomer/banksubmit/(:num)'] = 'appCustomer/bankSubmitCustEdit/$1';
$route['appcustomer/banksubmit/save'] = 'appCustomer/bankSubmitSave';
$route['appcustomer/bankapproval/(:num)'] = 'appCustomer/approvalResultBankCustEdit/$1';
$route['appcustomer/bankapproval/save'] = 'appCustomer/bankResultSave';
$route['appcustomer/assign/save'] = 'appCustomer/saveAssign';
$route['appcustomer/detailview/(:num)'] = 'appCustomer/viewDetailCust/$1';


$route['asesi/setting'] = 'Asesi';
$route['asesi/setting/add'] = 'asesi/add';
$route['asesi/setting/add/insert'] = 'asesi/addinsert';
$route['asesi/getlistAsesimember/(:num)'] = 'asesi/getlistAsesimember/$1';
$route['asesi/setting/export/(:num)'] = 'asesi/ExportAsesi/$1';
$route['asesi/setting/delete'] = 'asesi/delete_asesi/';
$route['asesi/setting/completed/(:num)'] = 'asesi/completed/$1';
$route['asesi/submit_form'] = 'asesi/submit_asesi/';
$route['asesi/setting/deletemember/(:num)'] = 'asesi/deletemember/$1';

$route['asesor/setting'] = 'asesor';
$route['asesor/setting/add'] = 'asesor/add';
$route['asesor/setting/add/insert'] = 'asesor/addinsert';
$route['asesor/setting/edit/(:num)'] = 'asesor/asesor_edit/$1';
$route['asesor/getlistAsesimember/(:num)'] = 'asesor/getlistAsesimember/$1';
$route['asesor/setting/export/(:num)'] = 'asesor/ExportAsesi/$1';
$route['asesor/setting/delete'] = 'asesor/delete_asesi/';
$route['asesor/submit_form'] = 'asesor/submit_asesi/';

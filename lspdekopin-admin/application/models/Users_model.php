<?php
/**
 * Login_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Users_model extends CI_Model {
	/**
	 * Constructor
	 */
	public function  __construct()
	{
		parent::__construct ();
	}
	
	// Inisialisasi nama tabel user
	var $table = 'users';
	

function daftarUsers ($read = false){

	if($read === false) {

		$query = $this->db->query('SELECT user_id,user_username,user_email,user_nama,
		b.Description Role,case user_status when 1 then \'Yes\' else \'No\' end user_status,user_telp,
		mspt.Description ProductTypeDesc
		FROM user a inner join m_setting b on a.user_level=b.value and parameter=\'Role\'
		LEFT JOIN m_setting mspt on a.ProductTypeID = mspt.Value and mspt.Parameter = \'ProductType\'
		ORDER BY user_id asc');
	
		return $query->result_array();
		}
		$query = $this->db->get_where('users', array('slug' => $read));
		return $query->row_array();
	}


	function getUserData ($id = 0) {

		$query = $this->db->query('select * from user where user_id='.$id);
		return $query->row();
		
	}

	function getRoleType(){
		$query = $this->db->query('SELECT value,description FROM m_setting where parameter=\'Role\' AND IsActive = 1');
		return $query->result_array();
	}

	function getUserDropDownByLevelID($levelID){
		$query = $this->db->query("SELECT user_id value,user_nama description 
			FROM user where user_level='$levelID'");
		return $query->result_array();
	}

	function getUserDropDownByParentID($parentID){
		$query = $this->db->query("SELECT user_id value,user_nama description 
			FROM user where user_parent='$parentID'");
		return $query->result_array();
	}
	
	function getUserDropDownByUserID($userID){
		$query = $this->db->query("SELECT user_id value,user_nama description 
			FROM user where user_id='$userID'");
		return $query->result_array();
	}

	function saveUpdateUser($newData){
		$this->db
			->where('user_id',$newData['user_id'])
			->update('user', $newData);

		$error = $this->db->error();
		if (isset($error['message']) && $error['message'] != NULL) {
			show_error($error['message']);
		}

		return !($this->db->affected_rows() == NULL || $this->db->affected_rows() == 0);
	}

	function saveInsertUser($newData){
		$this->db
			->insert('user', $newData);

		$error = $this->db->error();
		if (isset($error['message']) && $error['message'] != NULL) {
			show_error($error['message']);
		}

		// return $error;

		return !($this->db->affected_rows() == NULL || $this->db->affected_rows() == 0);
	}

}

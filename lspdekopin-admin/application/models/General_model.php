<?php
/**
 * General Model Class
 *
 * @author	NEV(nahar.echa@msinfotama.com)
 */
class General_model extends CI_Model {
	/**
	 * Constructor
	 */
	public function  __construct()
	{
		parent::__construct ();
	}	

	function deleteTableBasedOnTableAndID($id, $tablename, $singleWhereId){
		$this->db
            ->where($singleWhereId, $id)
            ->delete($tablename);

		$error = $this->db->error();
		if (isset($error['message']) && $error['message'] != NULL) {
			show_error($error['message']);
		}

		return !($this->db->affected_rows() == NULL || $this->db->affected_rows() == 0);
	}

	function getSettingByParameter ($parameter){
		$query = $this->db->query("SELECT value,description FROM m_setting where parameter='$parameter' AND IsActive=1 Order By Value");
		return $query->result_array();
	}

	function getFullSettingBySettingID ($settingID){
		$query = $this->db->query("SELECT * FROM m_setting where SettingID=$settingID");
		$result  =  $query->row();

		$this->db->close();
		return $result;
	}
	
	function getFullSettingByParameter ($parameter){
		$query = $this->db->query("SELECT SettingID,Parameter,Value,Description,case when IsActive = 1 then 'Active' else 'Inactive' End FROM m_setting where parameter='$parameter' AND IsActive=1 Order By Value");
		return $query->result_array();
	}

	function getAllParameter (){
		$query = $this->db->query("SELECT DISTINCT Parameter FROM m_setting WHERE Parameter NOT IN ('ApplicationStatus','ProductType','Role','ApprovalResult') ORDER By Value");
		return $query->result_array();
	}

	function getSettingByParameterWithCond ($parameter, $condValue){
		$query = $this->db->query("SELECT value,description FROM m_setting where parameter='$parameter' AND Value='$condValue' AND IsActive=1 Order By Value");
		return $query->result_array();
	}

	function saveUpdateMasterSetting($newData){
		$this->db
			->where('SettingID',$newData['SettingID'])
			->update('m_setting', $newData);

        $error = $this->db->error();
        if (isset($error['message']) && $error['message'] != NULL) {
            show_error($error['message']);
        }

		return !($this->db->affected_rows() == NULL || $this->db->affected_rows() == 0);
	}

	function saveInsertMasterSetting($newData){
		$this->db
			->insert('m_setting', $newData);

		$error = $this->db->error();
		if (isset($error['message']) && $error['message'] != NULL) {
			show_error($error['message']);
		}
		
		return !($this->db->affected_rows() == NULL || $this->db->affected_rows() == 0);
	}

	function getUserByUserName ($username){
		$query = $this->db->query("SELECT * FROM user WHERE user_username='$username'");
		$result  =  $query->row();

		$this->db->close();
		return $result;
	}

	function getMonthDropDown(){
		$arrMonth = [];
		for ($x = 1; $x <= 12; $x++) {
			array_push($arrMonth,
				array(
					'value'=>$x,
					'description'=> date('F', strtotime("2018".str_pad($x,2,"0",STR_PAD_LEFT)."01"))
					)
				);
		}
		return $arrMonth;
	}

	// function getQueryWithCondition ($query, $fieldList, $SignList, $ValueList){
	// 	$query = $this->db->query($query);
	// 	$idx = 0;
	// 	foreach($fieldList as $field)
	// 	{
	// 		if($SignList[$idx] == 'like')
	// 		{
	// 			$query->like($field, $ValueList[$idx], 'both');
	// 		}
	// 		$idx++;
	// 	}
	// 	return $query->result_array();
	// }
}

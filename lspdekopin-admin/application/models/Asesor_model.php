<?php
/**
 * Asesor_model Class
 *
 * @author	Archie Ananda
 */
class Asesor_model extends CI_Model {
	/**
	 * Constructor
	 */
	public function  __construct()
	{
		parent::__construct ();
	}
	
	// Inisialisasi nama tabel user
	var $table = 'ms_assesor';
	

function daftar($read = false){

	if($read === false) {
		$query = $this->db->query('SELECT ID,  KODE_ASSESOR, DESKRIPSI, ISACTIVE FROM ms_assesor  where ISACTIVE = 1 order by DESKRIPSI asc  ');
		return $query->result_array();
		}
		$query = $this->db->get_where('ms_assesor', array('slug' => $read));
		return $query->row_array();
	}


	function getData ($id = 0) {

		$query = $this->db->query('select * from ms_assesor where ID='.$id);
		return $query->row();
		
	}



	function getlistasesor() {

		$query = $this->db->query('SELECT KODE_ASSESOR,DESKRIPSI,ISACTIVE FROM ms_assesor where ISACTIVE = 1 order by DESKRIPSI ASC');
		return $query->result_array();
		
	}

	function DaftarAsesibyid($id = 0) {

		$query = $this->db->query('SELECT NAMA, H_KODE_ASESI, D_KODE_ASESI, NIK, TEMPAT_LAHIR, DATE_FORMAT(TGL_LAHIR, "%d-%l-%Y") AS TGL_LAHIR, JNS_KELAMIN,TEMPAT_TINGGAL, KODE_KOTA, KODE_PROPINSI,
		TELP, EMAIL, KODE_PENDIDIKAN,KODE_PEKERJAAN, KODE_SKEMA, DATE_FORMAT(TGL_UJI, "%d-%l-%Y") AS TGL_UJI, KODE_TUK, KODE_ASSESOR,
		KODE_ANGGARAN,KODE_KEMENTRIAN, HASIL FROM d_daftar_asesi where H_KODE_ASESI = '.$id.' order by NAMA ASC');
		return $query->result_array();
		
	}

	function saveInsertSetting($newData){
		$this->db
			->insert('ms_assesor', $newData);
	
		$error = $this->db->error();
		if (isset($error['message']) && $error['message'] != NULL) {
			show_error($error['message']);
		}
		
		return !($this->db->affected_rows() == NULL || $this->db->affected_rows() == 0);
	}

	function saveUpdateSetting($newData){
		$this->db
			->where('ID',$newData['ID'])
			->update('ms_assesor', $newData);
	
		$error = $this->db->error();
		if (isset($error['message']) && $error['message'] != NULL) {
			print($error['message']);
			show_error($error['message']);
		}
	
		return !($this->db->affected_rows() == NULL || $this->db->affected_rows() == 0);
	}




	function saveUpdatemsTuk($newData2,$tuklama){
		$this->db
			->where('KODE_TUK',$tuklama)
			->update('ms_tuk', $newData);
	
		$error = $this->db->error();
		if (isset($error['message']) && $error['message'] != NULL) {
			print($error['message']);
			show_error($error['message']);
		}
	
		return !($this->db->affected_rows() == NULL || $this->db->affected_rows() == 0);
	}

	function saveInsertmstuk($newData2){
		$this->db
			->insert('ms_tuk', $newData2);
	
		$error = $this->db->error();
		if (isset($error['message']) && $error['message'] != NULL) {
			show_error($error['message']);
		}
		
		return !($this->db->affected_rows() == NULL || $this->db->affected_rows() == 0);
	}

	function deleteasesi($newData){
		$this->db
			->where('H_KODE_ASESI',$newData['H_KODE_ASESI'])
			->update('h_daftar_asesi', $newData);
	
		$error = $this->db->error();
		if (isset($error['message']) && $error['message'] != NULL) {
			print($error['message']);
			show_error($error['message']);
		}
	
		return !($this->db->affected_rows() == NULL || $this->db->affected_rows() == 0);
	}


	function saveUpdateAsesi($newData,$detailkode){
		$this->db
			->where('D_KODE_ASESI',$detailkode)
			->update('d_daftar_asesi', $newData);
	
		$error = $this->db->error();
		if (isset($error['message']) && $error['message'] != NULL) {
			print($error['message']);
			show_error($error['message']);
		}
	
		return !($this->db->affected_rows() == NULL || $this->db->affected_rows() == 0);
	}

}

<?php
/**
 * Login_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Login_model extends CI_Model {
	/**
	 * Constructor
	 */
	public function  __construct()
	{
		parent::__construct ();
	}
	
	// Inisialisasi nama tabel user
	var $table = 'user';
	
	/**
	 * Cek tabel user, apakah ada user dengan username dan password tertentu
	 */
	function check_user($username, $password)
	{
		// $query = $this->db->get_where($this->table, array('user_username' => $username, 'user_password' => $password, 'user_level' => '1'), 1, 0);
		$strquery = "SELECT * FROM $this->table WHERE user_username = '$username' AND user_password = '$password' AND user_level IN ('1','3','4','5')";
		
		$result = $this->db->query($strquery);

		if ($result->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	function get_userlevel_by_username($username)
	{
		$query = $this->db->get_where($this->table, array('user_username' => $username), 1, 0);
		$result  =  $query->row();

		return $result->user_level;
	}
}
// END Login_model Class

/* End of file login_model.php */ 
/* Location: ./system/application/model/login_model.php */
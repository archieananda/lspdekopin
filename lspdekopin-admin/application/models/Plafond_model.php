<?php
/**
 * Plafond Model Class
 *
 * @author	NEV(nahar.echa@msinfotama.com)
 */
class Plafond_model extends CI_Model {
	/**
	 * Constructor
	 */
	public function  __construct()
	{
		parent::__construct ();
    }	
    
    var $table = 'm_plafondscheme';

	function plafondSettingList (){

        $query = $this->db->query(
        "SELECT PlafondID, PlafondMIN, PlafondMAX, IsActive FROM ".$this->table
		);
		$this->db->close();
        return $query->result_array();
    }
    
    function getPlafondSetting ($id = 0){
		
		$query = $this->db->query(
            "SELECT PlafondID, PlafondMIN, PlafondMAX, IsActive FROM ".$this->table
            ." WHERE PlafondID = $id"
        );
		$this->db->close();
		return $query->row();
    }

    function saveUpdatePlafondSetting($newData){
		$this->db
			->where('PlafondID',$newData['PlafondID'])
			->update('m_plafondscheme', $newData);

        $error = $this->db->error();
        if (isset($error['message']) && $error['message'] != NULL) {
            show_error($error['message']);
        }

		return !($this->db->affected_rows() == NULL || $this->db->affected_rows() == 0);
	}

	function saveInsertPlafondSetting($newData){
		$this->db
			->insert('m_plafondscheme', $newData);

		$error = $this->db->error();
		if (isset($error['message']) && $error['message'] != NULL) {
			show_error($error['message']);
		}
		
		return !($this->db->affected_rows() == NULL || $this->db->affected_rows() == 0);
	}
    

}
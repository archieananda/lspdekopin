<?php
/**
 * Product Model Class
 *
 * @author	NEV(nahar.echa@msinfotama.com)
 */
class Product_model extends CI_Model {
	/**
	 * Constructor
	 */
	public function  __construct()
	{
		parent::__construct ();
	}	

	function productSettingList ($whereCond = null){

		$table = 'm_product';

		if($whereCond == null) {
			// $query = $this->db->query("Call spM_product_Definition (1, '')");
			$query = $this->db->query("SELECT ProductID, Description ProductTypeName, ProductName, p.IsActive 
			FROM m_product p INNER JOIN m_setting ms on p.ProductType = ms.Value AND ms.Parameter = 'ProductType'");
			return $query->result_array();
		}

		$query = $this->db->get_where($table, $whereCond);
		$this->db->close();
		return $query->result_array();
	}

	function getProductSetting ($id = 0){
		
		$query = $this->db->query( "Call spM_product_Definition (2, $id)" );
		$result  =  $query->row();

		$this->db->close();
		return $result;
	}
		
	function saveUpdateProductSetting($newData){
		$this->db
			->where('ProductID',$newData['ProductID'])
			->update('m_product', $newData);

		$error = $this->db->error();
		if (isset($error['message']) && $error['message'] != NULL) {
			print($error['message']);
			show_error($error['message']);
		}

		return !($this->db->affected_rows() == NULL || $this->db->affected_rows() == 0);
	}

	function saveInsertProductSetting($newData){
		$this->db
			->insert('m_product', $newData);

		$error = $this->db->error();
		if (isset($error['message']) && $error['message'] != NULL) {
			show_error($error['message']);
		}
		
		return !($this->db->affected_rows() == NULL || $this->db->affected_rows() == 0);
	}

	function productSettingRate ($read = false){

		$table = 'm_productrate';

		if($read === false) {
			$query = $this->db->query('SELECT ProductID,PlafondId FROM '.$table.' ORDER BY ProductID,PlafondId asc');
			return $query->result_array();
		}

		$query = $this->db->get_where($table, array('slug' => $read));
		$this->db->close();
		return $query->row_array();
	}

	function getProductType (){
		$query = $this->db->query('SELECT value,description FROM m_setting where parameter=\'ProductType\'');
		$this->db->close();
		return $query->result_array();
	}

	function getProductTenor (){
		$query = $this->db->query('SELECT value,description FROM m_setting where parameter=\'Tennor\'');
		$this->db->close();
		return $query->result_array();
	}

	function getPlafondScheme (){
		$query = $this->db->query('SELECT PlafondID as value, concat(
			PlafondMin
			 , \' to \' , PlafondMax) as  description FROM m_plafondscheme 
		where IsActive=1');
		$this->db->close();
		return $query->result_array();
	}

	function productRateSettingList ($prodID){
		
		$query = $this->db->query("Call spM_productrate_Definition (1, '', $prodID);" );
		$result  =  $query->result_array();
		$this->db->close();
		return $result;
	}

	function getProductRateSetting ($rateid = 0){

		$query = $this->db->query(
			"SELECT a.* FROM m_productrate a
			WHERE ProductRateId = $rateid
			ORDER BY ProductRateId");
		$this->db->close();
		return $query->row();
	}

	function saveUpdateProductRateSetting($newData){

		$this->db
			->where('ProductRateID',$newData['ProductRateID'])
			->update('m_productrate', $newData);

		$error = $this->db->error();
		if (isset($error['message']) && $error['message'] != NULL) {
			show_error($error['message']);
		}
		
		return !($this->db->affected_rows() == NULL || $this->db->affected_rows() == 0);
	}
	
	function saveInsertProductRateSetting($newData){
		$this->db
			->insert('m_productrate', $newData);

		$error = $this->db->error();
		if (isset($error['message']) && $error['message'] != NULL) {
			show_error($error['message']);
		}

		return !($this->db->affected_rows() == NULL || $this->db->affected_rows() == 0);
	}
}

<?php
/**
 * AppCustomer Model Class
 *
 * @author	NEV(nahar.echa@msinfotama.com)
 */
class appCustomer_model extends CI_Model {
	/**
	 * Constructor
	 */
	public function  __construct()
	{
		parent::__construct ();
    }

	function appCustomerList ($fieldCond){
		$baseQuery = "SELECT a.TransID,b.user_nama SalesName, tdd.FullName CustomerName, 
			ProductName, d.Description as ProductType, e.Description as StatusPengajuan,
			TglPengajuan
			FROM trans_h_pengajuan a 
			LEFT JOIN user b ON a.SalesID = b.user_id
			LEFT JOIN m_product c ON a.ProductID = c.ProductID
			LEFT JOIN m_setting d on c.ProductType = d.Value AND d.Parameter = 'ProductType'
			LEFT JOIN m_setting e on a.StatusPengajuan = e.Value and e.Parameter = 'ApplicationStatus'
			LEFT JOIN trans_d_datadiri tdd on a.TransID = tdd.TransID ";

		// if(!isset($fieldCond["fullname"]) || $fieldCond["fullname"] == null){
		// 	$fieldCond["fullname"] = '';
		// }

		// if(!isset($fieldCond["ktpno"]) || $fieldCond["ktpno"] == null){
		// 	$fieldCond["ktpno"] = '';
		// }

		// if(!isset($fieldCond["productname"]) || $fieldCond["productname"] == null){
		// 	$fieldCond["productname"] = '';
		// }

		if($fieldCond == NULL){			
			$query = $this->db->query($baseQuery);
			return $query->result_array();
		}else{
			$condappStatus = "";
			if(isset($fieldCond["appstatus"]) && is_numeric($fieldCond["appstatus"])){
				$condappStatus = "AND e.Value = ".$fieldCond["appstatus"]."";
			}
			
			$condProdType = "";
			if(isset($fieldCond["producttypeid"]) && is_numeric($fieldCond["producttypeid"])){
				$condProdType = "AND d.Value = ".$fieldCond["producttypeid"]."";
			}

			$condsalesid = "";
			if(isset($fieldCond["salesid"]) && is_numeric($fieldCond["salesid"])){
				$condsalesid = "AND b.user_id = ".$fieldCond["salesid"]."";
			}

			$condleaderid = "";
			if(isset($fieldCond["leaderid"]) && is_numeric($fieldCond["leaderid"])){
				$condleaderid = "AND (b.user_parent = ".$fieldCond["leaderid"]." OR a.SalesID = 0)";
			}

			$condDateFrom = "";
			if(isset($fieldCond["trxfrom"]) && strlen($fieldCond["trxfrom"]) > 0){	
				$date_from = DateTime::createFromFormat('d/m/Y', $fieldCond["trxfrom"]);
				$date_from = $date_from->format('Y-m-d');
				$condDateFrom = "AND TglPengajuan >= '$date_from'";
			}

			$condDateTo = "";
			if(isset($fieldCond["trxto"]) && strlen($fieldCond["trxto"]) > 0){	
				$date_to = DateTime::createFromFormat('d/m/Y', $fieldCond["trxto"]);
				$date_to = $date_to->format('Y-m-d');
				$condDateTo = "AND TglPengajuan <= '$date_to'";
			}

			$query = $this->db->query(
				"
				$baseQuery
				WHERE (CASE WHEN tdd.FullName IS NULL THEN '' ELSE tdd.FullName END) LIKE '%".$fieldCond["fullname"]."%'
				AND (CASE WHEN KTPNumber IS NULL THEN '' ELSE tdd.FullName END) LIKE '%".$fieldCond["ktpno"]."%'  
				$condappStatus
				$condProdType
				$condDateFrom
				$condDateTo
				$condsalesid
				$condleaderid
				AND ProductName LIKE '%".$fieldCond["productname"]."%'"
				);
			$this->db->close();			
			return $query->result_array();
		}
	}
	
	function getCustomerTransactionByID ($id = 0){
		
		$query = $this->db->query("
		select 
			/*Trans_H_Pengajuan*/
			thp.TransID, thp.TglPengajuan, thp.SalesID, thp.AccountID, thp.ProductID, 
			thp.Notes, thp.BillingAddress, thp.ShippingAddress, thp.SpouseName, thp.SpouseKTP, 
			thp.SpousePhone, thp.SurveyAddress, thp.SurveyNotes, thp.DateOfSUrvey, thp.TimeOfSUrvey, thp.HasKTP, 
			thp.KTPNumber, thp.HasNPWP, thp.NPWPNumber, thp.HasKK, thp.KKNumber, thp.HasCreditCard, 
			thp.HasBankAccount, thp.HasRekListrik, thp.HasSTNK, thp.HasBPKB, thp.HasSIUP, thp.HasCertificate, 
			thp.HasAjb, thp.StatusPengajuan, thp.FolowupStatus, thp.MailingAddressType,
			thp.PickupStatus, thp.PickupBy, thp.PickupNotes,
			thp.BankSubmitDate, thp.BankSubmitNotes, thp.RejectDate, thp.RejectReason, 
			thp.ApprovalDate, thp.ResultBankNotes, thp.ApprovalResult, thp.PickupStatusDate,
			thp.DataEntryDate, thp.EntryDoneDate,
			/*Trans_D_Prod*/
			tdp.DetailID, tdp.ChannelID, tdp.FlatRate, tdp.EffectiveRate, tdp.TennureMonth, tdp.Plafond, 
			/*Trans_D_Data_Diri*/
			tdd.Trans_D_DatadiriID, tdd.FullName, tdd.Address, tdd.HomePhone, tdd.MobileNo, tdd.MotherSureName, 
			tdd.EmailAddress, tdd.ResidenceStatus, tdd.LivingPeriod, tdd.JmlTanggungan, 
			tdd.EducationID, tdd.Occupation, tdd.CompanyName, tdd.CoreBusiness, tdd.LamaKerja, tdd.Position, 
			tdd.Division, tdd.CompanyAddress, tdd.CompanyPhone, tdd.ExtensionPhone, tdd.MonthlyIncomeAmount, tdd.BankPayrol,
			/*Trans_D_Existing_CC*/
			tdec.Trans_d_existing_ccID, tdec.CCTypeID, tdec.`Limit`, tdec.CCBankID,
			/*Trans_D_EmergencyContact*/
			tdem.Trans_D_EmergencyContactID, tdem.RelationshipID, tdem.FullName FullNameEC, tdem.HomeAddress HomeAddressEC, tdem.HomePhone HomePhoneEC,
			/*M_Product*/
			mp.ProductType, mp.ProductName, mp.HTMLContent, mp.ImagePath,
			/*M_Account*/
			mac.AccountName, mac.User_id, mac.RefferalID, mac.BankAccountNumber, mac.BankName, mac.RefferalRef,
			/*M_Setting_ApplicationStatus*/
			ms.Description StatusPengajuanDesc,
			/*M_Setting_ProductType*/
			ms2.Description ProductTypeDesc,
			/*M_Setting_BankChannel*/
			ms3.Description ChannelDesc,
			/*M_Setting_Education*/
			ms4.Description EducationDesc,
			/*M_Setting_ResidenceStatus*/
			ms5.Description ResidenceStatusDesc,
			/*M_Setting_RejectReason*/
			ms6.Description RejectReasonDesc,
			/*M_Setting_PickupStatus*/
			ms7.Description PickupStatusDesc,
			/*M_Setting_FollowupStatus*/
			ms8.Description FollowupStatusDesc,
			/*M_Setting_ApprovalResult*/
			ms9.Description ApprovalResultDesc,
			/*User*/
			user.user_id, user.user_nama,
			/*Month_Year_Custom*/
			month(tdec.ActiveDate) CCActiveMonth, year(tdec.ActiveDate) CCActiveYear,
			month(tdec.ExpiredDate) CCExpiredMonth, year(tdec.ExpiredDate) CCExpiredYear
			from trans_h_pengajuan thp 
			left join trans_d_product tdp on thp.TransID = tdp.TransID
			left join trans_d_datadiri tdd on thp.TransID = tdd.TransID
			left join trans_d_existing_cc tdec on tdd.TransID = tdec.TransID
			left join trans_d_emergencycontact tdem on thp.TransID = tdem.TransID
			left join m_product mp on thp.ProductID = mp.ProductID
			left join m_account mac on mac.AccountID = thp.AccountID
			left join user on user.user_id = thp.SalesID
			left join m_setting ms on thp.StatusPengajuan = ms.Value and ms.Parameter = 'ApplicationStatus'
			left join m_setting ms2 on mp.ProductType = ms2.Value and ms2.Parameter = 'ProductType'
			left join m_setting ms3 on tdp.ChannelID = ms3.Value and ms3.Parameter = 'Channel'
			left join m_setting ms4 on tdd.EducationID = ms4.Value and ms4.Parameter = 'Education'
			left join m_setting ms5 on tdd.ResidenceStatus = ms5.Value and ms5.Parameter = 'ResidenceStatus'
			left join m_setting ms6 on thp.RejectReason = ms6.Value and ms6.Parameter = 'RejectReason'
			left join m_setting ms7 on thp.PickupStatus = ms7.Value and ms7.Parameter = 'PickupStatus'
			left join m_setting ms8 on thp.FolowupStatus = ms8.Value and ms8.Parameter = 'FollowupStatus'
			left join m_setting ms9 on thp.ApprovalResult = ms9.Value and ms9.Parameter = 'ApprovalResult'
		where thp.TransID=$id
		");
		$result  =  $query->row();

		$this->db->close();
		return $result;
	}

	function checkTransTableDataExistsByTransID ($id, $tableName){
		$query = $this->db->query("Select 1 Result From $tableName Where TransID=$id");
		$result  =  $query->row();

		// $this->db->close();
		return $result;
	}

	function saveAssign($newData){
		$transIDs = $newData['TransIDs'];
		$newData = array_splice($newData, 1, 1);

		$this->db
			->where_in('TransID', $transIDs)
			->update('trans_h_pengajuan', $newData);

		$error = $this->db->error();
		if (isset($error['message']) && $error['message'] != NULL) {
			print($error['message']);
			show_error($error['message']);
		}

		// $this->db->close();
		return !($this->db->affected_rows() == NULL || $this->db->affected_rows() == 0);
	}

	function saveFollowupTransactionData($newData, $tableName){
		$this->db
			->where('TransID',$newData['TransID'])
			->update($tableName, $newData);

		$error = $this->db->error();
		if (isset($error['message']) && $error['message'] != NULL) {
			print($error['message']);
			show_error($error['message']);
		}

		// $this->db->close();
		return !($this->db->affected_rows() == NULL || $this->db->affected_rows() == 0);
	}

	function insertFollowupTransactionData($newData, $tableName){
		$this->db
			->insert($tableName, $newData);

		$error = $this->db->error();
		if (isset($error['message']) && $error['message'] != NULL) {
			show_error($error['message']);
		}
		
		return !($this->db->affected_rows() == NULL || $this->db->affected_rows() == 0);
	}

	function insertTransactionHistory($transid,$statuspengajuan,$notes,$userupdate,$updatedate){

		$newData = array(
			'TransID'=>$transid,
			'StatusPengajuan'=>$statuspengajuan,
			'Notes'=>$notes,
			'UserUpdate'=>$userupdate,
			'UpdateDate'=>$updatedate
		);

		$this->db
			->insert('trans_h_pengajuan_history', $newData);

		$error = $this->db->error();
		if (isset($error['message']) && $error['message'] != NULL) {
			show_error($error['message']);
		}
		
		return !($this->db->affected_rows() == NULL || $this->db->affected_rows() == 0);
	}

	// function saveFollowupTrans_D_Product($newData){
	// 	$this->db
	// 		->where('TransID',$newData['TransID'])
	// 		->update('trans_d_product', $newData);

	// 	$error = $this->db->error();
	// 	if (isset($error['message']) && $error['message'] != NULL) {
	// 		print($error['message']);
	// 		show_error($error['message']);
	// 	}

	// 	// $this->db->close();
	// 	return !($this->db->affected_rows() == NULL || $this->db->affected_rows() == 0);
	// }
}
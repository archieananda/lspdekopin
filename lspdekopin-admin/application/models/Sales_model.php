<?php
/**
 * Sales Model Class
 *
 * @author	NEV(nahar.echa@msinfotama.com)
 */
class Sales_model extends CI_Model {
    /**
	 * Constructor
	 */
	public function  __construct()
	{
		parent::__construct ();
    }

    var $table = 'm_sales';

	function salesSettingList (){

        $query = $this->db->query(
        "SELECT SalesID, SalesName, SalesContact, IsActive FROM ".$this->table
		);
		$this->db->close();
        return $query->result_array();
    }
    
    function getSalesSetting ($id = 0){
		
		$query = $this->db->query(
            "SELECT SalesID, SalesName, SalesContact, IsActive FROM ".$this->table
            ." WHERE SalesID = $id"
		);
		$this->db->close();
		return $query->row();
    }

    function saveUpdateSalesSetting($newData){
		$this->db
			->where('SalesID',$newData['SalesID'])
			->update('m_sales', $newData);

        $error = $this->db->error();
        if (isset($error['message']) && $error['message'] != NULL) {
            show_error($error['message']);
        }

		return !($this->db->affected_rows() == NULL || $this->db->affected_rows() == 0);
	}

	function saveInsertSalesSetting($newData){
		$this->db
			->insert('m_sales', $newData);

		$error = $this->db->error();
		if (isset($error['message']) && $error['message'] != NULL) {
			show_error($error['message']);
		}
		
		return !($this->db->affected_rows() == NULL || $this->db->affected_rows() == 0);
	}
}

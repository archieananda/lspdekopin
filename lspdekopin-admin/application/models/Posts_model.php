<?php
/**
 * Login_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Posts_model extends CI_Model {
	/**
	 * Constructor
	 */
	public function  __construct()
	{
		parent::__construct ();
	}
	
	// Inisialisasi nama tabel user
	var $table = 'Post';
	

function daftarPosts ($whereCond = false){

	if($whereCond === false) {
		$query = $this->db->query('select berita.id_berita, 
		(select name_pages from pages where id_pages = berita.id_pages ) nama_pages , 
		status_berita, berita.judul_berita, 
		( select user_nama from user where user_id = berita.author ) publisher, judul_seo, tags from berita berita order by tgl_publish desc');
		return $query->result_array();
	}

 	$query = $this->db->get_where('berita', array('slug' => $read));
 	return $query->row_array();
 }


 function terbaruPost ($read = false) {

 	$query = $this->db->query('select * from berita order by tgl_publish desc limit 1');
 	return $query->result_array();
 	
 }


 function postsbyid($idberita){

		
	$query = $this->db->query( "select * from berita where id_berita=$idberita" );

	$result  =  $query->row();

	$this->db->close();
	return $result;
}


function saveUpdatePostsSetting($newData){
	$this->db
		->where('id_berita',$newData['id_berita'])
		->update('berita', $newData);

	$error = $this->db->error();
	if (isset($error['message']) && $error['message'] != NULL) {
		print($error['message']);
		show_error($error['message']);
	}

	return !($this->db->affected_rows() == NULL || $this->db->affected_rows() == 0);
}

function saveInsertPostsSetting($newData){
	$this->db
		->insert('berita', $newData);

	$error = $this->db->error();
	if (isset($error['message']) && $error['message'] != NULL) {
		show_error($error['message']);
	}
	
	return !($this->db->affected_rows() == NULL || $this->db->affected_rows() == 0);
}

}

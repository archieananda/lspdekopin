<?php
/**
 * Login_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class Pages_model extends CI_Model {
	/**
	 * Constructor
	 */
	public function  __construct()
	{
		parent::__construct ();
	}
	
	// Inisialisasi nama tabel user
	var $table = 'pages';
	

function daftarPages ($whereCond = null){

	if($whereCond === null) {

		$query = $this->db->query(
			'SELECT a.name_pages, 
			(select name_pages from pages where id_pages = a.parentid ) namaparent ,tags,
			id_pages, judul_seo FROM pages a  ORDER BY urutan asc');
		return $query->result_array();
	}
	
	$query = $this->db->get_where('pages', array('slug' => $read));
	return $query->row_array();
 }


 function terbaruPages ($read = false) {

 	$query = $this->db->query('select * from pages order by last_update desc limit 1');
 	return $query->result_array();
 	
 }


 function saveUpdatePagesSetting($newData){
	$this->db
		->where('id_pages',$newData['id_pages'])
		->update('pages', $newData);

	$error = $this->db->error();
	if (isset($error['message']) && $error['message'] != NULL) {
		print($error['message']);
		show_error($error['message']);
	}

	return !($this->db->affected_rows() == NULL || $this->db->affected_rows() == 0);
}

function saveInsertPagesSetting($newData){
	$this->db
		->insert('pages', $newData);

	$error = $this->db->error();
	if (isset($error['message']) && $error['message'] != NULL) {
		show_error($error['message']);
	}
	
	return !($this->db->affected_rows() == NULL || $this->db->affected_rows() == 0);
}


 function bacapages($idpages){



 	 	$query = $this->db->query('select * from pages order by last_update desc limit 1');
 	return $query->result_array();
 }

 function pagesbyid($idpages){

		
		$query = $this->db->query( "select * from pages where id_pages=$idpages" );

		$result  =  $query->row();

		$this->db->close();
		return $result;
 }

 	function get_pages()
	{

	
		$this->db->order_by('id_pages');
		
		return $this->db->get($this->table);
	}




}

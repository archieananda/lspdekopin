<?php
/**
 * Login_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class users_model extends Model {
	/**
	 * Constructor
	 */
	function users_model()
	{
		parent::Model();
	}
	
	// Inisialisasi nama tabel user
	var $table = 'users';
	

function daftarUsers ($read = false){

	if($read === false) {

		$query = $this->db->query('SELECT user_id,user_username,user_email,user_nama,
		Description Role,case user_status when 1 then \'Yes\' else \'No\' end user_status,user_telp
		FROM user a inner join m_setting b on a.user_level=b.value and parameter=\'Role\' ORDER BY user_id asc');
		return $query->result_array();
		}
		$query = $this->db->get_where('users', array('slug' => $read));
		return $query->row_array();
	}


	function getUserData ($id = 0) {

		$query = $this->db->query('select * from user where user_id='.$id);
		return $query->row();
		
	}

	function getRoleType(){
		$query = $this->db->query('SELECT value,description FROM m_setting where parameter=\'Role\'');
		return $query->result_array();
	}

	function saveUpdateUser($newData){
		$this->db
			->where('user_id',$newData['user_id'])
			->update('user', $newData);

		if ($this->db->_error_message()) {
			return false;
		}else{
			return true;
		}
	}

	function saveInsertUser($newData){
		$this->db
			->insert('user', $newData);

		if ($this->db->_error_message()) {
			return false;
		}else{
			return true;
		}
	}

}

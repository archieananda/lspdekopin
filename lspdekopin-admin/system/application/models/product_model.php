<?php
/**
 * Login_model Class
 *
 * @author	NEV(nahar.echa@msinfotama.com)
 */
class product_model extends Model {
	/**
	 * Constructor
	 */
	function productsetting_model()
	{
		parent::Model();
	}	

	function productSettingList ($read = false){

		$table = 'm_product';

		if($read === false) {
			$query = $this->db->query(
			'SELECT mp.ProductId,ms.Description as ProductType,mp.ProductName,
			case mp.IsActive when 1 then \'Yes\' else \'No\' end IsActive,mp.HTMLContent 
			FROM '.$table.' as mp
			INNER JOIN m_setting as ms on mp.ProductType=ms.Value and Parameter=\'PRODUCT_TYPE\' 
			ORDER BY productid asc');
			return $query->result_array();
		}

		$query = $this->db->get_where($table, array('slug' => $read));
		return $query->row_array();
	}

	function getProductSetting ($id = 0){
		$table = 'm_product';

		$query = $this->db->query('SELECT * FROM '.$table.' as mp
		WHERE productid='.$id);
		return $query->row();
	}

		
	function saveUpdateProductSetting($newData){
		$this->db
			->where('ProductID',$newData['ProductID'])
			->update('m_product', $newData);

		if ($this->db->_error_message()) {
			return false;
		}else{
			return true;
		}
	}

	function saveInsertProductSetting($newData){
		$this->db
			->insert('m_product', $newData);

		if ($this->db->_error_message()) {
			return false;
		}else{
			return true;
		}
	}

	function productSettingRate ($read = false){

		$table = 'm_productrate';

		if($read === false) {
			$query = $this->db->query('SELECT ProductID,PlafondId FROM '.$table.' ORDER BY ProductID,PlafondId asc');
			return $query->result_array();
		}

		$query = $this->db->get_where($table, array('slug' => $read));
		return $query->row_array();
	}

	function getProductType (){
		$query = $this->db->query('SELECT value,description FROM m_setting where parameter=\'PRODUCT_TYPE\'');
		return $query->result_array();
	}
}

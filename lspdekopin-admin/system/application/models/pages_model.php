<?php
/**
 * Login_model Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class pages_model extends Model {
	/**
	 * Constructor
	 */
	function pages_model()
	{
		parent::Model();
	}
	
	// Inisialisasi nama tabel user
	var $table = 'pages';
	

function daftarPages ($read = false){




	if($read === false) {

		 $query = $this->db->query('SELECT a.name_pages, (select name_pages from pages where id_pages = a.parentid ) namaparent , id_pages, judul_seo FROM pages a  ORDER BY urutan asc');
 return $query->result_array();
 }
 $query = $this->db->get_where('pages', array('slug' => $read));
 return $query->row_array();
 }


 function terbaruPages ($read = false) {

 	$query = $this->db->query('select * from pages order by last_update desc limit 1');
 	return $query->result_array();
 	
 }


 function bacapages($idpages){



 	 	$query = $this->db->query('select * from pages order by last_update desc limit 1');
 	return $query->result_array();
 }

 function pagesbyid($idpages){

 		$this->db->select('id_pages, judul_seo, name_pages, tags, tipe_pages, last_update, pages, urutan, parentid, image_name');
		$this->db->where('id_pages', $idpages);
		return $this->db->get($this->table);
 }

 	function get_pages()
	{

	
		$this->db->order_by('id_pages');
		
		return $this->db->get($this->table);
	}




}

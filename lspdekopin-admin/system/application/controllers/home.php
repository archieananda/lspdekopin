<?php
/**
 * Login Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class home extends Controller {
	/**
	 * Constructor
	 */
	function home()
	{
		parent::Controller();
		$this->load->model('home_model', '', TRUE);
		$this->load->model('pages_model', '', TRUE);
	}

	var $limit = 10;
	var $title = 'Home';
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman absen,
	 * jika tidak akan meload halaman login
	 */
	function index()
	{
		if ($this->session->userdata('login') == TRUE)
		{
			$this->tampil_home();
					}
		else
		{
			redirect('login');
		}
	}

		function image_editor() {
		
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= 'Image Editor';
		$data['main_view'] 		= 'image_editor.php';
		$data['form_action']	= site_url('post/add_process');
		$data['link'] 			= array('link_back' => anchor('absen/','kembali', array('class' => 'back')));
		$this->load->view('template', $data);
	
	}

	function tampil_home(){

		$data['title'] = $this->title;
		$data['h2_title'] = 'HOME CMS Punyaweb';
		$data['main_view'] = 'home/home_view';
		$data['newPages'] = $this->pages_model->terbaruPages();

		$this->load->view('template', $data);

	}
	
	}
	
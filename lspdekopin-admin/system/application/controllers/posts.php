<?php
/**
 * Login Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class posts extends Controller {
	/**
	 * Constructor
	 */
	function posts()
	{
		parent::Controller();
		$this->load->model('posts_model', '', TRUE);
	}

	var $limit = 10;
	var $title = 'Posts';
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman absen,
	 * jika tidak akan meload halaman login
	 */
	function index()
	{
		if ($this->session->userdata('login') == TRUE)
		{
			$this->tampil_posts();
					}
		else
		{
			redirect('login');
		}
	}

		function add() {
		
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= 'Posts > Add post';
		$data['main_view'] 		= 'posts/posts_form';
		$data['form_action']	= site_url('post/add_process');
		$data['link'] 			= array('link_back' => anchor('absen/','kembali', array('class' => 'back')));
		$this->load->view('template', $data);
	
	}

	function tampil_posts(){

		$data['title'] = $this->title;
		$data['h2_title'] = 'Pages CMS Punyaweb';
		$data['main_view'] = 'posts/posts_view';
		$data['isi'] = $this->posts_model->daftarPosts();
		

		$this->load->view('template', $data);

	}
	
	}
	
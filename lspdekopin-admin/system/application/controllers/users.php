<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Login Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class users extends Controller {
	/**
	 * Constructor
	 */
	function users()
	{
		parent::Controller();x
		$this->load->model('users_model', '', TRUE);
		//declare helper web_helper
        $this->load->helper('web_helper');
	}

	var $limit = 10;
	var $title = 'Users';
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman absen,
	 * jika tidak akan meload halaman login
	 */
	function index()
	{
		if ($this->session->userdata('login') == TRUE)
		{
			$this->tampil_users();
					}
		else
		{
			redirect('login');
		}
	}

	function tampil_users(){

		$data['title'] = $this->title;
		$data['h2_title'] = '<i class="glyphicon glyphicon-cog"></i> Users Management';
		$data['main_view'] = 'users/users_view';
		
		$data['extraHelperScript'] = getDataTableScriptSource();
        $data['extraHelperCss'] = getDataTableCssSource();
		$data['extraScript'] = array('users/users-view.js');
		
		$data['isi'] = json_encode($this->users_model->daftarUsers());
		$this->load->view('template', $data);
	}

	function add() {
		
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= '<i class="glyphicon glyphicon-cog"></i> Users > Add';
		$data['main_view'] 		= 'users/users_form';
		$data['form_action']	= site_url('users/setting/add/insert');
		$data['extraScript'] = array('users/users-form.js');
		$data['roleTypeList'] = $this->users_model->getRoleType();
		$this->load->view('template', $data);
	
	}

	function user_edit($id = 0) {
		
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= '<i class="glyphicon glyphicon-cog"></i> Users > Edit';
		$data['main_view'] 		= 'users/users_form';
		$data['form_action']	= site_url('users/setting/edit/update');
		$data['userData'] = $this->users_model->getUserData($id);
		$data['roleTypeList'] = $this->users_model->getRoleType();
		$data['extraScript'] = array('users/users-form.js');
		$this->load->view('template', $data);

	}

	function update_user() {
		$username = $this->input->post('username');
		$name = $this->input->post('name');
		$password = $this->input->post('password');
		$email = $this->input->post('email');
		$userstatus = $this->input->post('userstatus');
		$userlevel = $this->input->post('userlevel');
		$phone = $this->input->post('phone');

		if($this->input->post('userid') !== NULL)
			$userid = $this->input->post('userid');		

		$newData = array(
			'user_nama'=>$name,	
			'user_username'=>$username,
			'user_email'=>$email,  
			'user_level'=>$userlevel,
			'user_status'=>($userstatus) ? "1" : "0",
			'user_telp'=>$phone
		);

		if(strlen($password) > 0){
			$newData = array_merge($newData, array(
				'user_password'=>$password
			));
		}
		
		if(strlen($userid) > 0){		
			$newData = array_merge($newData, array(
				'user_id'=>$userid
			));
			return $this->users_model->saveUpdateUser($newData);
		}
		else{
			return $this->users_model->saveInsertUser($newData);
		}
	}
}
	
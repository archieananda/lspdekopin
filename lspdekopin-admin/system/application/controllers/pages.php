<?php
/**
 * Login Class
 *
 * @author	Awan Pribadi Basuki <awan_pribadi@yahoo.com>
 */
class pages extends Controller {
	/**
	 * Constructor
	 */
	function pages()
	{
		parent::Controller();
		$this->load->model('pages_model', '', TRUE);
	}

	var $limit = 10;
	var $title = 'Pages';
	
	/**
	 * Memeriksa user state, jika dalam keadaan login akan menampilkan halaman absen,
	 * jika tidak akan meload halaman login
	 */
	function index()
	{
		if ($this->session->userdata('login') == TRUE)
		{
			$this->tampil_pages();
					}
		else
		{
			redirect('login');
		}
	}

	function add() {
		
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= 'Pages > Add pages';
		$data['main_view'] 		= 'pages/pages_form';
		$data['form_action']	= site_url('pages/add_process');
		$data['link'] 			= array('link_back' => anchor('pages/','kembali', array('class' => 'back')));
		
// Data pages untuk dropdown
		$pagesbaca = $this->pages_model->get_pages()->result();
$data['option_pages']['-'] = 'Pilih Parent';
		foreach($pagesbaca as $row)
		{
			$data['option_pages'][$row->id_pages] = $row->name_pages;
		}

		$this->load->view('template', $data);
	
	}

		function update($id_pages) {
		
		$data['title'] 			= $this->title;
		$data['h2_title'] 		= 'Pages > Edit pages';
		$data['main_view'] 		= 'pages/pages_form';
		$data['form_action']	= site_url('pages/update_proses');
		$data['link'] 			= array('link_back' => anchor('pages/','kembali', array('class' => 'back')));
		

		// cari data dari database
		$pages = $this->pages_model->pagesbyid($id_pages)->row();
		
		// buat session untuk menyimpan data primary key (id_absen)
		$this->session->set_userdata('id_pages', $pages->id_pages);
		
		// Data untuk mengisi field2 form
		$data['default']['name_pages'] 		= $pages->name_pages;
		$data['default']['tags'] = $pages->tags;		
		$data['default']['last_update'] 	= date('d-M-Y h:i:s a', strtotime($pages->last_update));
		$data['default']['tipe_pages'] 		= $pages->tipe_pages;
		$data['default']['pages'] 		= $pages->pages;
		$data['default']['urutan'] 		= $pages->urutan;
		$data['default']['parentid'] 		= $pages->parentid;
		$data['default']['image_name'] 		= $pages->image_name;
		$data['default']['judul_seo'] 		= $pages->judul_seo;

		$data['message'] = $data['default']['last_update'] ;
		
		// buat session untuk menyimpan data tanggal yang sedang diupdate
		$this->session->set_userdata('last_update', $pages->last_update);

		$pagesbaca = $this->pages_model->get_pages()->result();
		$data['option_pages']['-'] = 'Pilih Parent';
		foreach($pagesbaca as $row)
		{
			$data['option_pages'][$row->id_pages] = $row->name_pages;
		}
			
		$this->load->view('template', $data);
	
	}

	function tampil_pages(){

		$data['title'] = $this->title;
		$data['h2_title'] = 'Pages CMS Punyaweb';
		$data['main_view'] = 'pages/pages_view';
		$data['isi'] = $this->pages_model->daftarPages();

		$this->load->view('template', $data);

	}
	
	}
	
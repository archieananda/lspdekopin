<?php
/**
 * Login Class
 *
 * @author	NEV(nahar.echa@msinfotama.com)
 */
class product extends Controller {
    function product()
	{
		parent::Controller();
        $this->load->model('product_model', '', TRUE);
        //declare helper web_helper
        $this->load->helper('web_helper');
    }
    
	var $limit = 10;
	var $title = 'Product Setting';

    function index()
	{
		if (!$this->session->userdata('login') == TRUE)
		{
            redirect('login');
        }
    }
    
    function product_setting(){

		$data['title'] = $this->title;
		$data['h2_title'] = 'Product Setting';
        $data['main_view'] = 'products/productsetting_view';
        //notes: getDataTableScriptSource declared in helpers directory, inside web_helper
        $data['extraHelperScript'] = getDataTableScriptSource();
        $data['extraHelperCss'] = getDataTableCssSource();
        $data['extraScript'] = array('product/product-setting.js');
        $data['prodListData'] = json_encode($this->product_model->productSettingList());

		$this->load->view('template', $data);
    }

    function product_setting_add(){
        $data['title'] = $this->title;
        $data['h2_title'] = 'Product Setting > Add';
        $data['main_view'] = 'products/productsetting_edit_view';
        $data['form_action']	= site_url('users/setting/add/insert');
        $data['extraHelperScript'] = array('tinymce/tinymce.min.js');
        $data['extraScript'] = array('product/product-setting-edit.js');
        $data['productTypeList'] = $this->product_model->getProductType();

		$this->load->view('template', $data);
    }

    function product_setting_edit($id=0){
        $data['title'] = $this->title;
        $data['h2_title'] = 'Product Setting > Edit';
        $data['main_view'] = 'products/productsetting_edit_view';
        $data['form_action']	= site_url('users/setting/edit/update');
        $data['extraHelperScript'] = array('tinymce/tinymce.min.js');
        $data['extraScript'] = array('product/product-setting-edit.js');
        $data['productTypeList'] = $this->product_model->getProductType();
        $data['productData'] = $this->product_model->getProductSetting($id);

		$this->load->view('template', $data);
    }

    
	function update_productsetting() {
        $productName = $this->input->post('productName');
        $productType = $this->input->post('productType');
        $productStatus = $this->input->post('productStatus');
        $productContent = $this->input->post('productContent');

		if($this->input->post('productid') !== NULL)
			$productid = $this->input->post('productid');		

		$newData = array(
			'ProductName'=>$productName,	
			'ProductType'=>$productType,
			'HTMLContent'=>$productContent,  
			'IsActive'=>($productStatus) ? "1" : "0"
		);
		
		if(strlen($userid) > 0){		
			$newData = array_merge($newData, array(
				'ProductID'=>$productid
			));
			return $this->product_model->saveUpdateProductSetting($newData);
		}
		else{
			return $this->product_model->saveInsertProductSetting($newData);
		}
	}

    function product_plafond_setting(){

		$data['title'] = $this->title;
		$data['h2_title'] = 'Product Plafond Scheme Setting';
        $data['main_view'] = 'products/productratesetting_view';
        //notes: getDataTableScriptSource declared in helpers directory, inside web_helper
        $data['extraHelperScript'] = getDataTableScriptSource();
        $data['extraHelperCss'] = getDataTableCssSource();
        $data['extraScript'] = array('product/product-setting.js');
        $data['prodListData'] = json_encode($this->product_model->productSettingList());

		$this->load->view('template', $data);
    }
}
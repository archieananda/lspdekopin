
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('getDataTableScript'))
{
    function getDataTableScriptSource(){
        return array('jquery.dataTables.min.js','dataTables.bootstrap.min.js','custom/data-table-logic.js');
    }

    function getDataTableCssSource(){
        return array('dataTables.bootstrap.min.css');
    }
}


?>
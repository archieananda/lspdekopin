
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Admin Danaku</title>

        <link rel="stylesheet" href="<?php echo base_url().'css/bootstrap.min.css';?>" />
         
        <link rel="stylesheet" href="<?php echo base_url().'css/style.css';?>" />

        <?php if(isset($extraHelperCss)){
            foreach($extraHelperCss as $css_helper_to_load):?>
            <link rel="stylesheet" href="<?php echo base_url().'css/'.$css_helper_to_load;?>" />
         <?php endforeach;}?>
    </head>
    <body>

        <div class="wrapper">
            <!-- Sidebar Holder -->
            <nav id="sidebar">
                <div class="sidebar-header">
                    <h3>Danaku Admin</h3>
                    <strong>DA</strong>
                </div>

                <!-- Untuk loading menu -->
                <?php $this->load->view('menu'); ?>

            </nav>

            <!-- Page Content Holder -->
            <div id="content">

                <nav class="navbar navbar-default">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="btn btn-primary navbar-btn">
                                <i class="glyphicon glyphicon-align-justify"></i>
                            </button>
                        </div>
                        <h3 class="title-page"><?php echo $h2_title ?></h3>
                        
                    </div>
                </nav>
                <div class="container-box">
                    <?php $this->load->View($main_view); ?>
                </div>
            </div>
        </div>

        <!-- jQuery -->
         <script src="<?php echo base_url().'js/jquery-3.3.1.min.js';?>"></script>
         <!-- Bootstrap Js -->
         <script src="<?php echo base_url().'js/bootstrap.min.js';?>"></script>
         <!-- Custom Js -->
         <script src="<?php echo base_url().'js/custom/pages.js';?>"></script>
         
         <?php if(isset($extraHelperScript)){ foreach($extraHelperScript as $scripts_helper_to_load):?>
            <script type='text/javascript' src = '<?php echo base_url()."js/".$scripts_helper_to_load;?>'></script>
         <?php endforeach;}?>

         <?php if(isset($extraScript)){ foreach($extraScript as $scripts_to_load):?>
            <script type='text/javascript' src = '<?php echo base_url()."js/custom/".$scripts_to_load;?>'></script>
         <?php endforeach;}?>

    </body>
</html>

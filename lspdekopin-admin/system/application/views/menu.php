<!--

Modified by NEV(nahar.echa@msinfotama.com)

-->
<ul class="list-unstyled components">
  <li class="active">
  <?php echo anchor('home', '<i class="glyphicon glyphicon-home"></i>Home');?>
  </li>
  <li class="active">
    <a href="#">
      <i class="glyphicon glyphicon-duplicate"></i>
      Application Inquiry
    </a>
  </li>
  <li>
      <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false">
          <i class="glyphicon glyphicon-wrench"></i>
          Setting
      </a>
      <ul class="collapse list-unstyled" id="pageSubmenu">
          <li><?php echo anchor('product/setting', 'Product Setting');?></li>
          <li><a href="#">Product Rate</a></li>
          <li><a href="#">Plafon Scheme</a></li>
          <li><a href="#">Sales</a></li>
          <!-- <li><a href="#">Master Setting</a></li> -->
      </ul>
  </li>
  <li>
    <?php echo anchor('users/setting', '<i class="glyphicon glyphicon-user"></i>User Account');?>
  </li>
  </li>
  <li>
  <?php echo anchor('login/process_logout', 
  '<i class="glyphicon glyphicon-off"></i>
  Log Out', array('onclick' => "return confirm('Anda yakin akan logout?')"));?>
    
  </li>
</ul>
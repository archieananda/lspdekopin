<form action='<?php echo $form_action; ?>' method='POST'>
<fieldset>
    <legend>User Info</legend>
    <input type="hidden" name="userid" id="userid" value='<?php echo $userData->user_id;?>'>
  <div class="form-group ">
    <label  for="formGroupInputLarge">Name</label>
    <input type="text" class="form-control" id="name" name="name" placeholder="Name" required="true"
    value='<?php if(isset($userData->user_nama)) echo $userData->user_nama; ?>'>
  </div>

    <div class="form-group ">
    <label  for="formGroupInputLarge">Username</label>
    <input type="text" class="form-control " id="username" name="username" placeholder="Username" required="true"
    value='<?php if(isset($userData->user_username)) echo $userData->user_username; ?>'>
       </div>

      <div class="form-group ">
    <label  for="formGroupInputLarge">Password</label>
    <input type="password" class="form-control " id="password" name="password" placeholder="Password">
  </div>

  <div class="form-group ">
    <label  for="formGroupInputLarge">Retype Password</label>
    <input type="password" class="form-control " id="retype" name="retype" placeholder="Retype Password">
  </div>
  <div class="form-group ">
    <label  for="formGroupInputLarge">Email</label>
    <input type="text" class="form-control " id="email" name="email" placeholder="Email" required="true"
    value='<?php if(isset($userData->user_email)) echo $userData->user_email; ?>'>
  </div>
  <div class="form-group ">
    <label  for="formGroupInputLarge">Phone</label>
    <input type="text" class="form-control " id="phone" name="phone" placeholder="Phone"
    value='<?php if(isset($userData->user_telp)) echo $userData->user_telp; ?>'>
  </div>
  <div class="form-group">
      <label class="form-check-label">
      <input class="form-check-input" name='userstatus' type="checkbox" 
            '<?php if(isset($userData->user_status)) echo ((bool)$userData->user_status) ? " checked='checked'" : ""; ?>' />
            User Status
      </label>
    </div>

<div class="form-group ">
    <label  for="formGroupInputLarge">Role</label>
   
<select class="form-control" required="true" name="userlevel">
<option value="">Select Role</option>
<?php
  foreach($roleTypeList as $role_type_list):
    if($userData->user_level == $role_type_list["value"])
        echo "<option selected='selected' value=".$role_type_list["value"].">"
        .$role_type_list["description"]."</option>";
    else
        echo "<option value=".$role_type_list["value"].">"
        .$role_type_list["description"]."</option>";
  endforeach;
  ?>
</select>
  </div>

  
  <?php echo anchor('users/setting', 'Cancel', 'id="btnCancel" class="btn btn-default btn-cancel"');?>
  <input type="submit" value="Save" class="btn btn-primary" />
 <div class="clearfix"></div>
</fieldset>
</form>
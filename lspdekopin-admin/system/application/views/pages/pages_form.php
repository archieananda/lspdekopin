
<div class="page-header">
<?php	echo ! empty($h2_title) ? '<h2>' . $h2_title . '</h2>': ''; ?>
 <?php echo anchor('pages/add', 'PUBLISH',  array('class' => 'btn btn-success') );?>
     <?php echo anchor('pages/add', 'SAVE',  array('class' => 'btn btn-primary') );?>
 <?php echo anchor('pages/add', 'PREVIEW',  array('class' => 'btn btn-primary') );?>

</div>

<div class="col-xs-12 col-md-12 ">

<?php echo ! empty($message) ? '<div class="alert alert-success" role="alert">last update <strong>'.$message.'</strong></div>': ''; ?>

<form class="form-horizontal" name="absen_form" method="post" action="<?php echo $form_action; ?>">


  <div class="form-group form-group-lg">
    <label class="col-sm-2 control-label" for="formGroupInputLarge">TITLE.</label>
    <div class="col-sm-10">
 <input type="text" class="form-control input-lg" id="title" name="title" placeholder="Title"
value="<?php echo set_value('name_pages', isset($default['name_pages']) ? $default['name_pages'] : ''); ?>">    </div>
  </div>

    <div class="form-group form-group-lg">
    <label class="col-sm-2 control-label" for="formGroupInputLarge">KEYWORD.</label>
    <div class="col-sm-10">
    <input type="text" class="form-control input-lg" id="keywords" name="keywords" placeholder="keywords"
value="<?php echo set_value('tags', isset($default['tags']) ? $default['tags'] : ''); ?>"
 >

       </div>
  </div>

      <div class="form-group form-group-lg">
    <label class="col-sm-2 control-label" for="formGroupInputLarge">IMAGES.</label><br>
    <div class="col-sm-10">
    <?php echo ! empty($default['image_name']) ? '<img src="'.base_url().'pictures/'.$default['image_name'].'" style="width:100px"': ''; ?>

  <br><br>
     <input type="file" id="images">
       </div>
  </div>


      <div class="form-group form-group-lg">
    <label class="col-sm-2 control-label" for="formGroupInputLarge">PAGES TYPE.</label>
    <div class="col-sm-10">
   
<select class="form-control">
<option>-</option>
  <option value="Single Page"  <?php echo set_select('tipe_pages', 'Single Page', isset($default['tipe_pages']) == 'Single Page'); ?>>Single Page</option>
  <option value="Post Page" <?php echo set_select('tipe_pages', 'Post Page', isset($default['tipe_pages']) == 'Post Page'); ?>>Post Page</option>
  <option value="Gallery Page" <?php echo set_select('tipe_pages', 'Gallery Page', isset($default['tipe_pages']) == 'Gallery Page'); ?>>Gallery Page</option>
  
</select>

       </div>
  </div>
        <div class="form-group form-group-lg">
    <label class="col-sm-2 control-label" for="formGroupInputLarge">PARENT.</label>
    <div class="col-sm-10">

<?php

 echo form_dropdown('id_pages', $option_pages , isset($default['parentid']) ? $default['parentid'] : '-' , 'class="form-control"'); ?>
      
       </div>
  </div>


        <div class="form-group form-group-lg">
    <label class="col-sm-2 control-label" for="formGroupInputLarge">IS MENU.</label>
    <div class="col-sm-2"><br>
    <input type="checkbox" id="inlineCheckbox1" value="option1"> YES
       </div>

        <label class="col-sm-4 control-label" for="formGroupInputLarge">SHOW PAGES IMAGE.</label>
     <div class="col-sm-4"><br>
    <input type="checkbox" id="inlineCheckbox1" value="option1"> YES
       </div>
  </div>



          <div class="form-group form-group-lg">
    <label class="col-sm-2 control-label" for="formGroupInputLarge">ADD ON.</label><br>
    <div class="col-sm-5">
    <input type="checkbox" id="inlineCheckbox1" value="option1"> SUB PAGES
       </div>

           <div class="col-sm-5">
    <input type="checkbox" id="inlineCheckbox1" value="option1"> SIDE PAGES
       </div>
  </div>

      <div class="form-group form-group-lg">
    <label class="col-sm-2 control-label" for="formGroupInputLarge">ORDER NUM.</label>
    <div class="col-sm-10">
    <input type="text" class="form-control input-lg" id="keywords" name="keywords" placeholder="order num"
value="<?php echo set_value('urutan', isset($default['urutan']) ? $default['urutan'] : ''); ?>"
 >

       </div>
  </div>

    <div class="form-group form-group-lg">
    <label class="col-sm-2 control-label" for="formGroupInputLarge"></label>
    <div class="col-sm-10">

<textarea class="form-control">
  <?php echo set_value('pages', isset($default['pages']) ? $default['pages'] : ''); ?>

</textarea>
       </div>
  </div>

</form>
</div>
<div class="clearfix"></div>
 
<?php

?>
 <!DOCTYPE html>
  <html>
    <head>
      <!--Import Google Icon Font-->
   <!--   <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	  
	  <style>
	  body {
  background: #222;
 <!-- background: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('https://unsplash.it/1200/800/?random');
 --> background-repeat: no-repeat;
  background-size: cover;
  background-position: center center;
  background-fill-mode: rgba(0,0,0,0.5);
  display: flex;
  justify-content: center;

  align-items: center;

}
.container {
  background: white;
  padding: 20px 25px;
  border: 5px solid #26a69a;
  width: 700px;

  box-sizing: border-box;
  position: relative;
}
.col.s6 > .btn {
   width: 100%;
}
.gender-male,.gender-female {
  display: inline-block;
}
.gender-female {
  margin-left: 35px;
}
radio:required {
  border-color: red;
}
.container {
  animation: showUp 0.5s cubic-bezier(0.18, 1.3, 1, 1) forwards;
}

@keyframes showUp {
  0% {
    transform: scale(0);
  }
  100% {
    transoform: scale(1);
  }
}
.row {margin-bottom: 10px;}

.ngl {
  position: absolute;
  top: -20px;
  right: -20px;
}

label {font-size:2em;}
</style>
	  
    </head>

    <body>
	<br><br>
	<div class="container">
		  
	  
      <div class="row">
        <div class="input-field col s6">
          <a class="btn btn-large btn-register waves-effect waves-light" target="_blank" href="registrasi_form.html">Personal</a>
        </div>

        <div class="input-field col s6">
         
          <a class="btn btn-large btn-register waves-effect waves-light" target="_blank" href="registrasi_group.html">Group</a>
          
        </div>
		<div class="input-field col s4">
         
        <p><a href="data.php"><button>Lihat Data</button></a></p>
          
        </div>
      </div>
    </form>
  </div>
  
</div>


      <!--JavaScript at end of body for optimized loading-->
      <script type="text/javascript" src="js/materialize.min.js"></script>
    </body>
  </html>
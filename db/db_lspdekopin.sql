-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 18, 2019 at 11:31 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_lspdekopin`
--

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE IF NOT EXISTS `berita` (
  `id_berita` bigint(20) unsigned NOT NULL,
  `id_pages` tinyint(4) DEFAULT NULL,
  `editor` varchar(250) NOT NULL,
  `detail_berita` longtext,
  `author` varchar(250) DEFAULT NULL,
  `status_headline` tinyint(4) DEFAULT '0',
  `status_newsflash` tinyint(4) DEFAULT '0',
  `status_berita` varchar(250) NOT NULL,
  `leadin_berita` longtext,
  `hari` varchar(250) DEFAULT NULL,
  `tgl_submit` datetime DEFAULT NULL,
  `tgl_publish` datetime NOT NULL,
  `judul_berita` varchar(250) DEFAULT NULL,
  `judul_seo` varchar(250) DEFAULT NULL,
  `tags` varchar(250) DEFAULT NULL,
  `views` int(5) unsigned DEFAULT '0',
  `img_path` varchar(250) NOT NULL,
  `thumbnail_path` varchar(250) NOT NULL,
  `img_caption` varchar(250) NOT NULL,
  `url` varchar(250) NOT NULL,
  `last_user` varchar(250) NOT NULL,
  `last_update` datetime NOT NULL,
  `is_pilihanredaksi` int(8) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `d_daftar_asesi`
--

CREATE TABLE IF NOT EXISTS `d_daftar_asesi` (
  `D_KODE_ASESI` bigint(20) NOT NULL AUTO_INCREMENT,
  `H_KODE_ASESI` bigint(20) NOT NULL,
  `KODE_PENDIDIKAN` varchar(50) NOT NULL,
  `KODE_PEKERJAAN` varchar(50) NOT NULL,
  `KODE_TUK` varchar(50) NOT NULL,
  `KODE_ANGGARAN` varchar(50) NOT NULL,
  `KODE_KEMENTRIAN` varchar(50) NOT NULL,
  `KODE_ASSESOR` varchar(50) NOT NULL,
  `KODE_SKEMA` varchar(50) NOT NULL,
  `NAMA` varchar(250) NOT NULL,
  `TEMPAT_LAHIR` varchar(50) NOT NULL,
  `TGL_LAHIR` datetime NOT NULL,
  `KODE_KOTA` varchar(10) NOT NULL,
  `KODE_PROPINSI` varchar(10) NOT NULL,
  `JNS_KELAMIN` varchar(10) NOT NULL,
  `TEMPAT_TINGGAL` varchar(500) NOT NULL,
  `TELP` varchar(20) NOT NULL,
  `EMAIL` varchar(250) NOT NULL,
  `NAMA_KOPERASI` varchar(250) NOT NULL,
  `JABATAN` varchar(50) NOT NULL,
  `ALAMAT_KOPERASI` varchar(250) NOT NULL,
  `TELP_KOPERASI` varchar(50) NOT NULL,
  `EMAIL_KOPERASI` varchar(250) NOT NULL,
  `TGL_UJI` datetime NOT NULL,
  `HASIL` varchar(10) NOT NULL,
  `CRTDATE` datetime DEFAULT NULL,
  `CRTBY` varchar(50) DEFAULT NULL,
  `LASTUPD` datetime DEFAULT NULL,
  `UPDBY` varchar(50) DEFAULT NULL,
  `NIK` varchar(50) NOT NULL,
  PRIMARY KEY (`D_KODE_ASESI`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `d_daftar_asesi`
--

INSERT INTO `d_daftar_asesi` (`D_KODE_ASESI`, `H_KODE_ASESI`, `KODE_PENDIDIKAN`, `KODE_PEKERJAAN`, `KODE_TUK`, `KODE_ANGGARAN`, `KODE_KEMENTRIAN`, `KODE_ASSESOR`, `KODE_SKEMA`, `NAMA`, `TEMPAT_LAHIR`, `TGL_LAHIR`, `KODE_KOTA`, `KODE_PROPINSI`, `JNS_KELAMIN`, `TEMPAT_TINGGAL`, `TELP`, `EMAIL`, `NAMA_KOPERASI`, `JABATAN`, `ALAMAT_KOPERASI`, `TELP_KOPERASI`, `EMAIL_KOPERASI`, `TGL_UJI`, `HASIL`, `CRTDATE`, `CRTBY`, `LASTUPD`, `UPDBY`, `NIK`) VALUES
(1, 1, '4', '1', 'TUK-LSP-023-TS', '1', '1', '000.004415.2014', 'SS.007.01/LSP.DEKOPIN/XII/2018', 'Archie ananda', 'Jakarta', '1988-12-10 00:00:00', '2991', '29', 'L', 'ALAMAT RUMAH', '212155231312', 'archie.ananda@gmail.com', 'KOPERASI', 'JABATAN', 'ALAMAT KOPERASI', '9292929299292', '9292929299292', '0000-00-00 00:00:00', 'K', '2019-11-10 22:04:06', 'Archie ananda', NULL, NULL, '2991991929192912'),
(2, 1, '5', '1', 'TUK-LSP-023-TS', '1', '1', '000.004415.2014', 'SS.007.01/LSP.DEKOPIN/XII/2018', 'Henny', 'Jakarta', '1987-05-05 00:00:00', '2991', '29', 'P', 'RUmah Alamat 2', '2123123123123', 'henny@gmail.com', 'KOPERASI', 'JABATAN', 'ALAMAT KOPERASI', '9292929299292', '9292929299292', '0000-00-00 00:00:00', 'BK', '2019-11-10 23:02:14', 'Henny', NULL, NULL, '29919919291929124'),
(3, 1, '3', '1', 'TUK-LSP-023-TS', '1', '1', '000.003500.2014', 'SS.007.01/LSP.DEKOPIN/XII/2018', 'Narso Suharno', 'Jakarta', '1988-10-10 00:00:00', '1111', '11', 'L', 'BSD bumi serpong damai', '09299910092121', 'narson@gmail.com', 'KOPERASI', 'JABATAN', 'ALAMAT KOPERASI', '9292929299292', '9292929299292', '0000-00-00 00:00:00', 'K', '2019-11-17 00:39:22', 'Narso Suharno', NULL, NULL, '111111111111111111');

-- --------------------------------------------------------

--
-- Table structure for table `h_daftar_asesi`
--

CREATE TABLE IF NOT EXISTS `h_daftar_asesi` (
  `H_KODE_ASESI` bigint(20) NOT NULL AUTO_INCREMENT,
  `KODE_TUK` varchar(50) NOT NULL,
  `TGL_ASESI` date NOT NULL,
  `STATUS` varchar(50) NOT NULL,
  `CRTDATE` datetime DEFAULT NULL,
  `CRTBY` varchar(50) DEFAULT NULL,
  `LASTUPD` datetime DEFAULT NULL,
  `UPDBY` varchar(50) DEFAULT NULL,
  `IS_DELETED` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`H_KODE_ASESI`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `h_daftar_asesi`
--

INSERT INTO `h_daftar_asesi` (`H_KODE_ASESI`, `KODE_TUK`, `TGL_ASESI`, `STATUS`, `CRTDATE`, `CRTBY`, `LASTUPD`, `UPDBY`, `IS_DELETED`) VALUES
(1, 'TUK-LSP-023-TS', '2019-11-06', 'DRAFT', '2019-11-07 00:00:00', 'ADMIN', '2019-11-07 00:00:00', 'ADMIN', 0),
(7, 'TUK-LSP-024-TS	', '2019-12-10', 'DRAFT', '2019-11-13 11:02:40', 'webadmin', '2019-11-13 11:10:06', 'webadmin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ms_assesor`
--

CREATE TABLE IF NOT EXISTS `ms_assesor` (
  `KODE_ASSESOR` varchar(50) NOT NULL,
  `DESKRIPSI` varchar(250) NOT NULL,
  `ISACTIVE` int(11) NOT NULL DEFAULT '1',
  `LASTUPD` datetime DEFAULT NULL,
  `UPDBY` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`KODE_ASSESOR`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_assesor`
--

INSERT INTO `ms_assesor` (`KODE_ASSESOR`, `DESKRIPSI`, `ISACTIVE`, `LASTUPD`, `UPDBY`) VALUES
('000.003500.2014', 'Yudoyono', 1, NULL, NULL),
('000.004415.2014', 'SUDIRMAN', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ms_kementrian`
--

CREATE TABLE IF NOT EXISTS `ms_kementrian` (
  `KODE_KEMENTRIAN` varchar(50) NOT NULL,
  `DESKRIPSI` varchar(250) NOT NULL,
  `ISACTIVE` int(11) NOT NULL DEFAULT '1',
  `LASTUPD` datetime DEFAULT NULL,
  `UPDBY` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`KODE_KEMENTRIAN`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ms_pekerjaan`
--

CREATE TABLE IF NOT EXISTS `ms_pekerjaan` (
  `KODE_PEKERJAAN` varchar(2) NOT NULL,
  `NAMA_PEKERJAAN` varchar(250) NOT NULL,
  `ISACTIVE` int(11) NOT NULL DEFAULT '1',
  `LASTUPD` datetime DEFAULT NULL,
  `UPDBY` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`KODE_PEKERJAAN`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_pekerjaan`
--

INSERT INTO `ms_pekerjaan` (`KODE_PEKERJAAN`, `NAMA_PEKERJAAN`, `ISACTIVE`, `LASTUPD`, `UPDBY`) VALUES
('1', 'Belum/Tidak Bekerja\r\n', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ms_pendidikan`
--

CREATE TABLE IF NOT EXISTS `ms_pendidikan` (
  `KODE_PENDIDIKAN` varchar(2) NOT NULL,
  `DESKRIPSI` varchar(250) NOT NULL,
  `ISACTIVE` int(11) NOT NULL DEFAULT '1',
  `LASTUPD` datetime DEFAULT NULL,
  `UPDBY` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`KODE_PENDIDIKAN`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_pendidikan`
--

INSERT INTO `ms_pendidikan` (`KODE_PENDIDIKAN`, `DESKRIPSI`, `ISACTIVE`, `LASTUPD`, `UPDBY`) VALUES
('1', 'SD', 1, NULL, 'ADMIN'),
('2', 'SMP', 1, NULL, 'ADMIN'),
('3', 'SMA/Sederajat', 1, NULL, 'ADMIN'),
('4', 'D2', 1, NULL, NULL),
('5', 'D3', 1, NULL, 'Admin'),
('6', 'D4', 1, NULL, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `ms_skema`
--

CREATE TABLE IF NOT EXISTS `ms_skema` (
  `KODE_SKEMA` varchar(50) NOT NULL,
  `DESKRIPSI` varchar(250) NOT NULL,
  `ISACTIVE` int(11) NOT NULL DEFAULT '1',
  `LASTUPD` datetime DEFAULT NULL,
  `UPDBY` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`KODE_SKEMA`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_skema`
--

INSERT INTO `ms_skema` (`KODE_SKEMA`, `DESKRIPSI`, `ISACTIVE`, `LASTUPD`, `UPDBY`) VALUES
('SS.007.01/LSP.DEKOPIN/XII/2018', 'KEPALA CABANG / MANAJER', 1, '2019-11-06 00:00:00', 'ADMIN');

-- --------------------------------------------------------

--
-- Table structure for table `ms_sumber_anggaran`
--

CREATE TABLE IF NOT EXISTS `ms_sumber_anggaran` (
  `KODE_ANGGARAN` varchar(50) NOT NULL,
  `DESKRIPSI` varchar(250) NOT NULL,
  `ISACTIVE` int(11) NOT NULL DEFAULT '1',
  `LASTUPD` datetime DEFAULT NULL,
  `UPDBY` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`KODE_ANGGARAN`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ms_tuk`
--

CREATE TABLE IF NOT EXISTS `ms_tuk` (
  `KODE_TUK` varchar(100) NOT NULL,
  `DESKRIPSI` varchar(250) NOT NULL,
  `ISACTIVE` int(11) NOT NULL DEFAULT '1',
  `LASTUPD` datetime DEFAULT NULL,
  `UPDBY` varchar(50) DEFAULT NULL,
  `KODE_UNIK` varchar(4) NOT NULL,
  PRIMARY KEY (`KODE_TUK`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_tuk`
--

INSERT INTO `ms_tuk` (`KODE_TUK`, `DESKRIPSI`, `ISACTIVE`, `LASTUPD`, `UPDBY`, `KODE_UNIK`) VALUES
('TUK-LSP-023-TS', 'Hotel Makasar Bandung', 1, '2019-11-07 00:00:00', 'ADMIN', '0988'),
('TUK-LSP-024-TS	', 'Cipinang Melayu', 1, '2019-11-13 11:02:40', 'webadmin', '9022');

-- --------------------------------------------------------

--
-- Table structure for table `ms_user`
--

CREATE TABLE IF NOT EXISTS `ms_user` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `USERNAME` varchar(50) NOT NULL,
  `PASSWORD` varchar(50) NOT NULL,
  `ISACTIVE` int(11) NOT NULL DEFAULT '1',
  `LASTUPD` datetime DEFAULT NULL,
  `UPDBY` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `USERNAME` (`USERNAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `m_setting`
--

CREATE TABLE IF NOT EXISTS `m_setting` (
  `SettingID` int(11) NOT NULL,
  `Parameter` varchar(100) NOT NULL,
  `Value` varchar(100) NOT NULL,
  `Description` varchar(200) DEFAULT NULL,
  `IsActive` bit(1) NOT NULL,
  `UserUpdate` varchar(20) NOT NULL,
  `LastUpdateDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_setting`
--

INSERT INTO `m_setting` (`SettingID`, `Parameter`, `Value`, `Description`, `IsActive`, `UserUpdate`, `LastUpdateDate`) VALUES
(162, 'Role', '3', 'LEADER', '1', 'admin', '2018-03-24 00:00:00'),
(163, 'Role', '4', 'SALES', '1', 'admin', '2018-03-24 00:00:00'),
(164, 'PageType', 'Single Page', 'Single Page', '1', 'nev', '2018-03-30 17:32:47'),
(165, 'PageType', 'Posts Page', 'Posts Page', '1', 'nev', '2018-03-30 17:32:58'),
(166, 'PageType', 'Galery Page', 'Galery Page', '1', 'nev', '2018-03-30 17:33:04'),
(1, 'ProductType', '1', 'KTA', '1', 'admin', '2018-02-11 00:00:00'),
(2, 'ProductType', '2', 'KARTU KREDIT', '1', 'admin', '2018-02-11 14:09:21'),
(3, 'ProductType', '3', 'MULTIGUNA', '1', 'admin', '2018-02-11 14:08:05'),
(1, 'ProductType', '1', 'KTA', '1', 'admin', '2018-02-11 00:00:00'),
(2, 'ProductType', '2', 'KARTU KREDIT', '1', 'admin', '2018-02-11 14:09:21'),
(3, 'ProductType', '3', 'MULTIGUNA', '1', 'admin', '2018-02-11 14:08:05'),
(4, 'Relationship', '1', 'SUAMI', '1', 'admin', '2018-02-11 15:41:09'),
(5, 'Relationship', '2', 'ISTRI', '1', 'admin', '2018-02-11 15:41:09'),
(6, 'Relationship', '3', 'AYAH', '1', 'admin', '2018-02-11 15:41:09'),
(7, 'Relationship', '4', 'IBU', '1', 'admin', '2018-02-11 15:41:09'),
(8, 'Relationship', '5', 'ADIK', '1', 'admin', '2018-02-11 15:41:09'),
(9, 'Relationship', '6', 'KAKAK', '1', 'admin', '2018-02-11 15:41:09'),
(10, 'Relationship', '7', 'IPAR', '1', 'admin', '2018-02-11 15:41:10'),
(11, 'Relationship', '8', 'ANAK', '1', 'admin', '2018-02-11 15:41:10'),
(12, 'Education', '1', 'SD', '1', 'admin', '2018-02-11 15:46:22'),
(13, 'Education', '2', 'SMP', '1', 'admin', '2018-02-11 15:46:22'),
(14, 'Education', '3', 'SMA', '1', 'admin', '2018-02-11 15:46:22'),
(15, 'Education', '4', 'D1', '1', 'admin', '2018-02-11 15:46:22'),
(16, 'Education', '5', 'D3', '1', 'admin', '2018-02-11 15:46:22'),
(17, 'Education', '6', 'S1', '1', 'admin', '2018-02-11 15:46:22'),
(18, 'Education', '7', 'S2', '1', 'admin', '2018-02-11 15:46:23'),
(19, 'Education', '8', 'S3', '1', 'admin', '2018-02-11 15:46:23'),
(20, 'Channel', '1', 'CIMB Niaga', '1', 'admin', '2018-02-11 15:48:06'),
(21, 'Channel', '2', 'Bank BRI', '1', 'admin', '2018-02-11 15:48:06'),
(22, 'Channel', '3', 'Bank BNI', '1', 'admin', '2018-02-11 15:48:06'),
(23, 'Channel', '4', 'Bank BCA', '1', 'admin', '2018-02-11 15:48:36'),
(24, 'Channel', '5', 'Bank Mandiri', '1', 'admin', '2018-02-11 15:48:36'),
(25, 'Channel', '6', 'CTBC', '1', 'admin', '2018-02-11 15:48:36'),
(26, 'Tennor', '1', '1 Bulan', '1', 'admin', '2018-02-11 17:43:22'),
(27, 'Tennor', '2', '2 Bulan', '1', 'admin', '2018-02-11 17:43:22'),
(28, 'Tennor', '3', '3 Bulan', '1', 'admin', '2018-02-11 17:43:22'),
(29, 'Tennor', '4', '4 Bulan', '1', 'admin', '2018-02-11 17:43:22'),
(30, 'Tennor', '5', '5 Bulan', '1', 'admin', '2018-02-11 17:43:22'),
(31, 'Tennor', '6', '6 Bulan', '1', 'admin', '2018-02-11 17:43:22'),
(32, 'Tennor', '7', '7 Bulan', '1', 'admin', '2018-02-11 17:43:22'),
(33, 'Tennor', '8', '8 Bulan', '1', 'admin', '2018-02-11 17:43:22'),
(34, 'Tennor', '9', '9 Bulan', '1', 'admin', '2018-02-11 17:43:22'),
(35, 'Tennor', '10', '10 Bulan', '1', 'admin', '2018-02-11 17:43:22'),
(36, 'Tennor', '11', '11 Bulan', '1', 'admin', '2018-02-11 17:43:22'),
(37, 'Tennor', '12', '12 Bulan', '1', 'admin', '2018-02-11 17:43:22'),
(38, 'Tennor', '13', '13 Bulan', '1', 'admin', '2018-02-11 17:43:22'),
(39, 'Tennor', '14', '14 Bulan', '1', 'admin', '2018-02-11 17:43:23'),
(40, 'Tennor', '15', '15 Bulan', '1', 'admin', '2018-02-11 17:43:23'),
(41, 'Tennor', '16', '16 Bulan', '1', 'admin', '2018-02-11 17:43:23'),
(42, 'Tennor', '17', '17 Bulan', '1', 'admin', '2018-02-11 17:43:23'),
(43, 'Tennor', '18', '18 Bulan', '1', 'admin', '2018-02-11 17:43:23'),
(44, 'Tennor', '19', '19 Bulan', '1', 'admin', '2018-02-11 17:43:23'),
(45, 'Tennor', '20', '20 Bulan', '1', 'admin', '2018-02-11 17:43:23'),
(46, 'Tennor', '21', '21 Bulan', '1', 'admin', '2018-02-11 17:43:23'),
(47, 'Tennor', '22', '22 Bulan', '1', 'admin', '2018-02-11 17:43:23'),
(48, 'Tennor', '23', '23 Bulan', '1', 'admin', '2018-02-11 17:43:23'),
(49, 'Tennor', '24', '24 Bulan', '1', 'admin', '2018-02-11 17:43:23'),
(50, 'Tennor', '25', '25 Bulan', '1', 'admin', '2018-02-11 17:43:23'),
(51, 'Tennor', '26', '26 Bulan', '1', 'admin', '2018-02-11 17:43:23'),
(52, 'Tennor', '27', '27 Bulan', '1', 'admin', '2018-02-11 17:43:23'),
(53, 'Tennor', '28', '28 Bulan', '1', 'admin', '2018-02-11 17:43:23'),
(54, 'Tennor', '29', '29 Bulan', '1', 'admin', '2018-02-11 17:43:23'),
(55, 'Tennor', '30', '30 Bulan', '1', 'admin', '2018-02-11 17:43:23'),
(56, 'Tennor', '31', '31 Bulan', '1', 'admin', '2018-02-11 17:43:23'),
(57, 'Tennor', '32', '32 Bulan', '1', 'admin', '2018-02-11 17:43:24'),
(58, 'Tennor', '33', '33 Bulan', '1', 'admin', '2018-02-11 17:43:24'),
(59, 'Tennor', '34', '34 Bulan', '1', 'admin', '2018-02-11 17:43:24'),
(60, 'Tennor', '35', '35 Bulan', '1', 'admin', '2018-02-11 17:43:24'),
(61, 'Tennor', '36', '36 Bulan', '1', 'admin', '2018-02-11 17:43:24'),
(62, 'Tennor', '37', '37 Bulan', '1', 'admin', '2018-02-11 17:43:24'),
(63, 'Tennor', '38', '38 Bulan', '1', 'admin', '2018-02-11 17:43:24'),
(64, 'Tennor', '39', '39 Bulan', '1', 'admin', '2018-02-11 17:43:24'),
(65, 'Tennor', '40', '40 Bulan', '1', 'admin', '2018-02-11 17:43:24'),
(66, 'Tennor', '41', '41 Bulan', '1', 'admin', '2018-02-11 17:43:24'),
(67, 'Tennor', '42', '42 Bulan', '1', 'admin', '2018-02-11 17:43:24'),
(68, 'Tennor', '43', '43 Bulan', '1', 'admin', '2018-02-11 17:43:24'),
(69, 'Tennor', '44', '44 Bulan', '1', 'admin', '2018-02-11 17:43:24'),
(70, 'Tennor', '45', '45 Bulan', '1', 'admin', '2018-02-11 17:43:24'),
(71, 'Tennor', '46', '46 Bulan', '1', 'admin', '2018-02-11 17:43:24'),
(72, 'Tennor', '47', '47 Bulan', '1', 'admin', '2018-02-11 17:43:24'),
(73, 'Tennor', '48', '48 Bulan', '1', 'admin', '2018-02-11 17:43:24'),
(74, 'Tennor', '49', '49 Bulan', '1', 'admin', '2018-02-11 17:43:24'),
(75, 'Tennor', '50', '50 Bulan', '1', 'admin', '2018-02-11 17:43:25'),
(76, 'Tennor', '51', '51 Bulan', '1', 'admin', '2018-02-11 17:43:25'),
(77, 'Tennor', '52', '52 Bulan', '1', 'admin', '2018-02-11 17:43:25'),
(78, 'Tennor', '53', '53 Bulan', '1', 'admin', '2018-02-11 17:43:25'),
(79, 'Tennor', '54', '54 Bulan', '1', 'admin', '2018-02-11 17:43:25'),
(80, 'Tennor', '55', '55 Bulan', '1', 'admin', '2018-02-11 17:43:25'),
(81, 'Tennor', '56', '56 Bulan', '1', 'admin', '2018-02-11 17:43:25'),
(82, 'Tennor', '57', '57 Bulan', '1', 'admin', '2018-02-11 17:43:25'),
(83, 'Tennor', '58', '58 Bulan', '1', 'admin', '2018-02-11 17:43:25'),
(84, 'Tennor', '59', '59 Bulan', '1', 'admin', '2018-02-11 17:43:25'),
(85, 'Tennor', '60', '60 Bulan', '1', 'admin', '2018-02-11 17:43:25'),
(86, 'ApplicationStatus', '1', 'Request', '1', 'admin', '2018-02-11 17:49:45'),
(87, 'ApplicationStatus', '2', 'Followup', '1', 'admin', '2018-02-11 17:49:45'),
(88, 'ApplicationStatus', '3', 'Junk', '1', 'admin', '2018-02-11 17:49:45'),
(89, 'ApplicationStatus', '4', 'Data Entry', '1', 'admin', '2018-02-11 17:49:46'),
(90, 'ApplicationStatus', '5', 'PickUp', '1', 'admin', '2018-02-11 17:49:46'),
(91, 'ApplicationStatus', '6', 'Entry Done', '1', 'admin', '2018-02-11 17:49:46'),
(92, 'CardType', '1', 'Bronze', '1', 'admin', '2018-02-11 17:52:47'),
(93, 'CardType', '2', 'Silver', '1', 'admin', '2018-02-11 17:52:47'),
(94, 'CardType', '3', 'Gold', '1', 'admin', '2018-02-11 17:52:47'),
(95, 'CardType', '4', 'Platinum', '1', 'admin', '2018-02-11 17:52:47'),
(96, 'CardType', '5', 'World', '1', 'admin', '2018-02-11 17:52:47'),
(97, 'Role', '1', 'ADMIN', '1', 'admin', '2018-03-04 15:39:00'),
(98, 'Role', '2', 'CUSTOMER', '1', 'admin', '2018-02-17 19:38:15'),
(99, 'ResidenceStatus', '1', 'PRIBADI', '1', 'admin', '2018-02-23 23:09:26'),
(100, 'ResidenceStatus', '2', 'KELUARGA', '1', 'webadmin', '2018-03-18 09:34:45'),
(101, 'ResidenceStatus', '3', 'SEWA', '1', 'admin', '2018-02-23 23:09:26'),
(102, 'Occupation', '1', 'PEGAWAI NEGERI', '1', 'admin', '2018-02-23 23:17:45'),
(103, 'Occupation', '2', 'PEGAWAI SWASTA', '1', 'admin', '2018-02-23 23:17:45'),
(104, 'Occupation', '3', 'WIRASWASTA', '1', 'admin', '2018-02-23 23:17:45'),
(105, 'Occupation', '4', 'PROFESIONAL', '1', 'webadmin', '2018-03-18 09:35:30'),
(106, 'Occupation', '5', 'LAIN - LAIN', '1', 'admin', '2018-02-23 23:17:45'),
(107, 'ApplicationStatus', '8', 'Reject Bank', '1', 'admin', '2018-02-11 17:49:46'),
(108, 'ApplicationStatus', '9', 'Approved', '1', 'admin', '2018-02-11 17:49:46'),
(110, 'CardType', '6', 'Infinite', '1', 'admin', '2018-03-07 21:22:01'),
(111, 'BankSetting', '1', 'BNI', '1', 'nev', '2018-03-04 15:40:06'),
(112, 'BankSetting', '2', 'BCA', '1', 'nev', '2018-03-04 15:40:20'),
(113, 'BankSetting', '3', 'CIMB Niaga', '1', 'nev', '2018-03-04 15:48:17'),
(114, 'BankSetting', '4', 'BRI', '1', 'nev', '2018-03-04 15:48:29'),
(115, 'BankSetting', '5', 'Mandiri', '1', 'nev', '2018-03-04 15:48:43'),
(116, 'BankSetting', '6', 'Mega', '1', 'nev', '2018-03-04 15:48:54'),
(117, 'Channel', '7', 'Bank Mega', '1', 'nev', '2018-03-04 21:12:01'),
(118, 'Channel', '8', 'Bank BTN', '1', 'nev', '2018-03-07 21:12:01'),
(119, 'FollowupStatus', '2', 'Minta dihubungi ulang', '1', 'nev', '2018-03-10 19:49:23'),
(120, 'FollowupStatus', '3', 'Telepon tidak diangkat', '1', 'nev', '2018-03-10 19:49:42'),
(121, 'FollowupStatus', '4', 'Nomor tidak aktif', '1', 'nev', '2018-03-10 19:49:59'),
(122, 'FollowupStatus', '1', 'OK', '1', 'nev', '2018-03-10 19:50:28'),
(123, 'FollowupStatus', '5', 'Lain lain', '1', 'nev', '2018-03-10 19:50:38'),
(124, 'CardType', '7', 'Other', '1', 'nev', '2018-03-10 20:31:22'),
(126, 'MailingAddressType', '1', 'Home Address', '1', 'nev', '2018-03-11 15:59:25'),
(127, 'MailingAddressType', '2', 'Business Address', '1', 'nev', '2018-03-11 15:59:51'),
(128, 'MailingAddressType', '3', 'Email Address', '1', 'nev', '2018-03-11 16:00:03'),
(129, 'PickupStatus', '1', 'Waiting For Pickup', '1', 'nev', '2018-03-12 21:49:26'),
(130, 'PickupStatus', '2', 'Failed Pickup', '1', 'nev', '2018-03-12 21:49:42'),
(131, 'PickupStatus', '3', 'Success Pickup', '1', 'nev', '2018-03-12 21:50:01'),
(132, 'RejectReason', '1', 'Nasbah blacklist', '1', 'nev', '2018-03-13 23:32:06'),
(133, 'RejectReason', '99', 'Alasan Lain', '1', 'nev', '2018-03-13 23:32:37'),
(134, 'ApplicationStatus', '7', 'Submit Bank', '1', 'nev', '2018-03-13 00:00:00'),
(135, 'ApprovalResult', 'APP', 'Approved', '1', 'nev', '2018-03-14 00:51:48'),
(136, 'ApprovalResult', 'REJ', 'Rejected', '1', 'nev', '2018-03-14 00:51:55'),
(137, 'Channel', '9', 'UOB', '1', 'webadmin', '2018-03-18 09:37:23'),
(138, 'BankSetting', '10', 'UOB', '1', 'webadmin', '2018-03-18 09:23:02'),
(139, 'BankSetting', '7', 'HSBC', '1', 'webadmin', '2018-03-18 09:24:24'),
(140, 'BankSetting', '8', 'CITIBANK', '1', 'webadmin', '2018-03-18 09:24:52'),
(141, 'BankSetting', '9', 'DBS', '1', 'webadmin', '2018-03-18 09:25:30'),
(142, 'BankSetting', '11', 'PANIN', '1', 'webadmin', '2018-03-18 09:26:02'),
(143, 'BankSetting', '12', 'ANZ', '1', 'webadmin', '2018-03-18 09:26:25'),
(144, 'BankSetting', '13', 'BTN', '1', 'webadmin', '2018-03-18 09:26:55'),
(145, 'BankSetting', '14', 'AEON', '1', 'webadmin', '2018-03-18 09:27:36'),
(146, 'BankSetting', '15', 'DANAMON', '1', 'webadmin', '2018-03-18 09:28:54'),
(147, 'BankSetting', '16', 'BII MAYBANK', '1', 'webadmin', '2018-03-18 09:29:30'),
(148, 'BankSetting', '17', 'STANDARD CHARTERED', '1', 'webadmin', '2018-03-18 09:30:03'),
(149, 'BankSetting', '18', 'BUKOPIN', '1', 'webadmin', '2018-03-18 09:30:35'),
(150, 'BankSetting', '19', 'PERMATA', '1', 'webadmin', '2018-03-18 09:31:03'),
(151, 'BankSetting', '20', 'OCBC NISP', '1', 'webadmin', '2018-03-18 09:31:37'),
(152, 'BankSetting', '21', 'BANK SINARMAS', '1', 'webadmin', '2018-03-18 09:32:00'),
(153, 'BankSetting', '22', 'MNC BANK', '1', 'webadmin', '2018-03-18 09:32:33'),
(154, 'Relationship', '9', 'LAIN-LAIN', '1', 'webadmin', '2018-03-18 09:33:26'),
(155, 'Channel', '10', 'HSBC', '1', 'webadmin', '2018-03-18 09:36:47'),
(156, 'Channel', '11', 'DBS', '1', 'webadmin', '2018-03-27 06:21:46'),
(157, 'Channel', '13', 'BTN', '1', 'webadmin', '2018-03-27 06:22:36'),
(158, 'Channel', '12', 'DBS Credit Card', '1', 'webadmin', '2018-03-27 06:26:05'),
(159, 'Channel', '14', 'PERMATA', '1', 'webadmin', '2018-03-18 09:39:57'),
(160, 'Channel', '15', 'KTA STANDARD CHARTERED Online', '1', 'webadmin', '2018-03-27 06:25:46'),
(161, 'Channel', '16', 'STANDARD CHARTERED BANK', '1', 'webadmin', '2018-03-27 06:25:27'),
(162, 'Role', '3', 'LEADER', '1', 'admin', '2018-03-24 00:00:00'),
(163, 'Role', '4', 'SALES', '1', 'admin', '2018-03-24 00:00:00'),
(164, 'PageType', 'Single Page', 'Single Page', '1', 'nev', '2018-03-30 17:41:53'),
(165, 'PageType', 'Posts Page', 'Posts Page', '1', 'nev', '2018-03-30 17:41:54'),
(166, 'PageType', 'Galery Page', 'Galery Page', '1', 'nev', '2018-03-30 17:41:54');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id_pages` bigint(20) NOT NULL,
  `judul_seo` varchar(250) DEFAULT NULL,
  `name_pages` varchar(250) DEFAULT NULL,
  `isMenu` tinyint(4) NOT NULL DEFAULT '0',
  `showImage` tinyint(4) NOT NULL DEFAULT '0',
  `pages` longtext,
  `tipe_pages` varchar(250) DEFAULT NULL,
  `subpages` tinyint(4) NOT NULL DEFAULT '0',
  `sidepages` tinyint(4) NOT NULL DEFAULT '0',
  `last_update` datetime NOT NULL,
  `last_user` varchar(250) NOT NULL,
  `image_name` varchar(250) DEFAULT NULL,
  `tags` varchar(250) DEFAULT NULL,
  `urutan` int(11) NOT NULL DEFAULT '99',
  `parentid` varchar(250) DEFAULT '-',
  `is_publish` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_pages`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id_pages`, `judul_seo`, `name_pages`, `isMenu`, `showImage`, `pages`, `tipe_pages`, `subpages`, `sidepages`, `last_update`, `last_user`, `image_name`, `tags`, `urutan`, `parentid`, `is_publish`) VALUES
(0, 'bussiness-strategy', 'BUSSINESS STRATEGY', 0, 0, '<div class="row">\r\n<div class="col-lg-8 col-lg-offset-2">\r\n<div class="wow fadeInDown" data-wow-delay="0.1s">\r\n<div class="section-heading text-center">\r\n<h2 class="h-bold label-overlay">Business Model</h2>\r\n<p>Smart Strategy leads to Smart Business.</p>\r\n<hr class="tall" /></div>\r\n</div>\r\n</div>\r\n</div>\r\n<div class="row">\r\n<div class="col-sm-6 col-md-6">\r\n<div class="box text-center">\r\n<h5 class="h-bold label-overlay marginbot-50">Corporate Business Yess</h5>\r\n<div class="col-sm-6 col-md-6">\r\n<div class="wow bounceInUp" data-wow-delay="0.1s">\r\n<div class="circle-primary">\r\n<div class="text-wrapper paddingtop-60"><strong>B - C</strong>\r\n<p>business - corporate</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<div class="wow bounceInUp" data-wow-delay="0.2s">\r\n<div class="col-sm-6 col-md-6">\r\n<div class="circle-primary">\r\n<div class="text-wrapper paddingtop-50"><strong>B - B - C</strong>\r\n<p>business - banking financial institution - corporate</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<div class="col-sm-6 col-md-6">\r\n<div class="box text-center">\r\n<h5 class="h-bold label-overlay orange marginbot-50">Retail Business</h5>\r\n<div class="col-sm-6 col-md-6">\r\n<div class="wow bounceInUp" data-wow-delay="0.3s">\r\n<div class="circle-warning">\r\n<div class="text-wrapper"><strong>B - C</strong>\r\n<p><strong>B </strong>Partner distribusi / other institutions IT platform - <strong> C </strong>(individual)</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<div class="col-sm-6 col-md-6">\r\n<div class="wow bounceInUp" data-wow-delay="0.4s">\r\n<div class="circle-warning">\r\n<div class="text-wrapper"><strong>B - B - C</strong>\r\n<p><strong>B </strong>( financial institution / retail/ Telkom/ IT) &ndash; <strong>B </strong>partner distribusi - <strong>C </strong>(individual)</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>', 'Single Page', 0, 0, '2018-09-17 10:17:02', 'webadmin', NULL, 'bussiness', 0, '-', 0),
(1, 'vision', 'Vision', 1, 0, '<div class="row">\r\n			<div class="col-lg-8 col-lg-offset-2">\r\n				<div class="wow fadeInDown" data-wow-delay="0.1s">\r\n					<div class="section-heading text-center">\r\n						<h2 class="h-bold label-overlay">Vison & Mission</h2>\r\n						<hr class="tall" />\r\n						</div>\r\n					</div>\r\n			</div>\r\n		</div>\r\n\r\n		<div class="row">\r\n			<div class="col-md-6">\r\n\r\n				<div class="col-md-12">\r\n\r\n					<blockquote id="blockquote-vision" class="wow bounceInUp" data-wow-delay="0.1s">\r\n					<h2 class="font-warning"><i class="fa fa-quote-left"></i></h2>\r\n					<h1 class="font-warning"><strong>Impacting Indonesian People beyond Protection</h1>\r\n					<h2 class="font-warning"><i class="fa fa-quote-right"></i></h2>\r\n					</blockquote>\r\n\r\n				</div>\r\n			</div>\r\n\r\n\r\n			<div class="col-md-6">\r\n\r\n				<div class="col-md-12">\r\n\r\n					<blockquote id="blockquote-mission" class="wow bounceInUp" data-wow-delay="0.2s">\r\n						<span class="list"><strong class="font-info">Empower our corporate and retail partners and customers</strong> to give positive impact\r\n						for society through penetrating the right\r\n						Insurance products to more Indonesians.</span>\r\n					</blockquote>\r\n\r\n\r\n				</div>\r\n			</div>\r\n		</div>', 'Single Page', 0, 0, '2018-09-09 10:40:27', 'webadmin', 'uploadResource/img/ee2a564aafe3cfcee88e952bb2b3101c.jpg', 'visi misi', 0, '-', 0),
(3, 'profiles', 'Profiles', 1, 0, '<div class="intro-content">\r\n		<div class="container">\r\n			<div class="row">\r\n				<div class="col-md-6">					\r\n					<div class="col-md-12">					\r\n						<blockquote>\r\n							<p class="wow fadeInLeft" data-wow-delay="0.1s">Why us?\r\n								<h1 class="font-warning wow fadeInLeft" data-wow-delay="0.3s"><strong>We Understand You,</strong><br/>\r\n								and Your Needs.\r\n								</h1>\r\n							</p>\r\n							<img id="imgtestimonial" src="img/icons/need.png" class="wow bounceInUp" data-wow-delay="0.1s" alt="" width="100" height="100" />\r\n						</blockquote>\r\n					</div>\r\n			\r\n				</div>\r\n				<div class="col-md-6">\r\n						<div class="col-md-12">					\r\n\r\n							<blockquote>\r\n							<span class="list wow fadeInRight" data-wow-delay="0.7s"><strong class="font-info">Brokers are first and foremost representing the Customers</strong>\r\n							, hence the insurance products\r\n							offered and sold to the customers should focus\r\n							on the needs and benefits of the customers.\r\n							Thus, ‘Needs driven selling’ method will become\r\n							a quick win strategy for Provis.<br/>\r\n							</span>\r\n							<span class="list wow fadeInRight" data-wow-delay="0.9s"><strong class="font-info">Personalized customer needs and technology driven selling method.</strong></span>\r\n							<span class="list wow fadeInRight" data-wow-delay="1.2s"><strong class="font-info">Simple. Fast. Easy Business Process.</strong></span>\r\n							<span class="list wow fadeInRight" data-wow-delay="1.4s"><strong class="font-info">Experienced. Trusted. Credible consultancy.</strong></span>\r\n							</blockquote>\r\n\r\n\r\n						</div>\r\n					</div>\r\n				</div>\r\n			</div>\r\n			\r\n		</div>\r\n	</div>', 'Single Page', 0, 0, '2018-09-09 11:30:03', 'webadmin', 'uploadResource/img/e70b33681ac18d9ca00465a73a0419dc.png', 'Profiles', 0, '-', 0),
(4, 'contact', 'Contact', 1, 0, '<div class="row">\r\n    <!-- <div class="footer-ribon">\r\n          <span>Reach Us Now</span>\r\n    </div> -->\r\n\r\n      <div class="col-md-12 text-center">\r\n        <h1 class="paddingtop-30">Life is an investment.</h1>\r\n        <p>We Had Helped Many People,<br/>\r\n            and Still Adding Up.</p>\r\n        <hr class="light">\r\n      </div>\r\n     \r\n  </div>\r\n  <div class="row">\r\n    <div class="col-md-12 text-center">\r\n\r\n      <p class="short"><strong>p.</strong>(021) 252 5080/ 5083</p>\r\n      <p class="short"><strong>e.</strong> service@provisms.com</p>\r\n      <p class="short"><strong>e.</strong>  www.provisms.com</p>\r\n      <ul class="list icons list-unstyled">\r\n        <li> WORLD TRADE CENTRE 5, 6TH FLOOR</li>\r\n        <li>Jend. Sudirman kav 29</li>\r\n        <li>Jakarta 12920</li>\r\n      </ul>\r\n    </div>\r\n  </div>', 'Single Page', 0, 0, '2018-09-09 11:40:05', 'webadmin', 'uploadResource/img/ccbb71e5e875f1906c51e18793e2fd1e.png', 'contact', 0, '-', 0),
(5, 'shareholders', 'Shareholders', 1, 0, '<div id="intro-content align-content-center">\r\n\r\n            <ul class="lead-list margintop-50">\r\n                <li><span class="list">\r\n                <p class="h-bold wow bounceInDown" data-wow-duration="1s" data-wow-delay="0.1s" > Our Management Team</p>\r\n                    <h1 class="h-bold label-overlay"><strong class="wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.2s">Professional People</strong> <br/>\r\n                    <span class="wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.4s">Behind the stage</span></h1>\r\n                    <!-- <desc class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.4s">— Millard Drexler</desc> -->\r\n\r\n                    </span>\r\n                </li>\r\n                <li>\r\n                <span class="list marginbot-20">\r\n                  <h6 class="font-warning wow bounceInLeft" data-wow-duration="1s" data-wow-delay="0.5s">Head Office</h6>\r\n                  <desc class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.8s">Jakarta, Indonesia</desc></span>\r\n\r\n                </li>\r\n                <li>\r\n                  <span id="our-team" class="list wow bounceInLeft" data-wow-duration="0.5s" data-wow-delay="1.1s"><h5 class="list h-bold">Our Team</h5></span>\r\n                  <div class="row">\r\n                    <div  class="col-md-6">\r\n                    <span class="list">\r\n                    <desc class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1.2s">PRESIDENT COMMISSIONER</desc>\r\n                    <h6 class="wow fadeInDown" data-wow-duration="3s" data-wow-delay="1.2s">Indra Simarta</h6>\r\n                  </span>\r\n                    </div>\r\n                    <div  class="col-md-6">\r\n                    <span class="list">\r\n                      <desc class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1.6s">PRESIDENT DIRECTOR</desc>\r\n                      <h6 class="wow fadeInDown" data-wow-duration="3s" data-wow-delay="1.6s">Sylvia Yan</h6>\r\n                    </span>\r\n                    </div>\r\n                  </div>\r\n                </li>\r\n                <li>\r\n                  <div class="row">\r\n                    <div  class="col-md-6">\r\n                    <span class="list">\r\n                      <desc class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="1.9s">COMMISSIONER</desc>\r\n                      <h6 class="wow fadeInDown" data-wow-duration="3s" data-wow-delay="1.9s">Salim Kartono</h6>\r\n                    </span>\r\n                    </div>\r\n                    <div  class="col-md-6">\r\n                    <span class="list">\r\n                      <desc class="wow fadeInUp" data-wow-duration="2s" data-wow-delay="2.4s">DIRECTOR</desc>\r\n                      <h6  class="wow fadeInDown" data-wow-duration="3s" data-wow-delay="2.4s">Liny Tanto</h6>\r\n                    </span>\r\n                    </div>\r\n                  </div>\r\n                </li>\r\n              </ul>\r\n          \r\n          </div>', 'Single Page', 0, 0, '2018-09-09 11:16:20', 'webadmin', 'uploadResource/img/3cc42ad37dfc28fe347bcb573c90ba27.jpg', 'shareholders', 0, '-', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` bigint(20) NOT NULL,
  `user_username` varchar(250) NOT NULL,
  `user_email` varchar(250) NOT NULL,
  `user_nama` varchar(250) DEFAULT NULL,
  `user_password` varchar(250) NOT NULL,
  `user_level` int(5) NOT NULL DEFAULT '3',
  `user_status` int(5) NOT NULL,
  `user_telp` varchar(250) DEFAULT NULL,
  `ProductTypeID` int(11) DEFAULT NULL,
  `user_parent` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_username`, `user_email`, `user_nama`, `user_password`, `user_level`, `user_status`, `user_telp`, `ProductTypeID`, `user_parent`) VALUES
(22, 'webadmin', 'henny@gmail.com', 'Henny Ananda', '3cd3d3dfb2408b879ecd322a86d28150c1e1ad5c', 1, 0, '22323232', 0, 0),
(36, 'archie.ananda@gmail.com', 'archie.ananda@gmail.com', 'Archie', '8e0613efb7869190a32f7dc201e83ba6ec957464', 1, 0, '+6287776141024', 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

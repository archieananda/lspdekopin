

<?php 
require_once 'getdata.php';
?>



<!DOCTYPE html>
<html>

<head>

    <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" />

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link type="text/css" rel="stylesheet" href="css/style.css" media="screen,projection" />
<title>Daftar </title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>

<body>
    <br>
    <br>
    <div class="container">

        <div class="row">
            <form class="col s12" id="reg-form" method="POST" action="registrasi_form.php">
               

                <div class="row">
                    <div class="input-field col s6">
                        <h5> TUK</h5>
                        <select class="browser-default" id="kode_tuk" name="kode_tuk">
                            <option value="" disabled selected>Choose your option</option>
                            <?php while($d = mysqli_fetch_array($tuk)){ ?>
                            <option value="<?php echo $d['KODE_TUK']; ?>"><?php echo $d['DESKRIPSI']; ?></option>
                            
                         <?php   }; ?>

                        </select>
                        <span class="helper-text" data-error="Pertanyaan ini wajib di isi" data-success="right"></span>
                    </div>


                    <div class="input-field col s6">
                        <h5> Skema</h5>
                        <select class="browser-default" id="kode_skema" name="kode_skema">
                            <option value="" disabled selected>Choose your option</option>
                            <?php while($d = mysqli_fetch_array($skema)){ ?>
                            <option value="<?php echo $d['KODE_SKEMA']; ?>"><?php echo $d['DESKRIPSI']; ?></option>
                            
                         <?php   }; ?>

                        </select>
                        <span class="helper-text" data-error="Pertanyaan ini wajib di isi" data-success="right"></span>
                    </div>
                </div>

 <div class="row">
                    <div class="input-field col s12">
                        <h5>Kode UNIK</h5>
                        <input id="kode_unik" name="kode_unik" type="text" class="validate" required>

                    </div>

                </div>
               

                <div class="input-field col s12 x">
                    <button class="btn btn-large btn-register waves-effect waves-light" type="submit" name="action">Mulai Registrasi

                    </button>

                </div>

        </div>
        </form>
    </div>

</body>

</html>